#!/bin/bash
#NO_CLASS=400
NUM_VALUE=400
NO_CLASS=1000
NO_EVENT=6000
RANK=17

PUB_BROKER_SIZE=6
SUB_BROKER_SIZE=4
CASE_NUM=7

NO_RUN=1
PUB_SIZE=1
WAIT_TIME=50
SUB_ETIME=4400
PUB_ETIME=3600
#NUM_VALUE=1
FOLDER_PREFIX="case_"
PUB_FILE_PATH=""
SUB_FILE_PATH=""
CUR_EXP_PATH=$(pwd)
#./scrum3/padres
echo "$CUR_EXP_PATH"

TR=(
	0 
	0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.06:0.06:0.06:0.06:0.06		
	0.1:0.08:0.07:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.05:0.04:0.04:0.02		
	0.15:0.1:0.08:0.06:0.05:0.05:0.05:0.05:0.05:0.04:0.03:0.03:0.03:0.02:0.01		
	0.2:0.14:0.08:0.06:0.05:0.05:0.05:0.04:0.03:0.03:0.02:0.02:0.01:0.01:0.01		
	0.25:0.13:0.09:0.07:0.07:0.05:0.04:0.03:0.02:0.02:0.01:0.01:0.01:0:0		
	0.3:0.16:0.1:0.06:0.06:0.03:0.03:0.02:0.02:0.01:0.01:0:0:0:0		
	0.35:0.15:0.09:0.05:0.04:0.03:0.03:0.02:0.02:0.01:0.01:0:0:0:0		
   )	

echo "-----For testcase-----"
for (( testcase=1;testcase<=$CASE_NUM;testcase++ ))
do
	CURRENT_FOLDER=$FOLDER_PREFIX$testcase
	if [ -d $CURRENT_FOLDER ];
	then
		rm -rf $CURRENT_FOLDER
	fi
	mkdir $CURRENT_FOLDER

	cd $CURRENT_FOLDER
#echo "-shell_filepath "$CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValueShell"
	echo "-----periodGenerator-----"
	CR=`echo ${TR[$testcase]} | tr : " "`
	echo $CR
	java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
		-Djava.security.policy=$PADRES_HOME/etc/java.policy\
		ca.utoronto.msrg.padres.demo.eventgenerator.PeriodGenerator\
		-publisher $NO_CLASS \
		-periodLow 3 \
		-periodHigh $RANK \
		-degree_co $CR \
		-average 14 \
		-filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriod" \
		-shell_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriodShell" 

	echo "-----valueGenerator-----"
	/bin/bash $EXP_HOME/script/valueGenerator.sh \
		-publisher $NO_CLASS \
		-value $NUM_VALUE \
		-filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValue" \
		-shell_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValueShell"

	#PUB_FOLDER=pub
	#if [ -d "$PUB_FOLDER" ];
	#then
	#	rm -rf $PUB_FOLDER
	#fi
	#mkdir $PUB_FOLDER
	#for (( i=0;i<$NO_CLASS;i++ ))
	#do
	#	/bin/bash $EXP_HOME/script/pubGenerator.sh -number $i -num_event $NO_EVENT -num_value $NUM_VALUE -cur_folder $PUB_FOLDER
	#	echo "/bin/bash $EXP_HOME/script/pubGenerator.sh -number $i -num_event $NO_EVENT -num_value $NUM_VALUE -cur_folder $PUB_FOLDER"
	#done
	#PUB_FILE_PATH=$CUR_EXP_PATH"/$CURRENT_FOLDER""/$PUB_FOLDER"
	#echo "-----pubGenerator-----"
	#echo "$PUB_FILE_PATH"

	#SUB_FOLDER=sub
	#if [ -d "$SUB_FOLDER" ];
	#then
	#	rm -rf $SUB_FOLDER
	#fi
	#mkdir $SUB_FOLDER

	echo "-----subBrokerAssigner-----"

	/bin/bash $EXP_HOME/script/subBrokerAssigner.sh \
		-broker_size $SUB_BROKER_SIZE \
		-period_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriod" \
		-value_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValue" \
		-printpath_prefix $CUR_EXP_PATH"/"$CURRENT_FOLDER \
		-policy "BASE"

	echo "-----brokerAssigner-----"

	/bin/bash $EXP_HOME/script/assignClassToBroker.sh \
		-broker_size $PUB_BROKER_SIZE \
		-period_filepath $CUR_EXP_PATH"/$CURRENT_FOLDER""/classPeriod" \
		-value_filepath $CUR_EXP_PATH"/$CURRENT_FOLDER""/classValue" \
		-printpath_prefix $CUR_EXP_PATH"/"$CURRENT_FOLDER \
		-policy "BASE"  > brokerStatus
	
	echo "-----experiments-----"
	echo "Subscriber Running Time $SUB_ETIME"
	CURRENT_EXP_FOLDER=`pwd`
	/bin/bash $EXP_HOME/script/experiments.sh \
	       	-num_run $NO_RUN \
	       	-w $WAIT_TIME \
		-sub_e $SUB_ETIME \
		-pub_e $PUB_ETIME \
		-pub_broker $PUB_BROKER_SIZE \
		-sub_broker $SUB_BROKER_SIZE \
		-pub_filepath $PUB_FILE_PATH \
		-sub_filepath $SUB_FILE_PATH \
		-adv_time 30 \
		-current_exp_folder $CURRENT_EXP_FOLDER


	echo "SUB_ETIME : $SUB_ETIME"
	echo ""

	/bin/bash $EXP_HOME/script/broker_down.sh

	cd ..
done
