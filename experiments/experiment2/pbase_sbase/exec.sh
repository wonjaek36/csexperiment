#!/bin/bash
#NO_CLASS=400
NUM_VALUE=30
NO_EVENT=6000
RANK=13

PUB_BROKER_SIZE=6
SUB_BROKER_SIZE=4
CASE_START=31
CASE_NUM=45

NO_RUN=1
PUB_SIZE=1
WAIT_TIME=50
#SUB_ETIME=5400 //deprecated
PUB_ETIME=3000
#NUM_VALUE=1
ADV_TIME=30
SUB_TIME=100
FOLDER_PREFIX="case_"
PUB_FILE_PATH=""
SUB_FILE_PATH=""
CUR_EXP_PATH=$(pwd)
#./scrum3/padres
echo "$CUR_EXP_PATH"

echo "-----For testcase-----"
for (( testcase=$CASE_START;testcase<=$CASE_NUM;testcase++ ))
do
	let NO_CLASS="1000+$testcase*100"
	CURRENT_FOLDER=$FOLDER_PREFIX$testcase
	if [ -d $CURRENT_FOLDER ];
	then
		rm -rf $CURRENT_FOLDER
	fi
	mkdir $CURRENT_FOLDER

	cd $CURRENT_FOLDER
#echo "-shell_filepath "$CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValueShell"
	echo "-----periodGenerator-----"
	java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
		-Djava.security.policy=$PADRES_HOME/etc/java.policy\
		ca.utoronto.msrg.padres.demo.eventgenerator.PeriodGenerator\
		-publisher $NO_CLASS \
		-periodLow 1 \
		-periodHigh $RANK \
		-degree_co 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 0.07 \
		-average 14 \
		-filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriod" \
		-shell_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriodShell" 

	echo "-----valueGenerator-----"
	/bin/bash $EXP_HOME/script/valueGenerator.sh \
		-publisher $NO_CLASS \
		-value $NUM_VALUE \
		-filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValue" \
		-shell_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValueShell"

	#PUB_FOLDER=pub
	#if [ -d "$PUB_FOLDER" ];
	#then
	#	rm -rf $PUB_FOLDER
	#fi
	#mkdir $PUB_FOLDER
	#for (( i=0;i<$NO_CLASS;i++ ))
	#do
	#	/bin/bash $EXP_HOME/script/pubGenerator.sh -number $i -num_event $NO_EVENT -num_value $NUM_VALUE -cur_folder $PUB_FOLDER
	#	echo "/bin/bash $EXP_HOME/script/pubGenerator.sh -number $i -num_event $NO_EVENT -num_value $NUM_VALUE -cur_folder $PUB_FOLDER"
	#done
	#PUB_FILE_PATH=$CUR_EXP_PATH"/$CURRENT_FOLDER""/$PUB_FOLDER"
	#echo "-----pubGenerator-----"
	#echo "$PUB_FILE_PATH"

	#SUB_FOLDER=sub
	#if [ -d "$SUB_FOLDER" ];
	#then
	#	rm -rf $SUB_FOLDER
	#fi
	#mkdir $SUB_FOLDER

	echo "-----subBrokerAssigner-----"

	/bin/bash $EXP_HOME/script/subBrokerAssigner.sh \
		-broker_size $SUB_BROKER_SIZE \
		-period_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classPeriod" \
		-value_filepath $CUR_EXP_PATH"/"$CURRENT_FOLDER"/classValue" \
		-printpath_prefix $CUR_EXP_PATH"/"$CURRENT_FOLDER \
		-policy "BASE" > subBrokerStatus

	echo "-----brokerAssigner-----"

	/bin/bash $EXP_HOME/script/assignClassToBroker.sh \
		-broker_size $PUB_BROKER_SIZE \
		-period_filepath $CUR_EXP_PATH"/$CURRENT_FOLDER""/classPeriod" \
		-value_filepath $CUR_EXP_PATH"/$CURRENT_FOLDER""/classValue" \
		-printpath_prefix $CUR_EXP_PATH"/"$CURRENT_FOLDER \
		-policy "BASE"  > brokerStatus
	
	echo "-----experiments-----"
#echo "Subscriber Running Time $SUB_ETIME"
	CURRENT_EXP_FOLDER=`pwd`
	/bin/bash $EXP_HOME/script/experiments.sh \
	       	-num_run $NO_RUN \
	       	-w $WAIT_TIME \
		-pub_e $PUB_ETIME \
		-pub_broker $PUB_BROKER_SIZE \
		-sub_broker $SUB_BROKER_SIZE \
		-pub_filepath $PUB_FILE_PATH \
		-sub_filepath $SUB_FILE_PATH \
		-adv_time $ADV_TIME \
		-sub_time $SUB_TIME \
		-current_exp_folder $CURRENT_EXP_FOLDER
#-sub_e $SUB_ETIME \ deprecated


#echo "SUB_ETIME : $SUB_ETIME" deprecated
	echo ""

	/bin/bash $EXP_HOME/script/broker_down.sh

	cd ..
done
