#!/bin/bash

USAGE="Usage: $(basename $0) -uri broker_uri [-n neightbor broker] [-help] [-Xms <mem_min>] [-Xmx <mem_max>] ..."


# function to check whether a parameter is given to an option
check_arg_missing()
{
	if ( [[ $2 == -* ]] || [ -z $2 ] ); then
		echo "Option $1 requires a parameter"
		echo $USAGE
		exit 1
	fi
}



# check and set the PADRES_HOME environment value
if [ -z "$PADRES_HOME" ]
then
	PADRES_HOME="$(cd $(dirname "$0")/.. && pwd)"
	export PADRES_HOME
fi

ARGS=$*        # all arguments
MEMORY_MIN=512 # java memory min size
MEMORY_MAX=7168 # java memory max size
BROKER_URI=""  # broker's uri
START_BG="&"   # start background
PATHSEP=":"    # ?

BROKER_OPT=()

while [ $# -gt 0 ]; do
	case "$1" in 
		( -Xms )
			check_arg_missing $1 $2
			MEMORY_MIN="$2"
			shift 2 ;;
		( -Xmx )
			check_arg_missing $1 $2
			MEMORY_MAX="$2"
			shift 2 ;;
		( -hostname ) 
			check_arg_missing $1 $2
			ADDR="$2"
			shift 2 ;;
		( -uri )
			check_arg_missing $1 $2
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			BROKER_URI="$2"
			shift 2 ;;
		( -cli )
			echo -n "THIS IS NOT CLIENT"
			exit ;;
		( -help ) 
			echo -n "HELP IS NOT SUPPORTED"
			exit ;;
		( * ) 
			BROKER_OPT=( ${BROKER_OPT[@]} $1 )
			shift 1 ;;
	esac
done

if [ -z $BROKER_URI ]; then
	echo "You must specify a broker URI";
	echo $USAGE
	exit 1
fi


# check whether the hostname is given; if not, exit
#if [ -z $ADDR ]; then
#	echo "Please specify hostname (could not auto assign hostname)";
#	echo $USAGE
#	exit 1
#fi
#We do not use RMI

CMD="java -Xms${MEMORY_MIN}m -Xmx${MEMORY_MAX}m\
 -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
 -Djava.security.policy=$PADRES_HOME/etc/java.policy\
 ca.utoronto.msrg.padres.broker.brokercore.BrokerCore ${BROKER_OPT[@]}"

CMD=$CMD" "$START_BG

echo $CMD
$CMD &
