#!/bin/bash

USAGE="Usage: $(basename $0) [-help] [-b <broker uri>] [-i <client id>] [-e <event time>] [-f <filename>]"

ARGS=$*
P_OPT=()

while [ $# -gt 0 ]; do
	case "$1" in
	( * )
		P_OPT=( "${P_OPT[@]}" $1 )
		shift 1 ;;	
	esac
done

java -Xms16m -Xmx512m\
	-cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	ca.utoronto.msrg.padres.demo.eventgenerator.EventSubscriber ${P_OPT[@]}

