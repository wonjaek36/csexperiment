PREFIX="java -cp $CLASSPATH:$PADRES_HOME/build/dependency/*:$PADRES_HOME/build/padres-2.1.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	ca.utoronto.msrg.padres.demo.eventgenerator.SubGenerator"

OPTION1="-n 2 -a 5 -c(0) c0 -v(0) v0 -t(0) discrete -l(0) 0 -h(0) 5 -c(1) c1 -v(1) v1 -t(1) discrete -l(1) 0 -h(1) 5"
FILE1="./data/subTest"

COMMAND=$PREFIX" "$OPTION1

$COMMAND > $FILE1
