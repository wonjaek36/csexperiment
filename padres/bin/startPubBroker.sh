#!/bin/bash
BROKER_OPT=()

echo "---startPubBroker.sh---"
PADRES_HOME="/home/apl/padres"
export PADRES_HOME
while [ $# -gt 0 ]; do
	case "$1" in
		( -Xmx )
			MEMORY_MAX="$2"
			shift 2;;
		( -broker_number )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -uri )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -sync_time )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -exec_time )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -info_filepath )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -sub_wait_time )
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( -n ) 
			BROKER_OPT=( ${BROKER_OPT[@]} $1 $2 )
			shift 2;;
		( * )
			echo "Invalid parameter"
			echo "$1 $2"
			exit;
	esac
done

BROKER_OPT=( ${BROKER_OPT[@]} -ll logFile )
echo "java -Xms512m -Xmx${MEMORY_MAX}m -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar -Djava.security.policy=$PADRES_HOME/etc/java.policy ca.utoronto.msrg.padres.broker.brokercore.BrokerCore ${BROKER_OPT[@]}"

java -Xms512m -Xmx${MEMORY_MAX}m\
		-cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar \
		-Djava.security.policy=$PADRES_HOME/etc/java.policy\
		ca.utoronto.msrg.padres.broker.brokercore.BrokerCorePubTest ${BROKER_OPT[@]} -ll 1

