#!/bin/bash

USAGE="Usage: $(basename $0) [-help] [-c <class name>] [-v <variable name>] [-t <variable type>] [-n no of event]"

# get the commnad line arguments
ARGS=$*

CLASS_ID=""
VARI_ID=""
TYPE=""
NUMBER=""
FILEPATH=""

P_OPT=()
# Parse command line arguments

while [ $# -gt 0 ]; do
	case "$1" in
	( -l ) 
		P_OPT=( "${P_OPT[@]}" $1 $2 )	
		shift 2 ;;
	( -h )
		P_OPT=( "${P_OPT[@]}" $1 $2 )
		shift 2 ;;
	( -c )
		CLASS_ID=$2;
		shift 2 ;;
	( -v )
		VARI_ID=$2;
		shift 2 ;;
	( -t )
		TYPE=$2;
		shift 2 ;;
	( -n )
		NUMBER=$2;
		shift 2 ;;	
	( -f )
		FILEPATH=$2;
		shift 2 ;;
	esac
done

if [ -z $FILEPATH ]; then
	echo "Please specify the file path";
	echo $USAGE
	exit 1
fi

java -cp $CLASSPATH:$PADRES_HOME/build/dependency/*:$PADRES_HOME/build/padres-2.1.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	ca.utoronto.msrg.padres.demo.eventgenerator.PubGenerator -c $CLASS_ID -v $VARI_ID -t $TYPE -n $NUMBER ${P_OPT[@]} > $FILEPATH
