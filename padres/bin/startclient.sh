#!/bin/bash

USAGE="Usage: $(basename $0) [-help] [-Xms <mem_min>] [-Xmx <mem_max>] \
[-cp <classpath>] [-host <client_ip>] [-client <classname>] [-i <client_id>] \
[-b <broker_uri>] [-cli] [-web] [-c <config_file>] [OPTIONS]..."

# function to check whether a parameter is given to an option
check_arg_missing()
{
    if ( [[ $2 == -* ]] || [ -z $2 ] ); then
	echo "Option $1 requires a parameter"
	echo $USAGE
	exit 1
    fi
}

# get the command line arguments
ARGS=$*

# default values
MEMORY_MIN=16
MEMORY_MAX=64
CLIENT="ca.utoronto.msrg.padres.tools.cliclient.CLIClient"
CLIENT_PATH=""
CLIENT_ID=""
CLIENT_IP=""
BROKER_URI=""
#BG_PROCESS="&" not support

CLIENT_OPT=()

# Parse command line arguments
while [ $# -gt 0 ]; do
    case "$1" in
	( -Xms )
	   check_arg_missing $1 $2;
	   MEMORY_MIN=$2; 
	   shift 2 ;;
	( -Xmx )
	   check_arg_missing $1 $2
	   MEMORY_MAX=$2; 
	   shift 2 ;;
	( -cp ) # classpath
	   check_arg_missing $1 $2 CLIENT_PATH=$2
	   shift 2 ;;
	( -host ) #???
	   check_arg_missing $1 $2
	   CLIENT_IP=$2; 
	   shift 2 ;;
	( -client ) #
	   check_arg_missing $1 $2
	   CLIENT=$2; 
	   shift 2 ;;
	( -help )
	   cat ${PADRES_HOME}/bin/docs/startclient.help; 
	   exit 0;;
	( -cli ) #default value
	   CLIENT="ca.utoronto.msrg.padres.tools.cliclient.CLIClient"
	   BG_PROCESS=""
	   shift 1;;
	( -web )
	   CLIENT="ca.utoronto.msrg.padres.tools.webclient.WebClient"
	   shift 1;;
	( -i ) #Client name
	   check_arg_missing $1 $2
	   CLIENT_OPT=( "${CLIENT_OPT[@]}" $1 $2)
	   CLIENT_ID=$2; 
	   shift 2 ;;
	( -b ) #Broker connected with me
	   check_arg_missing $1 $2
	   CLIENT_OPT=( "${CLIENT_OPT[@]}" $1 $2)
	   BROKER_URI=$2; 
	   shift 2 ;;
	( * )
	   CLIENT_OPT=( "${CLIENT_OPT[@]}" $1)
	   shift 1 ;;
    esac
done

if [ -z $CLIENT_ID ]; then
	echo "Please specify the client id";
	echo $USAGE
	exit 1
fi

CMD="java -Xms${MEMORY_MIN}m -Xmx${MEMORY_MAX}m\
	-cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	$CLIENT ${CLIENT_OPT[@]}"

echo "option : ${CLIENT_OPT[@]}"
echo $CMD

$CMD

