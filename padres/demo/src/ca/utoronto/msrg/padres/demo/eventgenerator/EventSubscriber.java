package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;
import java.io.IOException;
import java.math.BigInteger;

import ca.utoronto.msrg.padres.client.Client;
import ca.utoronto.msrg.padres.client.ClientConfig;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.Publication;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.common.util.LogException;
import ca.utoronto.msrg.padres.common.util.LogSetup;
public class EventSubscriber extends Client{
	public static String parameterError = 
			  "-b brokerURI "
			+ "-i client id "	
			+ "-w wating time(second) "
			+ "-e event time(second) "
			+ "-f file name to read subscribe";
	
	public static String LOG_FILE_PATH = System.getProperty("user.dir") + File.separator +  "sub_log";
	public static String logError = "Log Exception Occured in Setup";
	public static final Pattern CLASSID_PATTERN 	= Pattern.compile("-c[(]([0-9]+)[)]");
	public static final Pattern VARIALBE_PATTERN 	= Pattern.compile("-v[(]([0-9]+)[)]");
	public static final Pattern TYPE_PATTERN 		= Pattern.compile("-t[(]([0-9]+)[)]");
	public static final Pattern HIGHVALUE_PATTERN 	= Pattern.compile("-h[(]([0-9]+)[)]");
	public static final Pattern LOWVALUE_PATTERN 	= Pattern.compile("-l[(]([0-9]+)[)]");
	
	String id = null;
	String brokerURI = null;
	Integer waitingTime;	// default value (5 seconds)
	Integer eventTime;	// default value (600 seconds  = 10 minutes)
	Integer cntReceivedMessage = 0;
	Long firstPubMessageSend = 0L;
	Long lastPubMessageReceive = 0L;
	static boolean sanityCheck = false; // deprecated
	
	Double highestThroughput = 0.0;
	Long highestThroughputTime = 0L;
	String filePath;
	Long latency;
	BigInteger delaySum;
	
	public EventSubscriber(ClientConfig newConfig, String i, String b,
			 Integer et, String fp)
			throws ClientException, ParseException {
		
		super(newConfig);
		id = i;
		brokerURI = b;
		eventTime = et;
		filePath = fp;
		latency = 0L;
		delaySum = new BigInteger("0");
	}
	
	public void processMessage(Message msg) {
		
		if (msg instanceof PublicationMessage) {
			
			cntReceivedMessage += 1;
			Publication pub = ((PublicationMessage) msg).getPublication();
			long currTime = System.currentTimeMillis();
			long timestamp = pub.getTimeStamp().getTime();
			long delay = currTime - timestamp;
			if ( firstPubMessageSend == 0L ) {
				firstPubMessageSend = timestamp;
			} if ( lastPubMessageReceive < currTime ) {
				lastPubMessageReceive = currTime;
			}
			
			double currentThroughput = 1.0 / (double)(currTime-timestamp );
			if( highestThroughput < currentThroughput ) { 
				highestThroughput = currentThroughput;
				highestThroughputTime = currTime;
			}
			System.out.print(pub);
			System.out.printf("\tPublication Message[%s][%dth]: ",pub.getPubID(), cntReceivedMessage.intValue());
			System.out.println("[S]" + timestamp + "   [R]" + currTime + "   [D]" + delay + "  ");
			BigInteger bigDelay = BigInteger.valueOf(delay);
			delaySum = delaySum.add(bigDelay);
			
			//System.out.println(pub);
		} 
		if (msg instanceof SubscriptionMessage) {
			System.out.print("Subscription Message: \n");
		}
	}
	public void subscribeEvent() throws ParseException {
		long readStartTime = System.currentTimeMillis();
		int subCnt = 0;
		try {
			@SuppressWarnings("resource")
			BufferedReader input = new BufferedReader(new FileReader(filePath));
			String line = input.readLine();
			
			while ((line = input.readLine()) != null) {
				handleCommand(line);
				subCnt += 1;
		    }
			
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		long readEndTime = System.currentTimeMillis();
		
		if ( sanityCheck ) return; // for Sanity Check
		
		System.out.println("Time: " + (readEndTime - readStartTime) );
		System.out.println("Number of " + subCnt + " Handle all subscription");
		
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				
				shutdown();
				timer.cancel();
			}
		}, (eventTime) * 1000);
	}
	
	public void shutdown() {
		//Throughput : processed message per second
		//Latency : time per msg
		
		System.err.println("Done SubScribe");
		System.err.printf("Terminating the client %s\n\n", clientID);
		//Received msg, firstPubMessageTime, secondPubMessageTime, Total Latency, Highest Throughput,  Highest Throughput Time
		System.out.printf(cntReceivedMessage + ", " + firstPubMessageSend + ", " + lastPubMessageReceive + ", " +
				delaySum.toString() + ", " + highestThroughput + ", " + highestThroughputTime );
		
		/*System.out.printf("Total receive pub message: %d\n", cntPubMessage);
		System.out.printf("FirstMessage Send: %d\n", firstPubMessageSend);
		System.out.printf("LastMessage Receive: %d\n", lastPubMessageReceive);
		System.out.printf("Throughtput : %d[%d s] / %d\n",
				lastPubMessageReceive-firstPubMessageSend,  (lastPubMessageReceive-firstPubMessageSend)/1000, cntPubMessage);
		if( cntPubMessage != 0 )
			System.out.printf("Average Latency: %s / %d = %d\n",delaySum.toString(), cntPubMessage, delaySum.divide( BigInteger.valueOf(cntPubMessage) ) );*/

		try {
			super.shutdown();
		} catch (ClientException e) {
			System.err.println(e.getMessage());
		}
		System.exit(0);
	}
	
	public static void main(String args[]) {
		
		String id = null;
		String brokerURI = null;
		Integer eventTime = 600;	// default value (600 seconds  = 10 minutes)
		String filePath = "subTest";
		
		
		try {
			for(int i=0;i<args.length;i++) {
			
				if( args[i].compareTo("-b") == 0 ) { // 1. broker URI
					brokerURI = args[++i];
				} else if( args[i].compareTo("-i") == 0 ) { // 2. Client ID
					id = args[++i];
				} else if( args[i].compareTo("-e") == 0 ) { // 5. eventTime(totalTime)
					eventTime = Integer.parseInt( args[++i] );
				} else if( args[i].compareTo("-f") == 0 ) {
					filePath = args[++i];
					filePath = filePath.replaceFirst("^~",System.getProperty("user.home"));
				} else if( args[i].compareTo("-s") == 0 ) {
					sanityCheck = true;
				} else {
					throw new Exception();
				}
			}
		} catch(Exception e){
			System.err.println("Parsing Error Occured");
			String exec="";
			System.err.println(parameterError);
			for( String para : args ) {
				exec += para + " ";
			}
			System.err.println( exec );
			e.printStackTrace();
			return;
		}
		
		try { 
			new LogSetup(LOG_FILE_PATH);
		} catch (LogException e) { 
			e.printStackTrace();
			return;
		}
		
		try {
			ClientConfig newConfig = new ClientConfig();
			newConfig.clientID = id;
			newConfig.connectBrokerList = new String[1];
			newConfig.connectBrokerList[0] = brokerURI;
			EventSubscriber evS = new EventSubscriber(newConfig, id, brokerURI,
					eventTime, filePath);

			evS.subscribeEvent();
			
		} catch (ClientException e) {
			e.printStackTrace();
			return;
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

}
