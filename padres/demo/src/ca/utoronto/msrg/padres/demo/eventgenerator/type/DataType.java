package ca.utoronto.msrg.padres.demo.eventgenerator.type;

import java.util.Random;

import ca.utoronto.msrg.padres.demo.exception.TypeException;

public class DataType {

	enum VariableType{
		BOOLEAN, DISCRETE, RANGE
	}
	VariableType type;
	Number lowValue = null, highValue = null;
	
	public void setType(String str){
		if( str.compareTo("boolean") == 0 ){
			type = VariableType.BOOLEAN;
		} else if( str.compareTo("discrete") == 0 ){
			type = VariableType.DISCRETE;
		} else if( str.compareTo("range") == 0 ){
			type = VariableType.RANGE;
		}
	}
	
	public String getType() throws TypeException {
		switch( type ) {
		case BOOLEAN:
			return "boolean";
		case DISCRETE:
			return "discrete";
		case RANGE:
			return "range";
		default:
			throw new TypeException();
		}
	}
	public void setLowValue(String value)
			throws TypeException {
		
		if( value.compareTo("inf") == 0 ) return;
		switch( type ) {
		case BOOLEAN:
			throw new TypeException();
		case DISCRETE:
			lowValue = Integer.parseInt(value); break;
		case RANGE:
			lowValue = Double.parseDouble(value); break;
		default:
			throw new TypeException();
		}
	}
	
	public void setHighValue(String value)
			throws TypeException {
		
		if( value.compareTo("inf") == 0 ) return;
		switch( type ) {
		case BOOLEAN:
			throw new TypeException();
		case DISCRETE:
			highValue = Integer.parseInt(value); break;
		case RANGE:
			highValue = Double.parseDouble(value); break;
		default:
			throw new TypeException();
		}
	}
	
	public Number getLowValue() {
		return lowValue;
	}
	public Number getHighValue() {
		return highValue;
	}
	
	public Number getRandomValue()
		throws TypeException {
		Random ran = new Random();
		
		switch( type ) {
		case BOOLEAN:
			throw new TypeException();
		case DISCRETE:
			return ran.nextInt( (Integer)highValue - (Integer)lowValue - 1) + (Integer)lowValue + 1;
		case RANGE:
			return ran.nextDouble() * ((Double)highValue - (Double)lowValue) + (Double)lowValue;
		default:
			throw new TypeException();
		}
	}
}