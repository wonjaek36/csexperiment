package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner.*;

public class SubscriptionAssigner {

	private HashMap<String, String> options;
	private HashMap<String, Assigner> assigners;
	
	static final String BROKER_SIZE_FLAG = "broker_size";
	static final String PERIOD_FILE_PATH_FLAG = "period_filepath";
	static final String VALUE_FILE_PATH_FLAG = "value_filepath";
	static final String SUBSCRIBER_SIZE_FLAG = "subscriber_size";
	static final String PRINT_PATH_PREFIX_FLAG = "printpath_prefix";
	static final String POLICY_FLAG = "policy";
	
	ArrayList<Integer> periods;
	ArrayList<Integer> values;
	Assigner assigner;

	public SubscriptionAssigner(String[] args) {
		
		options = new HashMap<String, String>();
		assigners = new HashMap<String,Assigner>();
		
		options.put(BROKER_SIZE_FLAG, 		null);
		options.put(PERIOD_FILE_PATH_FLAG, 	null);
		options.put(VALUE_FILE_PATH_FLAG, 	null);
		options.put(SUBSCRIBER_SIZE_FLAG, 	null);
		options.put(PRINT_PATH_PREFIX_FLAG, null);
		options.put(POLICY_FLAG, 			null);
		
		for(int i=0;i<args.length;i++) {
			String arg = args[i];
			if( arg.startsWith("-") ) {
				String optionName = arg.substring(1);
				if( options.containsKey(optionName) ) {
					options.put(optionName, args[++i]);
				}
			}
		}
		
		Integer brokerSize = Integer.parseInt(options.get(BROKER_SIZE_FLAG));
		assigners.put("BASE", 	new BaseAssigner(brokerSize) ); //only care about number of subscription
		assigners.put("LB",		new LoadBalanceAssigner(brokerSize) );
	}
	
	public void inputPublisherStatus(String periodFilePath, String valueFilePath) {
		periods = new ArrayList<Integer>();
		values = new ArrayList<Integer>();
		
		try {
			Scanner periodScanner = new Scanner(new File(periodFilePath));
			
			int size = periodScanner.nextInt();
			for(int i=0;i<size;i++){
				periods.add(periodScanner.nextInt());
			}
			
			Scanner valueScanner = new Scanner(new File(valueFilePath));
			
			size = valueScanner.nextInt();
			for(int i=0;i<size;i++) {
				values.add(valueScanner.nextInt());
			}
			
			valueScanner.close();
			periodScanner.close();
			
		} catch(FileNotFoundException e) {
			
			e.printStackTrace();
			System.out.println("Cannot read Information in inputClassStatus");
			System.exit(1);
		}
	}
	
	public void assignSubscriptionToSubscriber() {
		
		String curPolicy = this.getPolicy();
		assigner = assigners.get(curPolicy);
		assigner.assignSubscriptionToSubscriber(periods, values);
	}
	
	public void outputSubscriber(String printPathPrefix) {
		for(int i=0;i<getBrokerSize();i++) {
			String print_path_prefix = getPrintPathPrefix() + File.separator;
			String filename = "subInformation" + (i+1);
			String filepath = print_path_prefix + filename;
			ArrayList< ArrayList<CompositeSubscription> > subscribers = assigner.getSubscribers();
			try { 
				//
				BufferedWriter bw = new BufferedWriter(new FileWriter(filepath));
				ArrayList<CompositeSubscription> compositeSubscriptions = subscribers.get(i);
				bw.write(compositeSubscriptions.size() * 2 + " ");
				bw.newLine();
				for(int j=0;j<compositeSubscriptions.size();j++) {
					CompositeSubscription cs = compositeSubscriptions.get(j);
					bw.write(cs.number[0] + " ");
					bw.write(cs.number[1] + " ");
				}
				bw.newLine();
				for(int j=0;j<compositeSubscriptions.size();j++) {
					CompositeSubscription cs = compositeSubscriptions.get(j);
					bw.write(cs.m_values[0] + " ");
					bw.write(cs.m_values[1] + " ");
				}
				bw.close();
			} catch(Exception e) {
				System.err.println("outputSubscriber ERROR: Cannot create subInformation file");
				e.printStackTrace();
			}
		}
	}
	
	public void outputSubscriberStatus() {
		
		ArrayList< ArrayList<CompositeSubscription> > subscribers = assigner.getSubscribers();
		double[] loads = assigner.getLoads();
		
		for(int i=0;i<subscribers.size();i++){
			System.out.print((i+1) + "th sub-broker loads[" + loads[i] + "]: ");
			ArrayList<CompositeSubscription> subscriber = subscribers.get(i);
			
			for(int j=0;j<subscriber.size();j++) {
				CompositeSubscription cs = subscriber.get(j);
				System.out.print(cs.number[0] + " " + cs.number[1] + " ");
			}
			System.out.println();
		}
	}
	
	public Integer getBrokerSize() {
		return Integer.parseInt(options.get(BROKER_SIZE_FLAG));
	}
	public String getPeriodFilePath() {
		return options.get(PERIOD_FILE_PATH_FLAG);
	}
	public String getValueFilePath() {
		return options.get(VALUE_FILE_PATH_FLAG);
	}
	public String getPrintPathPrefix() {
		return options.get(PRINT_PATH_PREFIX_FLAG);
	}
	public String getPolicy() {
		return options.get(POLICY_FLAG);
	}
	public static void main(String args[]) {
		
		SubscriptionAssigner sa = new SubscriptionAssigner(args);
		sa.inputPublisherStatus(sa.getPeriodFilePath(), sa.getValueFilePath());
		sa.assignSubscriptionToSubscriber();
		sa.outputSubscriber(sa.getPrintPathPrefix());
		
		sa.outputSubscriberStatus();
	}
}
