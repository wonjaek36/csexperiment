package ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner;

import java.util.ArrayList;
import java.util.Collections;

public class LoadBalanceAssigner extends Assigner {

	public int findMinLoadBroker() {
		double min = loads[0];
		int pos=0;
		for(int i=1;i<brokerSize;i++) {
			if( min > loads[i] ){
				min = loads[i];
				pos = i;
			}
		}
		return pos;
	}
	public LoadBalanceAssigner(Integer brokerSize) {
		super(brokerSize);
	}
	public void assignSubscriptionToSubscriber(ArrayList<Integer> periods, ArrayList<Integer> values) {
		ArrayList<CompositeSubscription> CompositeSubscriptions = new ArrayList<CompositeSubscription>();
		
		int size = periods.size();
		for(int i=0;i<size;i+=2) {
			CompositeSubscription aCompositeSubscription = new CompositeSubscription(
					i, i+1,
					1.0 / periods.get(i) + 1.0 / periods.get(i+1), 
					values.get(i), values.get(i+1) );
			
			CompositeSubscriptions.add(aCompositeSubscription);
		}
		
		
		//System.out.println( CompositeSubscriptiones.get(0).m_period + " " + CompositeSubscriptiones.get(1).m_period );

		size = CompositeSubscriptions.size();
		for(int i=0;i<size;i++) {
			
			int selectedSub = findMinLoadBroker();
			ArrayList<CompositeSubscription> subscriber = subscribers.get(selectedSub);
			subscriber.add( CompositeSubscriptions.get(i) );
			
			loads[selectedSub] += CompositeSubscriptions.get(i).getPeriod();
		}
	}

}
