package ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner;

import java.util.ArrayList;

public class Subscriber {
	ArrayList<CompositeSubscription> CompositeSubscriptions;
	double Load;
	public Subscriber() {
		CompositeSubscriptions = new ArrayList<CompositeSubscription>();
		Load = 0.0;
	}
	public double getLoad() {
		return Load;
	}
	public ArrayList<CompositeSubscription> getCompositeSubscriptiones() {
		return CompositeSubscriptions;
	}
	
	public void addCompositeSubscription(CompositeSubscription CompositeSubscription) {
		CompositeSubscriptions.add(CompositeSubscription);
		Load += (CompositeSubscription.getPeriod() );
	}
	
	public String toString() {
		String ret;
		ret = "Load " + Load + "\n";
		for( int i=0;i<CompositeSubscriptions.size();i++) {
			ret += (CompositeSubscriptions.get(i).toString() + "\n"); 
		}
		return ret;
	}
}