package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class ValueGenerator {

	private HashMap<String, String> options;
	static final String PUBLISHER_SIZE_FLAG = "publisher";
	static final String VALUE_FLAG = "value";
	static final String FILE_PATH_FLAG = "filepath";
	static final String SHELL_FILE_PATH_FLAG = "shell_filepath";
	
	int m_publisher;
	int m_value;
	ArrayList<Integer> values;
	
	public String getFilePath() {
		return options.get(FILE_PATH_FLAG);
	}
	
	public String getShellFilePath() {
		return options.get(SHELL_FILE_PATH_FLAG);
	}
	public ValueGenerator(String[] args) {
		options = new HashMap<String, String>();
		options.put(PUBLISHER_SIZE_FLAG, 	null);
		options.put(VALUE_FLAG, 			null);
		options.put(FILE_PATH_FLAG, 		null);
		options.put(SHELL_FILE_PATH_FLAG, 	null);
		
		for(int i=0;i<args.length;i++){
			String arg = args[i];
			if( arg.startsWith("-") ) {
				String optionName = arg.substring(1);
				options.put(optionName, args[++i]);
			}
		}
		
		m_publisher = Integer.parseInt(options.get(PUBLISHER_SIZE_FLAG));
		m_value = Integer.parseInt(options.get(VALUE_FLAG));
		values = new ArrayList<Integer>();
	}
	
	public void allocateValues() {
		
		for(int i=0;i<m_publisher;i++) {
			values.add(m_value);
		}
	}
	
	public void printValues(String filepath) {
		
		try { 
			PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter(filepath)));
			
			int size = values.size();
			output.println(size);
			for(int i=0;i<size;i++) {
				output.print(values.get(i) + " ");
			}
			output.println();
			output.flush();
			output.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printShellValues(String shellFilepath) {
		try {
			PrintWriter output = new PrintWriter( new BufferedWriter(new FileWriter(shellFilepath)));
			
			int size = values.size();
			output.print("VALUES=( ");
			for(int i=0;i<size;i++) {
				output.print(values.get(i) + " ");
			}
			output.println(")");
			
			output.flush();
			output.close();
		} catch(Exception e){
			System.err.println("Fail to open file");
		}
	}
	
	public static void main(String[] args) {
		
		ValueGenerator valueGenerator = new ValueGenerator(args);
		valueGenerator.allocateValues();
		valueGenerator.printValues(valueGenerator.getFilePath());
		//valueGenerator.printShellValues(valueGenerator.getShellFilePath());
	}
}
