package ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner;

import java.util.ArrayList;
import java.util.Collections;

public class VerticalAssigner extends Assigner {

	public VerticalAssigner(Integer bs, boolean icc) {
		super(bs, icc);
	}

	public void assignPublisherToBroker(ArrayList<Integer> periods, ArrayList<Integer> values) {
		
		/*brokers = new ArrayList< ArrayList<Integer> >();
		loads = new double[brokerSize];*/
		ArrayList<BrokerElement> elements = new ArrayList<BrokerElement>();
		/*for(int i=0;i<brokerSize;i++){
			ArrayList<Integer> broker = new ArrayList<Integer>();
			loads[i] = 0.0;
			brokers.add(broker);	
		}*/
		int periodSize = periods.size();
		for(int i=0;i<periodSize;i+=2) {
			
			if( isCareCo == false ) {
				double msgPerSec = 1.0/((double)periods.get(i)) + 1.0/((double)periods.get(i+1));
				BrokerElement brokerElement = new BrokerElement(i, i+1, msgPerSec);
				elements.add(brokerElement);
			}
			else if( periods.get(i) == periods.get(i+1)) {
				double msgPerSec = 1.0/((double)periods.get(i)) + 1.0/((double)periods.get(i+1));
				BrokerElement brokerElement = new BrokerElement(i, i+1, msgPerSec);
				elements.add(brokerElement);
			} else {
				double msgPerSec = 1.0/((double)periods.get(i));
				BrokerElement brokerElement = new BrokerElement(i, -1, msgPerSec);
				elements.add(brokerElement);
				
				msgPerSec = 1.0/((double)periods.get(i+1));
				brokerElement = new BrokerElement(i+1, -1, msgPerSec);
				elements.add(brokerElement);
			}
		}
		
		for(int i=0;i<elements.size();i++){
			
			BrokerElement element = elements.get(i);
			int pos = findMinLoadBroker();
			ArrayList<Integer> broker = brokers.get(pos);
			broker.add(element.getIndexFirst());
			loads[pos] += element.getMsgPerSec();
			if( element.getIndexSecond() > 0 ) {
				broker.add(element.getIndexSecond());
			}
		}
	}

}
