package ca.utoronto.msrg.padres.demo.eventgenerator;

import ca.utoronto.msrg.padres.demo.eventgenerator.type.DataType;

public class PubGenerator {

	String classId = null;
	String variableId = null;
	Integer alpha = 1; // default 1
	Integer cntPub = 0;
	
	DataType value = null;
	boolean firstElement;
	boolean assendingOrder;
	public PubGenerator(String ci, String vi, DataType dt, int n, boolean fe, boolean as) {
		
		classId = ci;
		variableId = vi;
		value = dt;
		cntPub = n;
		firstElement = fe;
		assendingOrder = as;
	}
	
	public String createRanPubMsg() throws Exception {
		String pubMessage = "";
		
		String type = value.getType(); // throw Exception
		
		if( type.compareTo("boolean") == 0 ){
			pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,TRUE]";
		} else if( type.compareTo("discrete") == 0 ) {
			pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,'"+value.getRandomValue()+"']";
		} else if( type.compareTo("range") == 0 ) {
			pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,"+value.getRandomValue()+"]";
		}
		else
			throw new Exception();
		
		return pubMessage;
	}
	public String createAscPubMsg(int idx) throws Exception {
		String pubMessage = "";
		String type = value.getType(); // throw Exception
		
		
		if( type.compareTo("boolean") == 0 ){
			pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,TRUE]";
		} else if( type.compareTo("discrete") == 0 ) {
			Integer s = (Integer)value.getHighValue() - (Integer)value.getLowValue() - 1;
			pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,'"+((Integer)value.getLowValue()+(idx%s) + 1)+"']";
		} else if( type.compareTo("range") == 0 ) { // deprecated
			throw new Exception();
			/*pubMessage += "p [class,'"+classId+"'],"+
					"[name,'"+variableId+"'],"+
					"[value,"+value.getRandomValue()+"]";*/
		}
		else
			throw new Exception();
		
		return pubMessage;
	}
	
	public String createAdvMsg() throws Exception {
		String advMessage = "";
		String type = value.getType();
		
		if( type.compareTo("boolean") == 0 ){
			
			advMessage += "a [class,eq,'"+classId+"'],"+
					"[name,eq,'"+variableId+"'],"+
					"[value,eq,TRUE]";
		} else if( type.compareTo("discrete") == 0 ) {
			for(int i=(Integer)value.getLowValue(); i<(Integer)value.getHighValue(); i++){
				if( i != (Integer)value.getLowValue() )
					advMessage += "\n";
				advMessage += "a [class,eq,'"+classId+"'],"+
						"[name,eq,'"+variableId+"'],"+
						"[value,eq,'"+i+"']";
			}
		} else if( type.compareTo("range") == 0 ) {
			advMessage += "a [class,eq,'"+classId+"'],"+
					"[name,eq,'"+variableId+"'],"+
					"[value,>=,"+value.getLowValue()+"]";
		}
		else
			throw new Exception();
		
		return advMessage;
	}
	
	public static void main( String[] args ) {
		
		String classId = null;
		String variableId = null;
		DataType dataType = new DataType();
		boolean firstelement = false;
		boolean assending = false;
		int n = 0;
		String errorStr = "";
		
		try {
			for(int i=0;i<args.length;i++) {
				
				errorStr = args[i];
				if( args[i].compareTo("-c") == 0 ) { // class id
					classId = args[++i];
				} else if( args[i].compareTo("-v") == 0 ) { //variable id
					variableId = args[++i];
				} else if( args[i].compareTo("-t") == 0 ) { //variable type
					dataType.setType(args[++i]);
				} else if( args[i].compareTo("-l") == 0 ) { //low variable ( if type is boolean, not accepted )
					dataType.setLowValue(args[++i]);
				} else if( args[i].compareTo("-h") == 0 ) { //high variable ( if type is boolean, not accepted )
					dataType.setHighValue(args[++i]);
				} else if( args[i].compareTo("-n") == 0 ) { //number of message
					n = Integer.parseInt(args[++i]);
				} else if( args[i].compareTo("--first") == 0 ) { // use first element ( default is false because of unknown bug )
					firstelement = true;
				} else if( args[i].compareTo("--assending") == 0 ){ // print message by assending order ( default is false (i.e. random ))
					assending = true;
				} else {
					throw new Exception();
				}
			}
		} catch(Exception e){
			System.err.println("Parsing Error Occured");
			System.err.println("args[i] : " + errorStr);
			e.printStackTrace();
			return;
		}
		
		try {
			PubGenerator pubGenerator = new PubGenerator(classId, variableId, dataType, n, firstelement, assending);

			System.out.println( pubGenerator.createAdvMsg() );
			System.out.println(n);
			
			if( assending == false ) {
				for(int i=0;i<n; i++ ){
					System.out.println( pubGenerator.createRanPubMsg() );
				}
			}
			if( assending == true ) {
				for(int i=0;i<n; i++ ){
					System.out.println( pubGenerator.createAscPubMsg(i) );
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
