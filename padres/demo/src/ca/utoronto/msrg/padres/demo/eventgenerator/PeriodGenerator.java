package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

public class PeriodGenerator {

	private HashMap<String, String> options;
	final static String FILE_PATH_FLAG = "filepath";
	final static String SHELL_FILE_PATH_FLAG = "shell_filepath";
	final static String PUBLISHER_SIZE_FLAG = "publisher";
	final static String PERIOD_LOW_FLAG = "periodLow";
	final static String PERIOD_HIGH_FLAG = "periodHigh";
	final static String DEGREE_CO_FLAG = "degree_co";
	final static String AVERAGE_FLAG = "average";
	
	private ArrayList<Double> co;
	ArrayList<Integer> m_periods;
	
	private Random rand = new Random(System.currentTimeMillis());
	
	public String getFilePath() {
		return (options.get(FILE_PATH_FLAG));
	}
	
	public PeriodGenerator(String[] args) {
		
		options = new HashMap<String, String>();
		options.put(FILE_PATH_FLAG, 		null);
		options.put(SHELL_FILE_PATH_FLAG, 	null);
		options.put(PUBLISHER_SIZE_FLAG, 	null);
		options.put(PERIOD_LOW_FLAG, 		null);
		options.put(PERIOD_HIGH_FLAG, 		null);
		options.put(AVERAGE_FLAG, 			null);
		
		co = new ArrayList<Double>();
		m_periods = new ArrayList<Integer>();
		
		for( int i=0;i<args.length;i++ ) {
			String arg = args[i];
			if( arg.startsWith("-") ) {
				String optionName = arg.substring(1);
				
				if( optionName.compareTo(DEGREE_CO_FLAG) == 0 ) {
					Integer period_low = Integer.parseInt(options.get(PERIOD_LOW_FLAG));
					Integer period_high = Integer.parseInt(options.get(PERIOD_HIGH_FLAG));
					
					for(int j=period_low; j<=period_high; j++ ) {
						co.add(Double.parseDouble(args[++i]));
					}
				}
				else {
					String value = args[i+1];
					if( options.containsKey(optionName) ) {
						options.put(optionName, value);
						i += 1;
					}
				}
			}
		}
		
	}//End of Constructor

	public void allocatePeriodValue() {

		Integer m_size = Integer.parseInt(options.get(PUBLISHER_SIZE_FLAG));
		Integer m_periodLow = Integer.parseInt(options.get(PERIOD_LOW_FLAG));
		Integer m_periodHigh = Integer.parseInt(options.get(PERIOD_HIGH_FLAG));
		
		int co_size = co.size();
		int currentSize = 0;
		for(int i=0;i<co_size;i++) {
			int vSize = (int)(m_size * co.get(i));
			int currentPeriod = i + m_periodLow;
			if( vSize % 2 == 1 ) {
				vSize -= 1;
			}
			currentSize += vSize;
			
			for(int j=0;j<vSize;j+=2) {
				m_periods.add(currentPeriod);
				m_periods.add(currentPeriod);
			}
		}
		
		for(int i=0;i<m_size-currentSize;i+=2) {
			Integer p1, p2;
			
			p1 = rand.nextInt(m_periodHigh - m_periodLow + 1) + m_periodLow;
			do {
				p2 = rand.nextInt(m_periodHigh - m_periodLow + 1) + m_periodLow;
			} while(p1 == p2);
			m_periods.add(p1);
			m_periods.add(p2);
		}

		//printPeriods();
	}
	
	public void shuffleAllocatedArray() {
		Integer m_size = Integer.parseInt(options.get(PUBLISHER_SIZE_FLAG));
		int q;
		
		for(int i=0;i<m_size;i+=2) {
			q = rand.nextInt( (i / 2)  + 1 );
			
			Collections.swap(m_periods, i, q*2);
			Collections.swap(m_periods, i+1, q*2+1);
		}
	}
	
	public void printPeriods() {
		
		try { 
			double avg=0.0;
			int sum = 0;
			int size = m_periods.size();
			System.out.println(size);
			for(int i=0;i<size;i++) {
				System.out.print(m_periods.get(i) + " ");
				sum += m_periods.get(i);
			}
			System.out.println();
			avg = ((double)sum) / ((double)size);
			System.out.println(avg);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printPeriods(String filepath) {
		
		try { 
			PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter(filepath)));
			
			int size = m_periods.size();
			double avg=0.0;
			int sum = 0;
			
			output.println(size);
			for(int i=0;i<size;i++) {
				output.print(m_periods.get(i) + " ");
				sum += m_periods.get(i);
			}
			output.println();
			avg = ((double)sum) / ((double)size);
			output.print(avg);
			output.println();
			output.flush();
			output.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printPeriodsForShell(String shellFilepath) {
		try {
			PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter(shellFilepath)));
			Integer m_size = Integer.parseInt(options.get(PUBLISHER_SIZE_FLAG));
			output.print("PERIODS=( ");
			for(int i=0;i<m_size;i++) {
				output.print(m_periods.get(i) + " ");
			}
			output.println(")");
			output.flush();
			output.close();
		} catch(Exception e){
			System.err.println("Fail to open file");
		}
	}

	public static void main(String[] args) {
	
		PeriodGenerator pg = new PeriodGenerator(args);

		pg.allocatePeriodValue();
		pg.shuffleAllocatedArray();
		//pg.printPeriods();
		pg.printPeriods(pg.getFilePath());
		//pg.printPeriodsForShell(shellFilepath);
	}
}