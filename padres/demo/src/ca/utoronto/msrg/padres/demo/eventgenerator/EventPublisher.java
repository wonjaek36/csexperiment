package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.RandomAccessFile;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import ca.utoronto.msrg.padres.client.Client;
import ca.utoronto.msrg.padres.client.ClientConfig;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.util.LogException;
import ca.utoronto.msrg.padres.common.util.LogSetup;

public class EventPublisher extends Client {

	public static String logError = "Log Exception Occured in Setup";
	
	public static String parameterError = 
			  "-b brokerURI "
			+ "-i client id "
			+ "-c classname "
			+ "-v variable-name "		
			+ "-t [boolean | real | discrete(integer)] " 
			+ "-r outputFilePath "
			+ "-w wating time(second) "
			+ "-n order period(millisecond) "
			+ "-l lowValue " 
			+ "-h highValue\n";
	public static String LOG_FILE_PATH_PREFIX = System.getProperty("user.dir") + File.separator +  "pub_log";
	
	Integer waitingTime;// second
	Integer period;		// millisecond
	Integer eventTime;  // second
	String brokerURI;
	String id;
	String inputFilePath="";
	String outputFilePath="";
	boolean advFlag;
	boolean sanityCheck;
	String clientId;
	ArrayList<Integer> pubList;
	ArrayList<Integer> periodList;
	
	ArrayList<Long> inputFileOffset = null;
	ArrayList<Long> outputFileOffset = null;
	
	public EventPublisher(ClientConfig newConfig, String b, String in_id,
			Integer w , ArrayList<Integer> pL, Integer et,
			ArrayList<Integer> pl, String ifp, String ofp, boolean adf, boolean st) throws ClientException
	{
		super(newConfig);
		id = in_id;
		brokerURI = b;
		waitingTime = w;
		periodList = pL;
		eventTime = et;
		
		pubList = pl;
		
		inputFilePath = ifp;
		outputFilePath = ofp;
		advFlag = adf;
		sanityCheck = st;
		
		
		inputFileOffset = new ArrayList<Long>();
		outputFileOffset = new ArrayList<Long>();

		int pubListSize = pubList.size();
		for(int i=0;i<pubListSize;i++) {
			inputFileOffset.add(0L);
			outputFileOffset.add(0L);
		}
	}
	public boolean isAdvertisement() {
		return advFlag;
	}

	public void shutdown() {
		
		try {
			super.shutdown();
		} catch (ClientException e) {
			System.err.println(e.getMessage());
		}
		System.exit(0);
	}
	
	public void publishAdvertisement(int idx) {
		RandomAccessFile input = null;
		RandomAccessFile output = null;
		int number = pubList.get(idx);
		String curInputFilePath = inputFilePath + number;
		String curOutputFilePath = outputFilePath + number;
		
		try {
			input = new RandomAccessFile(curInputFilePath, "r");
			output = new RandomAccessFile(curOutputFilePath, "rw");
		} catch(Exception e) {
			e.printStackTrace();
			System.err.println(curInputFilePath + "and " + curOutputFilePath +" Cannot be opened");
			System.exit(1);
		}
		
		if( input == null || output == null ) {
			System.err.println(curInputFilePath + "and " + curOutputFilePath +" Cannot be opened");
			System.exit(1);
		}
		
		try {
			long curInFileOffset = inputFileOffset.get(idx);
			long curOutFileOffset = outputFileOffset.get(idx);
			input.seek(curInFileOffset);
			output.seek(curOutFileOffset);
			
			String line = input.readLine();
			while( line.charAt(0) == 'a' ) {
				synchronized(EventPublisher.this) {
					handleCommand(line);
				}
				line = input.readLine();
				output.writeBytes(line + "\n");
			}
			inputFileOffset.set(idx, input.getFilePointer());
			outputFileOffset.set(idx, output.getFilePointer());
			
			input.close();
			output.close();
		} catch(Exception e){
			System.err.println("Advertisement Fail");
			return;
		}
		return;
	}
	/*
	class PublishAllAdvertisement implements Runnable {
		RandomAccessFile input;
		RandomAccessFile output;
		int index;
		public PublishAllAdvertisement( int idx ) {
			index = idx;
			int number = pubList.get(idx);
			String curThreadInFilePath = inputFilePath + number;
			String curThreadOutFilePath = outputFilePath + number;
			
			try {
				input = new RandomAccessFile(curThreadInFilePath, "r");
				output = new RandomAccessFile(curThreadOutFilePath, "rw");
			} catch(Exception e) {
				System.err.println(curThreadInFilePath + "and " + curThreadOutFilePath + " Cannot be opened");
				System.exit(1);
			}
		}
		
		public void run() {
			
			if( input == null || output == null ) {
				System.err.println("Null Pointer Exception");
				System.exit(1);
			}
			
			try {
				long curInFileOffset = inputFileOffset.get(index);
				long curOutFileOffset = outputFileOffset.get(index);
				input.seek(curInFileOffset);
				output.seek(curOutFileOffset);
				
				String line = input.readLine();
				while( line.charAt(0) == 'a' ) {
					synchronized(EventPublisher.this) {
						handleCommand(line);
					}
					line = input.readLine();
					output.writeBytes(line + "\n");
				}
				inputFileOffset.set(index, input.getFilePointer());
				outputFileOffset.set(index, output.getFilePointer());
				
				input.close();
				output.close();
			} catch(Exception e){
				System.err.println("Advertisement Fail");
				return;
			}
			return;
		}// end of run
	}
	*/
	
	public void publishSingleEvent(int idx) {
		RandomAccessFile input = null;
		RandomAccessFile output = null;
		int pubcnt = 0;
		int number = pubList.get(idx);
		String curThreadInFilePath = inputFilePath + number;
		String curThreadOutFilePath = outputFilePath + number;
		
		try {
			input = new RandomAccessFile(curThreadInFilePath, "r");
			output = new RandomAccessFile(curThreadOutFilePath, "rw");
		} catch(Exception e) {
			System.err.println(curThreadInFilePath + "and " + curThreadOutFilePath + " Cannot be opened");
			e.printStackTrace();
			System.exit(1);
		}
		
		if( input == null || output == null ) {
			System.err.println("Null Pointer Exception");
			return;
		}
		
		try {
			long curInFileOffset = inputFileOffset.get(idx);
			long curOutFileOffset = outputFileOffset.get(idx);
			input.seek(curInFileOffset);
			output.seek(curOutFileOffset);
			
			String line = input.readLine();
			while( line.charAt(0) != 'p' ) {
				line = input.readLine();
			}
			
			long currTime = System.currentTimeMillis();
			output.writeBytes("Msg[" + (pubcnt++) + "]: ");
			output.writeBytes(line);
			output.writeBytes("  " + "time: " + currTime + "\n");
			
			synchronized(EventPublisher.this) {
				handleCommand(line);
			}
			
			inputFileOffset.set(idx, input.getFilePointer());
			outputFileOffset.set(idx, output.getFilePointer());
			input.close();
			output.close();
		} catch(IOException ie) {
			System.err.println("IOException is occured");
			ie.printStackTrace();
		} catch(Exception e){
			System.err.println("Publish Event Fail");
			e.printStackTrace();
		}
		return;
	}
	/*
	class PublishSingleEvent implements Runnable {

		RandomAccessFile input;
		RandomAccessFile output;
		int index;
		public PublishSingleEvent( int idx ) {
			index = idx;
			int number = pubList.get(idx);
			String curThreadInFilePath = inputFilePath + number;
			String curThreadOutFilePath = outputFilePath + number;
			
			try {
				input = new RandomAccessFile(curThreadInFilePath, "r");
				output = new RandomAccessFile(curThreadOutFilePath, "rw");
			} catch(Exception e) {
				System.err.println(curThreadInFilePath + "and " + curThreadOutFilePath + " Cannot be opened");
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		public synchronized void run() {
			
			if( input == null || output == null ) {
				System.err.println("Null Pointer Exception");
				return;
			}
			
			int pubcnt = 0;
			try {
				long curInFileOffset = inputFileOffset.get(index);
				long curOutFileOffset = outputFileOffset.get(index);
				input.seek(curInFileOffset);
				output.seek(curOutFileOffset);
				
				String line = input.readLine();
				while( line.charAt(0) != 'p' ) {
					line = input.readLine();
				}
				
				long currTime = System.currentTimeMillis();
				output.writeBytes("Msg[" + (pubcnt++) + "]: ");
				output.writeBytes(line);
				output.writeBytes("  " + "time: " + currTime + "\n");
				
				synchronized(EventPublisher.this) {
					handleCommand(line);
				}
				
				inputFileOffset.set(index, input.getFilePointer());
				outputFileOffset.set(index, output.getFilePointer());
				input.close();
				output.close();
			} catch(IOException ie) {
				System.err.println("IOException is occured");
				ie.printStackTrace();
			} catch(Exception e){
				System.err.println("Publish Event Fail");
				e.printStackTrace();
			}
			return;
		}// end of run
	}
	*/
	public void process() {
		//ExecutorService executor = Executors.newFixedThreadPool(100);
		try { Thread.sleep(waitingTime*1000); } catch(Exception e) { System.err.println("Thread sleep Err"); }
	
		if( advFlag ) {
			int pubListSize = pubList.size();
			for(int i=0;i<pubListSize;i++) {
				publishAdvertisement(i);
			}
		}
		
		try { Thread.sleep(10*1000); } catch(Exception e) { System.err.println("Thread sleep Err"); }
		System.out.println("After Advertisement, Wait 10 seconds.");
		int pubCnt = 0;
		while( pubCnt <= eventTime ) {
			int pubListSize = pubList.size();
			for(int i=0;i<pubListSize;i++) {
				int period = (periodList.get(i) / 1000);
				if( pubCnt % period == 0 ) {
					publishSingleEvent(i);
				}
			}
			pubCnt += 1;
			try { Thread.sleep(1000); } catch(Exception e) { System.err.println("Thread sleep Err"); }
		}
		
		try {
			System.out.println("Done Publisher");
			shutdown();
		} catch(Exception e) {
			System.err.println("Thread encounter an unexpected situation");
		}
	}
	public static void main(String args[]){
		
		//default variable
		Integer waitTime = 10; // 10s
		Integer eventTime = 600; // 600s
		String brokerURI="";
		String id="";
		String inputFilePath="";
		String outputFilePath="";
		boolean advFlag = true;
		boolean sanityCheck = false;
		ArrayList<Integer> publishList = new ArrayList<Integer>();
		ArrayList<Integer> periodList = new ArrayList<Integer>();
		
		try { 
			for(int i=0;i<args.length;i++){
				// compareTo : the value 0 if the argument string is equal to this string;
				if( args[i].compareTo("-b") == 0 ) { // 1. broker URI
					brokerURI = args[++i];
				} else if( args[i].compareTo("-i") == 0 ) { // 2. Client ID
					id = args[++i];
				} else if( args[i].compareTo("-w") == 0 ) { // 3. waiting Time
					waitTime = Integer.parseInt( args[++i] );
				} /*else if( args[i].compareTo("-p") == 0 ) { // 4. period
					period = Integer.parseInt( args[++i] );
				}*/ else if( args[i].compareTo("-ifp") == 0 ) { // 5. input File path
					inputFilePath = args[++i];
					inputFilePath = inputFilePath.replaceFirst("^~",System.getProperty("user.home"));
				} else if( args[i].compareTo("-ofp") == 0 ) { // 6. output File path
					outputFilePath = args[++i];
					outputFilePath = outputFilePath.replaceFirst("^~",System.getProperty("user.home"));
				} else if( args[i].compareTo("-e") == 0 ) { // 7. eventTime(totalTime)
					eventTime = Integer.parseInt( args[++i] );
				} else if( args[i].compareTo("-a") == 0 ) { // 8. flag of advertisement 
					advFlag = true;
				} else if( args[i].compareTo("-s") == 0 ) { // 9. flag of sanity (deprecated)
					sanityCheck = true;
				} else if( args[i].compareTo("-n") == 0 ) { // 10. order of publisher
					publishList.add( Integer.parseInt( args[++i] ));
					periodList.add( Integer.parseInt( args[++i] ));
				} else {
					System.err.print("Unknown Parameter: " + args[i]);
					throw new Exception();
				}
			}
			// Without value this parameter
			// Cannot do anymore
		} catch(NumberFormatException e) {
			System.err.println("NumberFormatException");
			//System.err.println(parameterError);
			/*for( String para : args ) {
				exec += para + " ";
			}*/
			//System.err.println( exec );
			System.err.println( "Input Proper Value(Especially p, w, e)" );
			return;
		} catch(Exception e) { /*NumberFormatException*/
			System.err.println("Parsing Error");
			//System.err.println(parameterError);
			/*for( String para : args ) {
				exec += para + " ";
			}
			System.err.println( exec );*/
			return;
		} 
		////////////////////// parsing parameter //////////////////////
		
		System.out.println("Parsing Complete");
		
		try { 
			new LogSetup(LOG_FILE_PATH_PREFIX + id);
		} catch (LogException e) {
			System.err.println(logError);
			return;
		}
		
		try {
			ClientConfig newConfig = new ClientConfig();
			newConfig.clientID = id;
			newConfig.connectBrokerList = new String[1];
			newConfig.connectBrokerList[0] = brokerURI;
			
			EventPublisher eventPublisher = new EventPublisher(newConfig, brokerURI, id,
					waitTime , periodList, eventTime,
					publishList, inputFilePath, outputFilePath, advFlag, sanityCheck);
			
			eventPublisher.process();
		} catch (ClientException e) {
			System.err.println("Event Publishing Error");
			e.printStackTrace();
			return;
		}
	}//end of Main()
}

