package ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner;

import java.util.ArrayList;

abstract public class Assigner {

	ArrayList<ArrayList<CompositeSubscription>> subscribers;
	double loads[];
	Integer brokerSize;
	public Assigner(Integer brokerSize) { 

		subscribers = new ArrayList<ArrayList<CompositeSubscription>>();
		loads = new double[brokerSize];
		for(int i=0;i<brokerSize;i++) {
			subscribers.add( new ArrayList<CompositeSubscription>() );
		}
		this.brokerSize = brokerSize;
	}
	
	public double[] getLoads() {
		return loads;
	}
	public ArrayList<ArrayList<CompositeSubscription>> getSubscribers() {
		return subscribers;
	}
	abstract public void assignSubscriptionToSubscriber(ArrayList<Integer> periods, ArrayList<Integer> values);
}
