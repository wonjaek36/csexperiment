package ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner;

public class CompositeSubscription implements Comparable<CompositeSubscription>{
	public int[] number = new int[2];
	public double m_period;
	public int[] m_values = new int[2];
	
	public CompositeSubscription(int number1, int number2, double periods, int value1, int value2) {
		number[0] = number1;
		number[1] = number2;
		m_period = periods;
		m_values[0] = value1;
		m_values[1] = value2;
	}
	
	@Override
	public int compareTo(CompositeSubscription element) {
		
		if( this.m_period > element.m_period ) {
			return -1;
		} else if( this.m_period < element.m_period ) {
			return 1;
		}
		return 0;
	}
	
	public double getPeriod() {
		return m_period;
	}
	public int[] getNumbers() {
		return number;
	}
	public int[] getValues() {
		return m_values;
	}
	public String toString() {
		String ret = "";
		ret = ("number: " + number[0] + " " + number[1] +
				"values: " + m_values[0] + " " + m_values[1] + 
				"periods: " + m_period);
		return ret;
	}
}