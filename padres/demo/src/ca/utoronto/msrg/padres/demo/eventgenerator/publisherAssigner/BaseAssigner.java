package ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner;

import java.util.ArrayList;
import java.util.Collections;

public class BaseAssigner extends Assigner {

	public BaseAssigner(Integer bs, boolean icc) {
		super(bs, icc);
	}

	@Override
	public void assignPublisherToBroker(ArrayList<Integer> periods, ArrayList<Integer> values) {
		/*brokers = new ArrayList< ArrayList<Integer> >();
		loads = new double[brokerSize];*/
		ArrayList<BrokerElement> elements = new ArrayList<BrokerElement>();
		int periodSize = periods.size();
		
		/*for(int i=0;i<brokerSize;i++) {
			ArrayList<Integer> broker = new ArrayList<Integer>();
			loads[i] = 0.0;
			brokers.add(broker);
		}*/
		for(int i=0;i<periodSize;i++) {
			double msgPerSec = 1.0/((double)periods.get(i));
			BrokerElement brokerElement = new BrokerElement(i, -1, msgPerSec);
			elements.add(brokerElement);
		}
		
		for(int i=0;i<elements.size();i++) {
			
			BrokerElement element = elements.get(i);
			int pos = findMinLoadBroker();
			ArrayList<Integer> broker = brokers.get(pos);
			broker.add(element.getIndexFirst());
			loads[pos] += element.getMsgPerSec();
		}
	}

}
