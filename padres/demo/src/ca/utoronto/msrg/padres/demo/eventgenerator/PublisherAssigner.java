package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner.Assigner;
import ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner.BaseAssigner;
import ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner.HorizontalAssigner;
import ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner.VerticalAssigner;
import ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner.ConflictFreeAssigner;

public class PublisherAssigner {

	private HashMap<String, String> options;
	private HashMap<String, Assigner> assigners;
	
	static final String BROKER_SIZE_FLAG = "broker_size";
	//static final String PUBLISHER_SIZE_FLAG = "publisher_size";
	static final String PERIOD_FILE_PATH_FLAG = "period_filepath";
	static final String VALUE_FILE_PATH_FLAG = "value_filepath";
	static final String IGNORE_CO_FLAG = "ign_co";
	static final String POLICY_FLAG = "policy";
	static final String WEIGHT_FLAG = "weight";
	static final String PRINT_PATH_PREFIX_FLAG = "printpath_prefix";
	
	//private int numEventPublisher;
	private boolean isCareCo = false;
	ArrayList<Integer> periods;
	ArrayList<Integer> values;
	Assigner assigner;
	
	public PublisherAssigner(String[] args) {
		
		options = new HashMap<String, String>();
		assigners = new HashMap<String, Assigner>();
		
		options.put(BROKER_SIZE_FLAG, 		null);
		//options.put(PUBLISHER_SIZE_FLAG, 	null);
		options.put(PERIOD_FILE_PATH_FLAG, 	null);
		options.put(VALUE_FILE_PATH_FLAG, 	null);
		options.put(POLICY_FLAG, 			null);
		options.put(WEIGHT_FLAG,  			null); // for conflictFreeAssigner
		options.put(PRINT_PATH_PREFIX_FLAG, null);
		//options.put(IGNORE_CO_FLAG, 		null); // only vertical or horizontal
		
		for(int i=0;i<args.length;i++) {
			String arg = args[i];
			if( arg.startsWith("-") ) {
				String optionName = arg.substring(1);
				
				if( options.containsKey(optionName) ){
					options.put(optionName, args[++i]);
				} else if(optionName.compareTo(IGNORE_CO_FLAG)==0) {
					isCareCo = true;
				}
			}
		}
		
		assigners.put("BASE", 			new BaseAssigner(getBrokerSize(), isCareCo));
		assigners.put("VERTICAL", 		new VerticalAssigner(getBrokerSize(), isCareCo));
		assigners.put("HORIZONTAL", 	new HorizontalAssigner(getBrokerSize(), isCareCo));
		assigners.put("CONFLICTFREE", 	new ConflictFreeAssigner(getBrokerSize(), isCareCo, getWeight()));
	}
	
	public void inputPublisherStatus(String periodFilePath, String valueFilePath) {
		
		periods = new ArrayList<Integer>();
		values = new ArrayList<Integer>();
		try {
			
			Scanner periodScanner = new Scanner(new File(periodFilePath));
			
			int periodSize = periodScanner.nextInt();
			for(int i=0;i<periodSize;i++){
				periods.add(periodScanner.nextInt());
			}
			periodScanner.close();
			
			Scanner valueScanner = new Scanner(new File(valueFilePath));
			int valueSize = valueScanner.nextInt();
			for(int i=0;i<valueSize;i++) {
				values.add(valueScanner.nextInt());
			}
			valueScanner.close();
			
			System.out.println("Class Information reads from in inputClassStauts");
		} catch(FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Cannot read Information from in inputClassStauts");
			System.exit(1);
		}
	}

	@SuppressWarnings("resource")
	public void brokerInformationPrint() {
		
		int brokerSize = getBrokerSize();
		for(int i=0;i<brokerSize;i++) {
			String print_path_prefix = getPrintPrefixPath() + File.separator;
			String filename = "PublisherInformation" + (i + 1);
			String filepath = print_path_prefix + filename;
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(filepath));
				ArrayList<Integer> selectedBroker = assigner.getBroker(i);
				
				bw.write(selectedBroker.size()+" ");
				bw.newLine();
				for(int j=0;j<selectedBroker.size();j++) {
					bw.write(selectedBroker.get(j) + " ");
				}
				bw.newLine();
				for(int j=0;j<selectedBroker.size();j++) {
					int classNumber = selectedBroker.get(j);
					bw.write(periods.get(classNumber) + " ");
				}
				bw.newLine();
				for(int j=0;j<selectedBroker.size();j++) {
					int classNumber = selectedBroker.get(j);
					bw.write(values.get(classNumber) + " ");
				}
				bw.newLine();
				
				bw.close();
			} catch(Exception e){
				System.err.println("Fail to write BrokerEdgePrint");
				e.printStackTrace();
			}
		}
	}
	
	public void assignPublisherToBroker() {
		String policy = options.get(POLICY_FLAG);
		assigner = assigners.get(policy);
		
		assigner.assignPublisherToBroker(periods, values);
	}
	
	public Integer getBrokerSize() {
		return Integer.parseInt(options.get(BROKER_SIZE_FLAG));
	}
	public String getPeriodFilePath() {
		return (options.get(PERIOD_FILE_PATH_FLAG));
	}
	public String getValueFilePath() {
		return (options.get(VALUE_FILE_PATH_FLAG));
	}
	public Double getWeight() {
		try {
			return Double.parseDouble(options.get(WEIGHT_FLAG));
		} catch(Exception e) {
			return 0.0;
		}
	}
	public String getPrintPrefixPath() {
		return (options.get(PRINT_PATH_PREFIX_FLAG));
	}
	
	public static void main( String args[] ){
		
		PublisherAssigner pg = new PublisherAssigner(args);
		pg.inputPublisherStatus(pg.getPeriodFilePath(), pg.getValueFilePath());
		pg.assignPublisherToBroker();
		
		/*pg.assignClassToBrokerNoPolicy();
		pg.assignClassToBrokerVertically();
		pg.assignClassToBrokerHorizontally();*/
		
		pg.assignedBrokerPrint();
		pg.brokerInformationPrint();
		//pg.publishersToPrint();
	}
	////////////////////////////////////////
	
	
	/*deprecated*/
	/*@SuppressWarnings("resource")
	public void publishersToPrint() {
		
		String verPublisherFilePrefix = "verPublisher";
		String horPublisherFilePrefix = "horPublisher";
		String lbPublisherFilePrefix = "lbPublisher";
		
		for(int i=0;i<numBroker;i++) {
			//String verPublisherFilePath = verPublisherFilePrefix + i;
			try {
				double pubLimitLoad = verLoads[i] / ((double)numEventPublisher); 
				int eventIndex = 0;
				for(int j=0;j<numEventPublisher;j++) {
					String verPublisherFilePath = verPublisherFilePrefix + (i*numEventPublisher+j);
					BufferedWriter verPublisherFile = new BufferedWriter(new FileWriter(verPublisherFilePath ));
					ArrayList <Integer> selectedBroker = verBrokers.get(i);
					int size = selectedBroker.size();
					double curPubLoad = 0.0;
					int startIndex = eventIndex;
					
					verPublisherFile.write("NUMBER=(");
					for(int k=0;curPubLoad < pubLimitLoad && k < size;k++){
						if( k != 0 ) { 
							verPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(eventIndex++);
						verPublisherFile.write(v.toString());	
						curPubLoad += (1.0/((double)periods.get(v)));
					}
					verPublisherFile.write(")\n");
					
					verPublisherFile.write("PERIOD=(");
					for(int k=startIndex;k<eventIndex;k++){
						if( k != startIndex ) { 
							verPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(k);
						verPublisherFile.write(Integer.toString(periods.get(v)*1000));	
					}
					verPublisherFile.write(")\n");
					
					verPublisherFile.flush();
					verPublisherFile.close();
				}
			} catch (IOException e) {
				System.err.println("Cannot write verPublisherFile");
				e.printStackTrace();
			} 
		}
		
		for(int i=0;i<numBroker;i++) {
			try {
				double pubLimitLoad = horLoads[i] / ((double)numEventPublisher); 
				int eventIndex = 0;
				for(int j=0;j<numEventPublisher;j++) {
					String horPublisherFilePath = horPublisherFilePrefix + (i*numEventPublisher+j);
					BufferedWriter horPublisherFile = new BufferedWriter(new FileWriter(horPublisherFilePath ));
					ArrayList <Integer> selectedBroker = horBrokers.get(i);
					int size = selectedBroker.size();
					double curPubLoad = 0.0;
					int startIndex = eventIndex;
					
					horPublisherFile.write("NUMBER=(");
					for(int k=0;curPubLoad < pubLimitLoad && k < size;k++){
						if( k != 0 ) { 
							horPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(eventIndex++);
						horPublisherFile.write(v.toString());	
						curPubLoad += (1.0/((double)periods.get(v)));
					}
					horPublisherFile.write(")\n");
					
					horPublisherFile.write("PERIOD=(");
					for(int k=startIndex;k<eventIndex;k++){
						if( k != startIndex ) { 
							horPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(k);
						horPublisherFile.write(Integer.toString(periods.get(v)*1000));	
					}
					horPublisherFile.write(")\n");
					
					horPublisherFile.flush();
					horPublisherFile.close();
				}
			} catch (IOException e) {
				System.err.println("Cannot write horPublisherFile");
				e.printStackTrace();
			} 
		}
		
		for(int i=0;i<numBroker;i++) {
			try {
				double pubLimitLoad = lbLoads[i] / ((double)numEventPublisher); 
				int eventIndex = 0;
				for(int j=0;j<numEventPublisher;j++) {
					String lbPublisherFilePath = lbPublisherFilePrefix + (i*numEventPublisher+j);
					BufferedWriter lbPublisherFile = new BufferedWriter(new FileWriter(lbPublisherFilePath ));
					ArrayList <Integer> selectedBroker = lbBrokers.get(i);
					int size = selectedBroker.size();
					double curPubLoad = 0.0;
					int startIndex = eventIndex;
					
					lbPublisherFile.write("NUMBER=(");
					for(int k=0;curPubLoad < pubLimitLoad && k < size;k++){
						if( k != 0 ) { 
							lbPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(eventIndex++);
						lbPublisherFile.write(v.toString());	
						curPubLoad += (1.0/((double)periods.get(v)));
					}
					lbPublisherFile.write(")\n");
					
					lbPublisherFile.write("PERIOD=(");
					for(int k=startIndex;k<eventIndex;k++){
						if( k != startIndex ) { 
							lbPublisherFile.write(" ");
						}
						Integer v = selectedBroker.get(k);
						lbPublisherFile.write(Integer.toString(periods.get(v)*1000));	
					}
					lbPublisherFile.write(")\n");
					
					lbPublisherFile.flush();
					lbPublisherFile.close();
				}
			} catch (IOException e) {
				System.err.println("Cannot write lbPublisherFile");
				e.printStackTrace();
			} 
		}
	}*/
	public void assignedBrokerPrint() {
		int numOfCo = 0;
		
		for(int i=0;i<periods.size();i+=2) {
			if( periods.get(i) == periods.get(i+1))
				numOfCo += 1;
		}
		
		System.out.println("# of class :" + periods.size());
		System.out.println("# of Co-occurence class: " + (numOfCo*2));
		Integer brokerSize = getBrokerSize();
		
		for(int i=0;i<brokerSize;i++) {
			ArrayList<Integer> selectedBroker = assigner.getBroker(i);
			System.out.print(i+"th broker[" + assigner.loads[i] + "]: ");
			for(int j=0;j<selectedBroker.size();j++){
				System.out.print(selectedBroker.get(j)+" ");
			}
			System.out.print("\n");
		}
	}
}
