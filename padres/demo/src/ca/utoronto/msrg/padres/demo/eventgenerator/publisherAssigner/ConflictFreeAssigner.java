package ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner;

import java.util.ArrayList;
import java.util.Collections;

public class ConflictFreeAssigner extends Assigner {

	Double conflict_weight;
	ArrayList<Integer> periods;
	
	static double baseLog(double x, double base) {
		return Math.log10(x) / Math.log10(base);
	}
	public ConflictFreeAssigner(Integer bs, boolean icc, Double w) {
		super(bs, icc);
		conflict_weight = w;
	}
	public int gcd(int a, int b) {
		if( b == 0 )
			return a;
		return gcd(b,  a%b);
	}
	
	public int lcm(int a, int b) {
		int GCD = gcd(a, b);
		return (a*b) / GCD;
	}
	public int findMinLoadBroker(BrokerElement element) {
		double min = 987654321.0;
		int pos=0;

		int period = periods.get(element.getIndexFirst());
		
		for(int i=0;i<brokerSize;i++) {
			double total_loads;
			double msg_loads = 0.0;
			double conflict_loads = 0.0;
			
			ArrayList<Integer> broker = brokers.get(i);
			for(int j=0;j<broker.size();j++) {
				int currentIndex = broker.get(j);
				int currentPeriod = periods.get(currentIndex);
				msg_loads += (1.0/((double)currentPeriod));
				conflict_loads += (1.0/((double)lcm(currentPeriod, period)));
			}
			total_loads = (1 - conflict_weight) * msg_loads + conflict_weight * conflict_loads;
//			System.out.println(i + " " + total_loads);
//			System.out.println(msg_loads + " " + conflict_loads);
			
			if( min > total_loads ){
				min = total_loads;
				pos = i;
			}
		}
		return pos;
	}
	
	@Override
	public void assignPublisherToBroker(ArrayList<Integer> periods, ArrayList<Integer> values) {

		this.periods = periods;
		/*brokers = new ArrayList< ArrayList<Integer> >();
		loads = new double[brokerSize];*/
		ArrayList<BrokerElement> elements = new ArrayList<BrokerElement>();
		int periodSize = periods.size();
			
		/*for(int i=0;i<brokerSize;i++) {
			ArrayList<Integer> broker = new ArrayList<Integer>();
			loads[i] = 0.0;
			brokers.add(broker);
		}*/
		for(int i=0;i<periodSize;i++) {
			double msgPerSec = 1.0/((double)periods.get(i));
			BrokerElement brokerElement = new BrokerElement(i, -1, msgPerSec);
			elements.add(brokerElement);
		}
		
		for(int i=0;i<elements.size();i++) {
			
			BrokerElement element = elements.get(i);
			int pos = findMinLoadBroker(element);
			ArrayList<Integer> broker = brokers.get(pos);
			broker.add(element.getIndexFirst());
			loads[pos] += element.getMsgPerSec();
		}
		
		System.out.println(conflict_weight);
	}

}
