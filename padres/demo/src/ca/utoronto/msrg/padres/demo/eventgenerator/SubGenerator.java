package ca.utoronto.msrg.padres.demo.eventgenerator;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.utoronto.msrg.padres.demo.eventgenerator.subscriptionAssigner.CompositeSubscription;
import ca.utoronto.msrg.padres.demo.eventgenerator.type.DataType;
import ca.utoronto.msrg.padres.demo.exception.TypeException;

public class SubGenerator {

	/*public static String parameterList = 
			  "-n number of subscription(related alphaNode) "
			+ "-a number of alpha node"
			+ "-c(k) kth classname "
			+ "-v(k) kth variable-name "		
			+ "-t(k) kth variable-type [boolean | discrete | range] " 
			+ "-l(k) kth variable-lowValue(included) " 
			+ "-h(k) kth variable-highValue(excluded)";
	
	public static final Pattern CLASSID_PATTERN 	= Pattern.compile("-c[(]([0-9]+)[)]");
	public static final Pattern VARIALBE_PATTERN 	= Pattern.compile("-v[(]([0-9]+)[)]");
	public static final Pattern TYPE_PATTERN 		= Pattern.compile("-t[(]([0-9]+)[)]");
	public static final Pattern HIGHVALUE_PATTERN 	= Pattern.compile("-h[(]([0-9]+)[)]");
	public static final Pattern LOWVALUE_PATTERN 	= Pattern.compile("-l[(]([0-9]+)[)]");*/
	
	String[] m_classId = null;
	String[] m_variableId = null;
	
	Integer m_cntSub = 0;
	DataType[] m_value = null;
	int alpha;
	static boolean replacement = false;
	boolean m_assendingOrder;
	String m_printPath;
	PrintWriter output;
	
	public SubGenerator(String[] ci, String[] vi, Integer al, Integer cs,
			DataType[] v, boolean assending, String printPath) {
		m_classId = ci;
		m_variableId = vi;
		m_cntSub = cs;
		m_value = v;
		m_printPath = printPath;
		m_assendingOrder = assending;
	}
	
	private void SubGeneratorWithAssedingOrder(CompositeSubscription cs, String printPath, boolean assendingOrder ) {
		int[] numbers = cs.getNumbers();
		int[] values = cs.getValues();
		int size = numbers.length;
		m_assendingOrder = assendingOrder;
		
		try {
			output = new PrintWriter( new BufferedWriter( new FileWriter(printPath, true) ) );
		} catch(Exception e) {
			System.err.println("Open " + printPath + " Err in SubGenerator");
			e.printStackTrace();
			System.exit(1);
		}
		
		m_cntSub = size;
		alpha = values[0];
		m_printPath = printPath;
		
		m_classId = new String[size];
		m_variableId = new String[size];
		m_value = new DataType[size];
		for(int i=0;i<numbers.length;i++) {
			m_classId[i] = "c" + numbers[i];
			m_variableId[i] = "v" + numbers[i];
			
			try { 
				m_value[i] = new DataType();
				m_value[i].setType("discrete");
				m_value[i].setLowValue((new Integer(0)).toString());
				m_value[i].setHighValue((new Integer(values[i])).toString());
			}
			catch(Exception e){
				System.err.println("Create Subscription Err in SubGenerator");
				e.printStackTrace();
			}
		}
	}

	public SubGenerator(CompositeSubscription cs, String printPath) {
		SubGeneratorWithAssedingOrder(cs, printPath, true);
	}


	// deprecated
	private String createIndividualMsg( int idx, boolean isComposite ) throws Exception { 
		String ret = "";
		String type = m_value[idx].getType(); // throw Exception
		
		
		if( type.compareTo("boolean") == 0 ){
			if( isComposite ) ret = "{";
			ret += "[class,eq,'"+m_classId[idx]+"'],"+
					"[name,eq,'"+m_variableId[idx]+"'],"+
					"[value,eq,TRUE]";
			if( isComposite ) ret = ret + "}";
		} else if( type.compareTo("discrete") == 0 ) {
			if( isComposite ) ret = "{";
			ret += "[class,eq,'"+m_classId[idx]+"'],"+
					"[name,eq,'"+m_variableId[idx]+"'],"+
					"[value,eq,"+m_value[idx].getRandomValue()+"]";
			if( isComposite ) ret = ret + "}";
		} else if( type.compareTo("range") == 0 ) {
			if( isComposite ) ret = "{";
			if( m_value[idx].getLowValue() != null && m_value[idx].getHighValue() != null ) {
				ret = ret + "[class,eq,'"+m_classId[idx]+"'],"+
						"[name,eq,'"+m_variableId[idx]+"'],"+
						"[value," + (idx % 2 == 0 ? "<" : ">") +
						","+(idx % 2 == 0 ? 
								( (Double)m_value[idx].getRandomValue()+(Double)m_value[idx].getHighValue() ) / 2
								: ( (Double)m_value[idx].getRandomValue()+(Double)m_value[idx].getLowValue() ) / 2)+"]";
			} else if( m_value[idx].getLowValue() == null && m_value[idx].getHighValue() == null ) {
				throw new Exception();
			}
			else {
				ret = ret + "[class,eq,'"+m_classId[idx]+"'],"+
						"[name,eq,'"+m_variableId[idx]+"'],"+
						"[value," + m_value[idx].getHighValue() == null ? "<" : ">" 
						+ ","+m_value[idx].getRandomValue()+"]";
			}
			if( isComposite ) ret = ret + "}";
		}
		else
			throw new Exception();
		
		return ret;
	}
	
	// deprecated
	public String createSubMsg() throws Exception {
		String subMessage = "";
		if ( m_cntSub == 0 ) { 
		}
		else if( m_cntSub == 1 ) {
			subMessage = createIndividualMsg(0,false); // It's so hard! make me crazy
		}
		else {
			subMessage = "cs {";
			for(int i=0;i<m_cntSub;i++) {
				if(i != 0)
					subMessage = subMessage + "&";
				subMessage += createIndividualMsg(i,true);
			}
			subMessage = subMessage +  "}";
			
		}
		return subMessage;
	}
	
	
	public void createSubMsgNoRep(int alpha) { 
		Random ran = new Random();
		
		Integer Value[][] = new Integer[m_cntSub+1][];
		for (int i=0;i<m_cntSub;i++ ){
			int lowValue = (Integer)m_value[i].getLowValue();
			int highValue = (Integer)m_value[i].getHighValue();
			int size = (Integer)m_value[i].getHighValue() - (Integer)m_value[i].getLowValue();
			Value[i] = new Integer[size];
			
			for(int j=lowValue, k=0;j<highValue;j++, k++){
				Value[i][k] = j;
			}
			
			if( m_assendingOrder == false ) {
				for(int j=1;j<size;j++){
					int idx = ran.nextInt(size-j)+j;
					//swap
					int temp = Value[i][j];
					Value[i][j] = Value[i][idx];
					Value[i][idx] = temp;
				}
			}
		}
		String ret;
		for(int j=0;j<alpha;j++) {
			ret = "cs {";
			for(int i=0;i<m_cntSub;i++) {
				if( i != 0 )
					ret += "&";
				ret += "{";
				ret += "[class,eq,'"+m_classId[i]+"'],"+
						"[name,eq,'"+m_variableId[i]+"'],"+
						"[value,eq,'"+Value[i][j]+"']";
				ret += "}";
			}
			ret += "}";
			output.println(ret);
		}
		
		output.flush();
		output.close(); 
	}
	public int getAlpha() {
		return alpha;
	}
	
	/*public static void main( String[] args ) {
		
		String[] classId = null;
		String[] variableId = null;
		Integer alpha = 0; // default 1
		Integer cntSub = 0;
		boolean assending = false;
		DataType[] value = null;
		
		Matcher classid_matcher, variable_matcher, type_matcher, highValue_matcher, lowValue_matcher;
		
		try {
			for(int i=0;i<args.length;i++) {
			
				classid_matcher 	= CLASSID_PATTERN.matcher(args[i]);
				variable_matcher 	= VARIALBE_PATTERN.matcher(args[i]);
				type_matcher 		= TYPE_PATTERN.matcher(args[i]);
				highValue_matcher	= HIGHVALUE_PATTERN.matcher(args[i]);
				lowValue_matcher	= LOWVALUE_PATTERN.matcher(args[i]);
				
				if( args[i].compareTo("-n") == 0 ) { // 1. No of class, variable
					cntSub 		= Integer.parseInt(args[++i]);
					// No of Subscription
					classId 	= new String[cntSub];
					variableId 	= new String[cntSub]; 
					value	 	= new DataType[cntSub];
					
				} else if( args[i].compareTo("-a") == 0 ) { // 2. no of alpha node
					alpha = Integer.parseInt( args[++i] );
				} else if( classid_matcher.matches() ) { 	// 3. class id -c(%d)
					Integer idx = Integer.parseInt( classid_matcher.group(1) );
					classId[idx]= args[++i];
				} else if( variable_matcher.matches() ) { 	// 4. variable id -v(%d)
					Integer idx = Integer.parseInt( variable_matcher.group(1) );
					variableId[idx]=args[++i];
				} else if( type_matcher.matches() ) { 		// 5. type -t(%d)
					Integer idx = Integer.parseInt( type_matcher.group(1) );
					value[idx]= new DataType();
					value[idx].setType(args[++i]);
				} else if( lowValue_matcher.matches() ) {  // 6. lowValue
					Integer idx = Integer.parseInt( lowValue_matcher.group(1) );
					value[idx].setLowValue(args[++i]);
				} else if( highValue_matcher.matches() ) { // 7. highValue
					Integer idx = Integer.parseInt( highValue_matcher.group(1) );
					value[idx].setHighValue(args[++i]);
				} else if( args[i].compareTo("--assending") == 0 ){
					assending = true;
				} else {
					System.out.println(args[i] + " " + args[++i]);
					throw new Exception();
				}
				highValue_matcher.reset();
				lowValue_matcher.reset();
				classid_matcher.reset();
				variable_matcher.reset();
				type_matcher.reset();
			}
		} catch(TypeException e) {
			System.err.println("SubGenerator: Type Error");
			String exec="";
			for( String para : args )
				exec += para + " ";
			System.err.println("SubGenerator input data: " +  exec );
			e.printStackTrace();
			return;
		} catch(Exception e){
			System.err.println("SubGenerator: Parsing Error Occured");
			String exec="";
			System.err.println(parameterList);
			for( String para : args )
				exec += para + " ";
			System.err.println("SubGenerator input data: " + exec );
			e.printStackTrace();
			return;
		}
		
		try {
			SubGenerator subGenerator = new SubGenerator(classId, variableId, alpha, cntSub, value, assending);
			
			
			
			//if ( replacement == true ) {// deprecated
			//	for(int i=0;i<subGenerator.getAlpha(); i++ ){
			//		System.out.println( subGenerator.createSubMsg() );
			//	}
			//}
			//else { replacement == false only support discrete
			//	 It's used for test
			//	subGenerator.createSubMsgNoRep(subGenerator.getAlpha());
			//}
			subGenerator.createSubMsgNoRep(subGenerator.getAlpha());
			System.out.flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}*/
}
