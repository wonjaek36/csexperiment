package ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner;

import java.util.ArrayList;

public abstract class Assigner {
	
	ArrayList<ArrayList<Integer>> brokers;
	public double[] loads;
	Integer brokerSize;
	boolean isCareCo;
	
	abstract public void assignPublisherToBroker(ArrayList<Integer> periods, ArrayList<Integer> values);
	
	public int findMinLoadBroker() {
		double min = loads[0];
		int pos=0;
		for(int i=1;i<brokerSize;i++) {
			if( min > loads[i] ){
				min = loads[i];
				pos = i;
			}
		}
		return pos;
	}
	public int findMinLoadBroker(int excluded) {
		double min = loads[0];
		int pos=0;
		for(int i=1;i<brokerSize;i++) {
			if( min > loads[i] && i != excluded ){
				min = loads[i];
				pos = i;
			}
		}
		return pos;
	}
	
	public ArrayList<Integer> getBroker(int index) {
		return brokers.get(index);
	}
	public Assigner(Integer bs, boolean icc) {
		brokerSize = bs;
		isCareCo = icc;
		brokers = new ArrayList<ArrayList<Integer>>();
		for(int i=0;i<brokerSize;i++) {
			brokers.add(new ArrayList<Integer>());
		}
		loads = new double[brokerSize];
	}
	
	public class BrokerElement implements Comparable<BrokerElement>{
		int m_index1, m_index2;
		double m_msgPerSec;
		public BrokerElement(int index1, int index2, double msgPerSec ) {
			m_index1 = index1;
			m_index2 = index2;
			m_msgPerSec = msgPerSec;
		}
		public int getIndexFirst() {
			return m_index1;
		}
		public int getIndexSecond() {
			return m_index2;
		}
		public double getMsgPerSec() {
			return m_msgPerSec;
		}
		@Override
		public int compareTo(BrokerElement o) {
			if( this.getMsgPerSec() < o.getMsgPerSec() ){
				return 1;
			} else if(this.getMsgPerSec() > o.getMsgPerSec() ) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}
