package ca.utoronto.msrg.padres.demo.eventgenerator.publisherAssigner;

import java.util.ArrayList;
import java.util.Collections;

public class HorizontalAssigner extends Assigner {

	public HorizontalAssigner(Integer bs, boolean icc) {
		super(bs, icc);
	}

	@Override
	public void assignPublisherToBroker(ArrayList<Integer> periods, ArrayList<Integer> values) {
		/*brokers = new ArrayList< ArrayList<Integer> >();*/
		/*loads = new double[brokerSize];*/
		int periodSize = periods.size();
		int[] assignedBroker = new int[periodSize];
		ArrayList<BrokerElement> elements = new ArrayList<BrokerElement>();
		
		/*for(int i=0;i<brokerSize;i++){
			ArrayList<Integer> broker = new ArrayList<Integer>();
			loads[i] = 0.0;
			brokers.add(broker);	
		}*/
		for(int i=0;i<periodSize;i+=2) {
			int index2 = -1;
			if( periods.get(i) == periods.get(i+1) || isCareCo == false ) 
				index2 = -2;
			
			double msgPerSec = 1.0/((double)periods.get(i));
			BrokerElement brokerElement = new BrokerElement(i, index2, msgPerSec);
			elements.add(brokerElement);
			msgPerSec = 1.0/((double)periods.get(i+1));
			brokerElement = new BrokerElement(i+1, index2, msgPerSec);
			elements.add(brokerElement);
		}
		
		
		for(int i=0;i<elements.size();i++){
			BrokerElement element = elements.get(i);
			int pos = 0;
			if( element.getIndexSecond() == -2 && element.getIndexFirst() % 2 == 1 ) {
				pos = findMinLoadBroker(assignedBroker[element.getIndexFirst()-1]);
			} else {
				pos = findMinLoadBroker();
			}
			ArrayList<Integer> broker = brokers.get(pos);
			broker.add(element.getIndexFirst());
			loads[pos] += element.getMsgPerSec();
			
			assignedBroker[element.getIndexFirst()] = pos;
			if( element.getIndexSecond() > 0 ) {
				broker.add(element.getIndexSecond());
			}
		}
	}

}
