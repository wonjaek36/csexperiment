package ca.utoronto.msrg.padres.broker.router;

import java.util.Set;

import ca.utoronto.msrg.padres.broker.brokercore.BrokerCore;
import ca.utoronto.msrg.padres.broker.brokercore.SenderTagger;
import ca.utoronto.msrg.padres.broker.order.TotalOrderEngine;
import ca.utoronto.msrg.padres.broker.topk.TopkEngine;
import ca.utoronto.msrg.padres.broker.topk.count.DesynchronizedChunkTopkEngine;
import ca.utoronto.msrg.padres.broker.topk.sub.TopkSubEngine;
import ca.utoronto.msrg.padres.broker.transformer.TransformerEngine;
import ca.utoronto.msrg.padres.broker.transformer.advertisements.SimpleAdvertisementMerging;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageType;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class PostProcessorImpl implements PostProcessor {

	private BrokerCore brokerCore;

	// To enforce Siena subscription covering routing
	private final boolean subCoveringIsOn;

	private SubscriptionFilter subFilter;

	private final boolean advCoveringIsOn;

	private AdvertisementFilter advFilter;

	private final boolean totalOrderIsOn;

	private TotalOrderEngine totalOrderEngine;

	private final boolean topkIsOn;
	
	private final boolean mergingAdsIsOn;
	
	private final boolean isMonitor;
	
	private final boolean topkSubIsOn;

	private final boolean topkOptCover;

	private final int topkSubK;
	
	private final String topkSubType;
	
	private String topkSubSort;
	
	private TopkEngine topkEngine;
	
	private TopkSubEngine topkSubEngine;
	
	private TransformerEngine transformer;

    private int count = 0, outMsg = 0, numSubs = 0, realOutMsg = 0;
    
	public PostProcessorImpl(BrokerCore broker) {
		brokerCore = broker;
		// Covering-related code
		subCoveringIsOn = SubscriptionFilter.isSubCoveringOn(brokerCore.getBrokerConfig().getSubCovering());
		advCoveringIsOn = AdvertisementFilter.isAdvCoveringOn(brokerCore.getBrokerConfig().getAdvCovering());
		// Total Order
		totalOrderIsOn = brokerCore.getBrokerConfig().isTotalOrder() && !(brokerCore.isCycle());
		topkIsOn = brokerCore.getBrokerConfig().isTopk();
		topkSubIsOn = brokerCore.getBrokerConfig().isTopkSubs();
		topkSubK = brokerCore.getBrokerConfig().getTopKSubK();
		topkOptCover = brokerCore.getBrokerConfig().getTopKOptCover();
		topkSubType = brokerCore.getBrokerConfig().getTopKSubType();
		topkSubSort = brokerCore.getBrokerConfig().getTopKSubSort();
		mergingAdsIsOn = brokerCore.getBrokerConfig().isMergingAds();
		isMonitor = brokerCore.getBrokerConfig().isMonitor();
	}

	public void initialize() {
		subFilter = subCoveringIsOn ? new SubscriptionFilter(
				brokerCore.getBrokerConfig().getSubCovering(), brokerCore.getBrokerDestination(),
				brokerCore.getRouter().getSubscriptions()) : null;
		advFilter = advCoveringIsOn ? new AdvertisementFilter(
				brokerCore.getBrokerConfig().getAdvCovering(), brokerCore.getBrokerDestination(),
				brokerCore.getRouter().getAdvertisements()) : null;
		totalOrderEngine = totalOrderIsOn ? new TotalOrderEngine(brokerCore.getRouter()) : null;
		topkSubEngine = topkSubIsOn? new TopkSubEngine(brokerCore.getRouter(), topkSubK, topkSubType, topkSubSort) : null;
		topkEngine = topkIsOn ? new DesynchronizedChunkTopkEngine(brokerCore.getRouter(), brokerCore.getBrokerConfig().getTopk()) : null;
		transformer = mergingAdsIsOn ? new TransformerEngine() : null;
		
		if(mergingAdsIsOn)
			transformer.registerTransformer(new SimpleAdvertisementMerging(5.0, 4));
	}

	public void postprocess(Message msg, MessageType type, Set<Message> messageSet) {
		
		if(brokerCore.isRunning() && isMonitor){
			brokerCore.getSystemMonitor().setMatchTime(msg.getMessageID());
		}
		
		if (type.equals(MessageType.PUBLICATION)) {
			if (totalOrderIsOn) {
				totalOrderEngine.processMessage((PublicationMessage) msg, messageSet);
			}
			if(topkIsOn){
				topkEngine.processMessage((PublicationMessage) msg, messageSet);
			}
			if(topkSubIsOn){
				topkSubEngine.processMessage((PublicationMessage) msg, messageSet);
			}
            
//			if(!((PublicationMessage)msg).isControl()){
//                realOutMsg += messageSet.size();
//                topkSubEngine.processMessage((PublicationMessage) msg, messageSet);
//                outMsg += messageSet.size();
//                if(realOutMsg >= 10){ 
//                    System.out.println(realOutMsg + "," + outMsg + "," + numSubs + "," + brokerCore.getSubscriptions().size());
//                    count = 0;
//                    outMsg = 0;
//                    realOutMsg = 0;          
//                }       
//            } else {
//                if(!((PublicationMessage)msg).isControl()){       
//                    count++;
//                    outMsg += messageSet.size();         
//                    if(count  >= 10){
//                        System.out.println(count + "," + outMsg + "," + numSubs);
//                        count = 0;
//                        outMsg = 0;                  
//                    }
//                }               
//            }

		} else if (type.equals(MessageType.SUBSCRIPTION)) {
            if(!((SubscriptionMessage)msg).isControl()){
                numSubs++;              
            }
            
			if (subCoveringIsOn && !((SubscriptionMessage)msg).isControl()) {
				int k = 1;
				
				if(topkSubIsOn){
					k = topkSubK;
					topkSubEngine.handleSubscription((SubscriptionMessage)msg);
				}
				// Do not forward subscriptions that were covered by previously sent out
				// subscriptions
				subFilter.removeCoveredSubscriptions(messageSet, k, topkOptCover);
				
				if(topkSubIsOn){
					topkSubEngine.processSubscription(messageSet);
				}
				
//				if(!((SubscriptionMessage)msg).isControl())
//					System.out.println(messageSet);
//				if(numSubs % 999 == 0) {
//					System.out.println("[" + numSubs + "]");
//				}
//				if(numSubs % 9999 == 0) {
//					System.out.println(numSubs + "\n" + subFilter.coveringStats());
//				}
			}

			if (totalOrderIsOn || topkIsOn)
				SenderTagger.processMessage((SubscriptionMessage) msg);
			if (topkIsOn)
				topkEngine.processMessage((SubscriptionMessage) msg);
		} else if (type.equals(MessageType.COMPOSITESUBSCRIPTION)) {

		} else if (type.equals(MessageType.ADVERTISEMENT)) {
			if(mergingAdsIsOn) {
				transformer.transform(messageSet);
			}
			if (advCoveringIsOn) {
				advFilter.removeCoveredAdvertisements(messageSet);
			}
			if (subCoveringIsOn) {
				// Do not forward subscriptions that were covered by previously sent out
				// subscriptions Advertisement messages are untouched
				int k = 1;
				
				if(topkSubIsOn){
					k = topkSubK;
				}
				
				subFilter.removeCoveredSubscriptions(messageSet, k, topkOptCover);
			}
		} else if (type.equals(MessageType.UNSUBSCRIPTION)) {
			if (subCoveringIsOn) {
				// We may need to forward previously suppressed subscriptions that were not part of
				// the subscription covering set for a particular next hop.

				int k = 1;
				
				if(topkSubIsOn){
					k = topkSubK;
				}
				subFilter.removeCoveredUnsubscriptions(messageSet, k, topkOptCover);
			}
		} else if (type.equals(MessageType.UNCOMPOSITESUBSCRIPTION)) {

		} else if (type.equals(MessageType.UNADVERTISEMENT)) {

		} else if (type.equals(MessageType.UNDEFINED)) {

		} else {

		}
	}

}
