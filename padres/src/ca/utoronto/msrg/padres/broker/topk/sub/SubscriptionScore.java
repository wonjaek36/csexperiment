package ca.utoronto.msrg.padres.broker.topk.sub;

import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class SubscriptionScore implements Comparable<SubscriptionScore> {
	
	private SubscriptionMessage msg;
	private double score;
	
	public SubscriptionScore(SubscriptionMessage msg, double score){
		this.msg = msg;
		this.score = score;
	}
	
	@Override
	public int compareTo(SubscriptionScore target) {
		return Double.compare(this.score, target.score);
	}

	public SubscriptionMessage getMsg() {
		return msg;
	}

	public double getScore() {
		return score;
	}

}
