package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageDestination;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class FairSorter implements PublicationSorter {

	protected AbstractMap<MessageDestination, Integer> count;

	public FairSorter(){
		this.count = new HashMap<MessageDestination, Integer>();
	}
	
	@Override
	public List<SubscriptionScore> sort(List<SubscriptionScore> scores) {
		Collections.sort(scores);
		List<SubscriptionScore> sortedScores = new Vector<SubscriptionScore>();
		
		double highScore = 0.0;
		boolean first = true;
		double currentScore = 0.0;
		List<SubscriptionScore> ties = new Vector<SubscriptionScore>();
		
		for(SubscriptionScore score : scores){
			currentScore = score.getScore();
			
			if(first) {
				highScore = currentScore;
				first = false;
			} else if(highScore > currentScore) {
				highScore = currentScore;
				resolveTies(ties, sortedScores);
				ties.clear();
			} else if (highScore < currentScore) {
				System.out.println("Scoring error!");
			}
			
			ties.add(score);
		}
		
		resolveTies(ties, sortedScores);
		
		return sortedScores;
	}

	private void resolveTies(List<SubscriptionScore> ties, List<SubscriptionScore> sortedScores) {
		if(ties.isEmpty()){
			return;
		}
		
		RandomCollection<SubscriptionScore> tiebreaker = new RandomCollection<SubscriptionScore>();
		
		for(SubscriptionScore score : ties) {
			int weight = count.get(score.getMsg().getLastHopID());
			tiebreaker.add(weight, score);
		}
		
		sortedScores.addAll(tiebreaker.getRandomList());		
	}

	@Override
	public void handleSubscription(SubscriptionMessage msg) {
		if(msg.getCount() != null){
			count.put(msg.getCount().getHop(), msg.getCount().getCount());
		}
		else {
			MessageDestination hop = msg.getLastHopID();
			if(!hop.isBroker()){
				incrementCount(hop);
			}
		}
	}

	@Override
	public void processSubscription(Set<Message> messageSet) {
		for(Message msg : messageSet){
			((SubscriptionMessage)msg).setCount(new TopkSubCount(getOthersCount(msg.getNextHopID()),msg.getLastHopID()));
		}	
	}

	private void incrementCount(MessageDestination hop){
		if(count.containsKey(hop)){
			count.put(hop, count.get(hop)+1);
		} else {
			count.put(hop, 1);
		}		
	}

	private int getOthersCount(MessageDestination hop){
		int value = 0;
		
		for(MessageDestination key : count.keySet()){
			if(!hop.equals(key)){
				value += count.get(key);
			}
		}
		
		return value;
	}

}
