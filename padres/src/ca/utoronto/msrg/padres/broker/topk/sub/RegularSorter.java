package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class RegularSorter implements PublicationSorter {

	@Override
	public List<SubscriptionScore> sort(List<SubscriptionScore> scores) {
		
		Collections.sort(scores);
		
		List<SubscriptionScore> sortedScores = new Vector<SubscriptionScore>();
		
		double highScore = 0.0;
		boolean first = true;
		double currentScore = 0.0;
		List<SubscriptionScore> ties = new Vector<SubscriptionScore>();
		
		for(SubscriptionScore score : scores){
			currentScore = score.getScore();
			
			if(first) {
				highScore = currentScore;
				first = false;
			} else if(highScore > currentScore){
				highScore = currentScore;
				resolveTies(ties, sortedScores);
				ties.clear();
			} else if (highScore < currentScore){
				System.out.println("Scoring error!");
			}
			
			ties.add(score);
		}
		
		resolveTies(ties, sortedScores);
		
		return sortedScores;
	}

	private void resolveTies(List<SubscriptionScore> ties,
			List<SubscriptionScore> sortedScores) {
		Collections.shuffle(ties);
		sortedScores.addAll(ties);
	}

	@Override
	public void handleSubscription(SubscriptionMessage msg) {
	}

	@Override
	public void processSubscription(Set<Message> messageSet) {
	}

}
