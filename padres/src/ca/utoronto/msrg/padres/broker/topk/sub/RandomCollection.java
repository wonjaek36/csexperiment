package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.Collections;
import java.util.List;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;
import java.util.Vector;

public class RandomCollection<E> {
    private final NavigableMap<Integer, E> map = new TreeMap<Integer, E>();
    private final Random random;
    private int total = 0;

    public RandomCollection() {
        this(new Random());
    }

    public RandomCollection(Random random) {
        this.random = random;
    }

    public void add(int weight, E result) {
        if (weight <= 0) return;
        total += weight;
        map.put(total, result);
    }

    public E next() {
        int value = random.nextInt(total);
        return map.higherEntry(value).getValue();
    }
    
    public List<E> getRandomList(){
    	List<E> list = new Vector<E>();
    	List<Integer> randomNumbers = new Vector<Integer>();
    	
    	for(int i = 0; i < total; i++){
    		randomNumbers.add(i);
    	}
    	
    	Collections.shuffle(randomNumbers);
    	
    	for(Integer i : randomNumbers){
    		E current = map.higherEntry(i).getValue();
    		
    		if(!list.contains(current))
    			list.add(current);
    		
    		if(list.size() == map.size())
    			break;
    	}
    	
    	return list;
    }
}