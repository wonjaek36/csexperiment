package ca.utoronto.msrg.padres.broker.topk.sub;

import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class ConstantScorer implements PublicationScorer {
	
	private long value;
	
	public ConstantScorer(long value){
		this.value = value;
	}
	

	@Override
	public double getScore(PublicationMessage pub, AdvertisementMessage ad,
			SubscriptionMessage sub) {
		return Double.longBitsToDouble(this.value);
	}

}
