package ca.utoronto.msrg.padres.broker.topk.sub;

import java.io.Serializable;

import ca.utoronto.msrg.padres.common.message.MessageDestination;

public class TopkSubCount implements Serializable {

	private static final long serialVersionUID = -93347226880525785L;
	private int count;
	private MessageDestination hop;
	
	public TopkSubCount(int count, MessageDestination hop) {
		this.count = count;
		this.hop = hop;
	}

	public int getCount(){
		return this.count;
	}
	
	public MessageDestination getHop(){
		return this.hop;
	}
	
	public String toString(){
		return "[HopID: " + hop + ", Count: " + count + "]";
	}
}
