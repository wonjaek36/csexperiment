package ca.utoronto.msrg.padres.broker.topk.sub;

public class PublicationScorerFactory {
	
	public enum ScorerType {
		random, constant
	}
	
	private ScorerType type;
	
	public void setType(String type){
		this.type = ScorerType.valueOf(type);
	}
	
	public PublicationScorer createPublicationScorer(){
		switch(this.type){
			case random :
				return new RandomScorer(System.currentTimeMillis());
			case constant :
				return new ConstantScorer(System.currentTimeMillis());
			default : 
				return null;
		}
	}

}
