package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.Random;

import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class RandomScorer implements PublicationScorer {

	private long seed;
	
	public RandomScorer(long seed){
		this.seed = seed;
	}
	
	@Override
	public double getScore(PublicationMessage pub, AdvertisementMessage ad, SubscriptionMessage sub) {
		
		int adScore = 0;
		
		if(ad != null)
			adScore = ad.getMessageID().hashCode();
		
		Random gen = new Random(seed+pub.getMessageID().hashCode()+sub.getMessageID().hashCode()+adScore);
		
		return gen.nextDouble();
	}

}
