package ca.utoronto.msrg.padres.broker.topk.sub;

import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public interface PublicationScorer {
	public double getScore(PublicationMessage pub, AdvertisementMessage ad, SubscriptionMessage sub);
}
