package ca.utoronto.msrg.padres.broker.topk.sub;


public class PublicationSorterFactory {
	
	public enum SorterType {
		regular, fair
	}

	private SorterType type;
	
	public void setType(String type){
		this.type = SorterType.valueOf(type);
	}
	
	public PublicationSorter createPublicationSorter(){
		switch(this.type){
			case regular :
				return new RegularSorter();
			case fair :
				return new FairSorter();
			default : 
				return null;
		}
	}
	
	public SorterType getType(){
		return type;
	}
}
