package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.List;
import java.util.Set;

import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public interface PublicationSorter {
	public List<SubscriptionScore> sort(List<SubscriptionScore> scores);
	public void handleSubscription(SubscriptionMessage msg);
	public void processSubscription(Set<Message> messageSet);
}
