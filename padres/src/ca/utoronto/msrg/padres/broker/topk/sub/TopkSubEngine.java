package ca.utoronto.msrg.padres.broker.topk.sub;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import ca.utoronto.msrg.padres.broker.router.Router;
import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageDestination;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

public class TopkSubEngine {

	protected int defaultK;
	protected Router router;
	protected PublicationScorer scorer;
	protected PublicationScorerFactory scorerFactory;
	protected PublicationSorter sorter;
	protected PublicationSorterFactory sorterFactory;
	
	public TopkSubEngine(Router router, int k, String scorerType, String sorterType) {
		this.router = router;
		this.scorerFactory = new PublicationScorerFactory();
		this.scorerFactory.setType(scorerType);
		this.scorer= scorerFactory.createPublicationScorer();
		this.sorterFactory = new PublicationSorterFactory();
		this.sorterFactory.setType(sorterType);
		this.sorter = sorterFactory.createPublicationSorter();
		this.defaultK = k;
	}

	public void processMessage(PublicationMessage msg, Set<Message> messageSet) {
		if(msg.isControl())
			return;
		
		if(msg.getTopkSubCount() == -1) { // source broker, do top-k
			
			AdvertisementMessage ad = null;
			
			if(msg.getAdvertisementId() != null){
				ad = router.getAdvertisement(msg.getAdvertisementId());
			}
			
			List<SubscriptionMessage> subs = router.getSubscriptions(msg);		
			List<SubscriptionScore> topkScores = new Vector<SubscriptionScore>();
						
			for(SubscriptionMessage sub : subs) {
				double score = scorer.getScore(msg, ad, sub);
				
				topkScores.add(new SubscriptionScore(sub,score));
			}
			
			topkScores = this.sorter.sort(topkScores);

			List<SubscriptionMessage> selectedSubs = new Vector<SubscriptionMessage>();
			
			int k = defaultK;
			int count = 0;
			
			if(ad != null && ad.getTopkSubK() != -1)
				k = ad.getTopkSubK();

			for(SubscriptionScore score : topkScores){
				selectedSubs.add(score.getMsg());
				count++;
				
				if(count == k){
					break;
				}
			}
												
			Map<MessageDestination, Integer> selectedHops = new HashMap<MessageDestination, Integer>();
			
			for(SubscriptionMessage sub : selectedSubs){
				if(!selectedHops.containsKey(sub.getLastHopID())){
					selectedHops.put(sub.getLastHopID(), 1);
				}
				
				selectedHops.put(sub.getLastHopID(), 1+selectedHops.get(sub.getLastHopID()));
			}
			
			Set<Message> tempMessageSet = new HashSet<Message>(messageSet);
			
			for(Message message : tempMessageSet){
				if(!selectedHops.containsKey(message.getNextHopID())){
					messageSet.remove(message);
				} else {
					((PublicationMessage)message).setTopkSubCount(selectedHops.get(message.getNextHopID()));
				}
			}
			
		} else { // forward mode
			
			AdvertisementMessage ad = null;
			
			if(msg.getAdvertisementId() != null){
				ad = router.getAdvertisement(msg.getAdvertisementId());
			}
			
			List<SubscriptionMessage> subs = router.getSubscriptions(msg);		
			List<SubscriptionScore> topkScores = new Vector<SubscriptionScore>();
						
			for(SubscriptionMessage sub : subs) {
				double score = scorer.getScore(msg, ad, sub);
				if(!sub.getLastHopID().equals(msg.getLastHopID()))				
					topkScores.add(new SubscriptionScore(sub,score));
			}
			
			topkScores = this.sorter.sort(topkScores);

			List<SubscriptionMessage> selectedSubs = new Vector<SubscriptionMessage>();
			
			int k = msg.getTopkSubCount();
			int count = 0;

			for(SubscriptionScore score : topkScores){
				selectedSubs.add(score.getMsg());
				count++;
				
				if(count == k){
					break;
				}
			}
												
			Map<MessageDestination, Integer> selectedHops = new HashMap<MessageDestination, Integer>();
			
			for(SubscriptionMessage sub : selectedSubs){
				if(!selectedHops.containsKey(sub.getLastHopID())){
					selectedHops.put(sub.getLastHopID(), 1);
				}
				
				selectedHops.put(sub.getLastHopID(), 1+selectedHops.get(sub.getLastHopID()));
			}
			
			Set<Message> tempMessageSet = new HashSet<Message>(messageSet);
			
			for(Message message : tempMessageSet){
				if(!selectedHops.containsKey(message.getNextHopID())){
					messageSet.remove(message);
				} else {
					((PublicationMessage)message).setTopkSubCount(selectedHops.get(message.getNextHopID()));
				}
			}
		}
	}

	public int getK() {
		return defaultK;
	}
	
	public void handleSubscription(SubscriptionMessage msg) {
		sorter.handleSubscription(msg);
	}

	public void processSubscription(Set<Message> messageSet) {
		sorter.processSubscription(messageSet);
	}
}
