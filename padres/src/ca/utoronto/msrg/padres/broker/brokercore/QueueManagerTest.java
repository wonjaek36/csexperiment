// =============================================================================
// This file is part of The PADRES Project.
//
// For more information, see http://www.msrg.utoronto.ca
//
// Copyright (c) 2003 Middleware Systems Research Group, University of Toronto
// =============================================================================
// $Id$
// =============================================================================
/*
 * Created on 17-Jul-2003
 *
 */
package ca.utoronto.msrg.padres.broker.brokercore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import ca.utoronto.msrg.padres.broker.controller.OverlayManager;
import ca.utoronto.msrg.padres.broker.monitor.SystemMonitor;
import ca.utoronto.msrg.padres.common.comm.CommSystem;
import ca.utoronto.msrg.padres.common.comm.CommSystem.HostType;
import ca.utoronto.msrg.padres.common.comm.MessageListenerInterface;
import ca.utoronto.msrg.padres.common.comm.MessageQueue;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageDestination;
import ca.utoronto.msrg.padres.common.message.MessageType;
import ca.utoronto.msrg.padres.common.message.Predicate;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;

/**
 * The QueueManagerTest handles the collection of MessageQueues for all destinations in the broker.
 * 
 * @author eli
 */
public class QueueManagerTest extends QueueManager {

	public ConcurrentHashMap <MessageDestination, Long> msgsPerQueue;
	public Timer timer;
	public long firstMsg = 0;
	public long lastMsg = 0;
	
	public long enQueueTestCall = 0;
	boolean isTimerAlive = false;
	
	public class Recorder extends TimerTask {

		public long LastMsgCount = 0;
		public long sameCount = 0;
		@Override
		public void run() {
			
			long totalMsg = 0;
			System.out.println("---Recorder---");
			//System.out.println("enQueueCall: " + enQueueCall);
			//System.out.println("enQueueTestCall: " + enQueueTestCall);
			for(Map.Entry<MessageDestination, Long> entry : msgsPerQueue.entrySet()) {
				MessageDestination key = entry.getKey();
				Long count = entry.getValue();
				
				if( key.toString().contains("Client") ) {
					System.out.println("Count: " + count + " " + key.toString());
					totalMsg += count;
				} else {
					System.out.println("Count: " + count + " " + key.toString());
				}
			}
			if ( totalMsg > LastMsgCount ) {
				LastMsgCount = totalMsg;
				sameCount = 0;
			} else if(LastMsgCount == totalMsg && totalMsg != 0 ){
				sameCount += 1;
			} else if(totalMsg != 0){
				System.out.println("LastMsg: " + LastMsgCount + " TotalMsg: " + totalMsg);
				System.out.println("TimerLastMsg Error Occurred");
				isTimerAlive = false;
				synchronized(timer){
					timer.notifyAll();
					timer.cancel();
				}
			}
			
			if( sameCount >= 16 ) {
				isTimerAlive = false;
				synchronized(timer){
					timer.notifyAll();
					timer.cancel();
				}
			}
		
			long difference = (lastMsg - firstMsg) / 1000;
			System.out.println("Last Message Time: " + lastMsg);
			System.out.println("First Message Time: " + firstMsg);
			System.out.println("Last Time - First Message Time: " +  (lastMsg - firstMsg));
			System.out.println("Total Receive Message: " + totalMsg);
			System.out.println("Same Count: " + sameCount);
			System.out.println("Throughput: " + (((double)totalMsg) / ((double)(difference))));
		}
	}
	public QueueManagerTest(BrokerCore brokerCore, String s, String p) throws BrokerCoreException {
		super(brokerCore);
		// TODO Auto-generated constructor stub
		Integer sync = ((BrokerCoreSubTest)brokerCore).getSyncTime();
		Integer adv = ((BrokerCoreSubTest)brokerCore).getAdvWaitTime();
		Integer period = Integer.parseInt(p);
		msgsPerQueue = new ConcurrentHashMap<MessageDestination, Long>();
		timer = new Timer();
		timer.schedule(new Recorder(), (sync+adv+10)*1000, period*1000);
		isTimerAlive = true;
	}

	/**
	 */
	public synchronized void enQueue(Message msg, MessageDestination destination) {
		
		
		enQueueTestCall += 1;
		super.enQueue(msg, destination);
		
		//System.out.println(msg.getNextHopID().toString() + " " + msg.getType().toString());
		
		if( msg.getNextHopID() != null && msg.getType() == MessageType.PUBLICATION) {
			
			if( firstMsg == 0 && (msg.getNextHopID().toString()).contains("Client") ) {
				firstMsg = System.currentTimeMillis();
				//then record current Time;
			}
			if( lastMsg < System.currentTimeMillis() && (msg.getNextHopID().toString()).contains("Client") ) {
				lastMsg = System.currentTimeMillis();
				//then record current Time;
			}
			if( msgsPerQueue.get(msg.getNextHopID()) == null ) {
				msgsPerQueue.put(msg.getNextHopID(), (long) 0);
			}
			//System.err.println(msg.toString());
			Long cnt = msgsPerQueue.get(msg.getNextHopID()) + 1;
			msgsPerQueue.put(msg.getNextHopID(), cnt);
		}
	}
}
