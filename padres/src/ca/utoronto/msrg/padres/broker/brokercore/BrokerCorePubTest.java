package ca.utoronto.msrg.padres.broker.brokercore;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.log4j.Logger;

import ca.utoronto.msrg.padres.client.CommandHandler;
import ca.utoronto.msrg.padres.client.CommandResult;
import ca.utoronto.msrg.padres.common.comm.CommunicationException;
import ca.utoronto.msrg.padres.common.comm.CommSystem.HostType;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.MessageDestination;
import ca.utoronto.msrg.padres.common.message.Publication;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.client.BrokerState;
import ca.utoronto.msrg.padres.client.Client;

public class BrokerCorePubTest extends BrokerCore{
	
	private HashMap<String, String> options;
	private static String BROKER_NUMBER = "broker_number";
	private static String PUB_INFO_FILE = "info_filepath";
	private static String BROKER_URI = "uri";
	private static String EXECUTE_TIME = "exec_time";
	private static String SYNC_TIME = "sync_time";
	private static String SUB_WAIT_TIME = "sub_wait_time";
	
	private ArrayList<Integer> classNumber;
	private ArrayList<Integer> classPeriod;
	private ArrayList<Integer> classValue;
	private ArrayList<Integer> currentValue;
	public ArrayList<Integer> msgCount;
	private String pesudoClientID;
	private String brokerURI;
	private Integer brokerMsgCnt;

	/*protected QueueManager createQueueManager() throws BrokerCoreException {
		return new QueueManagerTest(this);
	}*/
	
	public BrokerCorePubTest(String[] brokerOption, String[] args) throws BrokerCoreException {
		super(brokerOption, true);

		brokerMsgCnt = 0;
		options = new HashMap<String, String>();
		options.put(BROKER_NUMBER, null);
		options.put(PUB_INFO_FILE, null);
		options.put(BROKER_URI, null);
		options.put(EXECUTE_TIME, null);
		options.put(SYNC_TIME, null);
		options.put(SUB_WAIT_TIME, null);
		
		for(int i=0;i<args.length;i++) {
			String key = args[i];
			if (key.startsWith("-")) {
				String o = key.substring(1);
				if (options.containsKey(o)) {
					options.put(o, args[i+1]);
				}
			}
		}
		
		System.err.println("Broker Number: " + options.get(BROKER_NUMBER));
		System.err.println("Publisher Information File: " + options.get(PUB_INFO_FILE));
		System.err.println("Broker URI: " + options.get(BROKER_URI));
		System.err.println("Execution Time: " + options.get(EXECUTE_TIME));
		System.err.println("Sync Time: " + options.get(SYNC_TIME));
		System.err.println("Sub_Wait Time: " + options.get(SUB_WAIT_TIME));
		
		pesudoClientID = "Client" + options.get(BROKER_NUMBER);
		brokerURI = options.get(BROKER_URI);
	}
	
	static String[] extractBrokerOption(String[] args) {
		ArrayList<String> inputBrokerOption = new ArrayList<String>();
		String[] brokerOptions = BrokerConfig.getCommandLineKeys();
		int argsSize = args.length;
		for (int i=0;i<argsSize;i++) {
			String inputKey = args[i];
			if (inputKey.startsWith("-") == false)
				continue;
			
			String key = inputKey.substring(1);
			for(String brokerOption: brokerOptions) {
				if(  brokerOption.contains(key) && brokerOption.endsWith(":") ) {
					inputBrokerOption.add(args[i]);
					inputBrokerOption.add(args[i+1]);
					break;
				} else if( brokerOption.contains(key) ){
					inputBrokerOption.add(args[i]);
					break;
				}
			}
		}
		
		int x = inputBrokerOption.size();
		String[] ret = new String[x];
		ret = inputBrokerOption.toArray(ret);
		
		return ret;
	}
	
	public String createAdvertiseMsg(int idx, int value) {
		String advMsg = "a [class,eq,'c" + idx + "']," +
				"[name,eq,'v" + idx + "']," +
				"[value,eq,'"+value+"']";
		return advMsg;
	}
	
	public String createPublishMsg(int idx, int value) {
		String pubMsg = "p [class,'c"+idx+"'],"+
				"[name,'v"+idx+"'],"+
				"[value,'"+value+"']";
		return pubMsg;
	}
	
	
	public CommandResult handleCommand(String commandString) throws ParseException {
		CommandResult cmdResult = new CommandResult(commandString);
		return cmdResult;
	}
	
	//handleCommand - Client.java
	//runCommand - Client.java
	//advertisement - BaseCommandHandler
	//aAdvertisementMessage - Client.java
	/*
	public AdvertisementMessage advertise(Advertisement adv, String brokerURI)
			throws ClientException {
		if (!isConnected())
			throw new ClientException("Not connected to any broker");
		try {
			if (brokerURI == null || brokerURI.equals(""))
				brokerURI = defaultBrokerAddress.getNodeURI();
			BrokerState brokerState = getBrokerState(brokerURI);
			if (brokerState == null) {
				throw new ClientException("Not connected to broker " + brokerURI);
			}
			MessageDestination clientDest = MessageDestination.formatClientDestination(clientID,
					brokerState.getBrokerAddress().getNodeURI());
			AdvertisementMessage advMsg = new AdvertisementMessage(adv,
					getNextMessageID(brokerState.getBrokerAddress().getNodeURI()), clientDest);
			String msgID = brokerState.getMsgSender().send(advMsg, HostType.CLIENT);
			advMsg.setMessageID(msgID);
			if (clientConfig.detailState)
				brokerState.addAdvMsg(advMsg);
			return advMsg;
		} catch (CommunicationException e) {
			throw new ClientException(e.getMessage());
		}
	}*/
	protected String getNextMessageID(Integer msgCount) {
		return String.format("CM-%s-%s-%d", pesudoClientID, brokerURI, msgCount);
	}
	public void process() {
		int nClass = classNumber.size();
		System.err.println("Adv Start / Time: " + System.currentTimeMillis());
		
		for(int i=0;i<nClass;i++) {
			int maxValue = classValue.get(i);
			String advMsgStr = null;
			for(int j=0;j<=maxValue;j++){
				advMsgStr = createAdvertiseMsg(classNumber.get(i), j);
				CommandResult cmd = null;
				try {
					cmd = handleCommand(advMsgStr);
				} catch (ParseException e1) {
					System.err.println("Error: handleCommand fails to create cmdResult with adv");
					System.err.println("---------------------------------------------------");
					e1.printStackTrace();
					return;
				}
				try {
					//BrokerState brokerState = getBrokerState(brokerURI);
					Advertisement newAdv = MessageFactory.createAdvertisementFromString(cmd.cmdData[0]);
					//brokerURI
					//BrokerState brokerState = getBrokerState(brokerURI);
					//brokerState.getBrokerAddress().getNodeURI() = brokerURI
					/*if (brokerState == null) {
						throw new ClientException("Not connected to broker " + brokerURI);
					}*/
					MessageDestination clientDest = MessageDestination.formatClientDestination(pesudoClientID, brokerURI);
					AdvertisementMessage advMsg = new AdvertisementMessage(newAdv,
							getNextMessageID(brokerMsgCnt++), clientDest);
					
					inputQueue.addMessage(advMsg);
					
					//System.err.println(advMsgStr); disable print adv Message
				} catch (ParseException e) {
					System.err.println("Error: MessageFactory fails to create advertisement");
					System.err.println("---------------------------------------------------");
					e.printStackTrace();
				}
			}
		}
		
		System.err.println("Adv done / Time: " + System.currentTimeMillis());
		try { Thread.sleep(getSubWaitTime() * 1000); } catch(Exception e) { }
		System.err.println("Publish start / Time: " + System.currentTimeMillis());
		
		int time_limit = Integer.parseInt(options.get(EXECUTE_TIME));
		for(int t=1;t<=time_limit;t++) {
			long totalMsgCountPeriod = 0;
			long startTime = System.currentTimeMillis();
			for(int i=0;i<nClass;i++) {
				String pubMsgStr = null;
				int period = classPeriod.get(i);
				int msgNum = currentValue.get(i);
				
				if( t % period == 0 ) {
					pubMsgStr = createPublishMsg(classNumber.get(i), msgNum);
					CommandResult cmd = null;
					
					try {
						cmd = handleCommand(pubMsgStr);
					} catch (ParseException e1) {
						System.err.println("Error: handleCommand fails to create cmdResult with adv");
						System.err.println("------------------------------AFTER_---------------------");
						e1.printStackTrace();
						return;
					}
					try {
						Publication newPub = MessageFactory.createPublicationFromString(cmd.cmdData[0]);
						//brokerURI
						//BrokerState brokerState = getBrokerState(brokerURI);
						//brokerState.getBrokerAddress().getNodeURI() = brokerURI
						/*if (brokerState == null) {
							throw new ClientException("Not connected to broker " + brokerURI);
						}*/
						MessageDestination clientDest = MessageDestination.formatClientDestination(pesudoClientID, brokerURI);
						PublicationMessage pubMsg = new PublicationMessage(newPub, 
								getNextMessageID(brokerMsgCnt++), clientDest);
						
						inputQueue.addMessage(pubMsg);
						//queueManager.enQueue(pubMsg);
						
						//System.err.println(pubMsgStr); disable print pubMsg
						msgCount.set(i, msgCount.get(i)+1);
						totalMsgCountPeriod += 1;
					} catch (ParseException e) {
						System.err.println("Error: MessageFactory fails to create advertisement");
						System.err.println("---------------------------------------------------");
						e.printStackTrace();
					}
					if( msgNum >= classValue.get(i) )
						currentValue.set(i, 1);
					else
						currentValue.set(i, msgNum + 1);
				}
				
			}
			//wait 1 second
			long endTime = System.currentTimeMillis();
			long diff = endTime - startTime;
			long wait = 1000-diff < 0 ? 0 : 1000-diff;
			System.out.print("Start: " + startTime + "/ End: " + endTime + "/ Process: " + diff + "/ Msg: " + totalMsgCountPeriod);
			System.out.println("/ Wait: " + wait);
			try { Thread.sleep(wait); } catch (Exception e) { }
		}
	}
	
	public Integer getSyncTime() {
		String syncTime = options.get(SYNC_TIME);
		return (Integer.parseInt(syncTime));
	}
	
	public Integer getSubWaitTime() {
		String subWaitTime = options.get(SUB_WAIT_TIME);
		return (Integer.parseInt(subWaitTime));
	}

	public void readClassInformation() {
		try {
			@SuppressWarnings("resource")
			Scanner pubInfoScanner = new Scanner(new File(options.get(PUB_INFO_FILE)));
			int nPub = pubInfoScanner.nextInt();
			classNumber = new ArrayList<Integer>();
			classPeriod = new ArrayList<Integer>();
			classValue = new ArrayList<Integer>();
			currentValue = new ArrayList<Integer>();
			msgCount = new ArrayList<Integer>();
			
			for(int i=0;i<nPub;i++) {
				classNumber.add(pubInfoScanner.nextInt());
				currentValue.add(1);
				msgCount.add(0);
			}
			for(int i=0;i<nPub;i++)
				classPeriod.add(pubInfoScanner.nextInt());
			for(int i=0;i<nPub;i++)
				classValue.add(pubInfoScanner.nextInt());
			
		} catch(Exception e) {
			System.err.println("Error: readClassInformation");
			e.printStackTrace();
		}
	}
	
	public void printMsgCount() {
		long totalCount = 0;
		for(int i=0;i<msgCount.size();i++) {
			System.out.print("Class("+classNumber.get(i)+"):"+msgCount.get(i) + "/ ");
			totalCount += msgCount.get(i);
		}
		System.out.println("Total Msg Count: " + totalCount);
	}
	public static void main(String[] args) {
		try {
			String[] brokerOption = extractBrokerOption(args);
			BrokerCorePubTest pubBrokerCore = new BrokerCorePubTest(brokerOption, args);
			
			pubBrokerCore.readClassInformation();
			pubBrokerCore.initialize();
			try { Thread.sleep(pubBrokerCore.getSyncTime() * 1000); } catch(Exception e) { }
			//Waiting for Initialization of All broker
			
			//MessageFactory.createPublicationFromString(cmd.cmdData[0]);
			pubBrokerCore.process();
			//Message
			//brokerCore.createQueueManager();
			
			//CommandResult cmdResult1 = new CommandResult(pubBrokerCore.createPublishMsg(1, 2));
			//CommandResult cmdResult2 = new CommandResult(pubBrokerCore.createAdvertiseMsg(2, 2));
			/*if (!cmdResult.isError()) {
				CommandHandler handler = getCommandHandler(cmdResult.command);
				if (handler == null) {
					cmdResult.errMsg = "Command " + cmdResult.command + " is not supported\n"
							+ "Type 'help' for more info";
				} else {
					handler.runCommand(cmdResult);
				}
			}*/
			//System.out.println(cmdResult2.toString());
			//System.out.println(cmdResult1.toString());
			
			pubBrokerCore.shutdown();
			
			pubBrokerCore.printMsgCount();
			System.err.println("pubBrokerCore Done");
			
			System.exit(0);
		} catch (Exception e) {
			// log the error the system error log file and exit
			Logger sysErrLogger = Logger.getLogger("SystemError");
			if (sysErrLogger != null)
				sysErrLogger.fatal(e.getMessage() + ": " + e);
			e.printStackTrace();
			System.exit(1);
		}
	}
}

