package ca.utoronto.msrg.padres.broker.brokercore;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import ca.utoronto.msrg.padres.client.BrokerState;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.client.CommandResult;
import ca.utoronto.msrg.padres.common.comm.CommunicationException;
import ca.utoronto.msrg.padres.common.comm.CommSystem.HostType;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.CompositeSubscription;
import ca.utoronto.msrg.padres.common.message.CompositeSubscriptionMessage;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageDestination;
import ca.utoronto.msrg.padres.common.message.Subscription;
import ca.utoronto.msrg.padres.common.message.SubscriptionMessage;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;

public class BrokerCoreSubTest extends BrokerCore{
	//private QueueManagerTest queueManager;
	private HashMap<String, String> options;
	private static String BROKER_NUMBER = "broker_number";
	private static String PUB_INFO_FILE = "info_filepath";
	private static String BROKER_URI = "uri";
	//private static String EXECUTE_TIME = "exec_time"; deprecated
	private static String SYNC_TIME = "sync_time";
	private static String RECORD_PERIOD = "record_period";
	private static String ADV_WAIT_TIME = "adv_wait_time";
	
	private ArrayList<Integer> classNumber;
	private ArrayList<Integer> classValue;
	private ArrayList<Integer> currentValue;
	private ArrayList<Integer> msgCount;
	private String pesudoClientID;
	private String brokerURI;
	private Integer brokerMsgCnt;
	public boolean isSubscriptionEnrolled = false;

	protected void initQueueManager() throws BrokerCoreException {
		queueManager = createQueueManager();
		brokerCoreLogger.info("Queue Manager is created");
	}

	protected QueueManager createQueueManager() throws BrokerCoreException {
		return new QueueManagerTest(this,options.get(SYNC_TIME), options.get(RECORD_PERIOD));
	}
	
	public BrokerCoreSubTest(String[] brokerOption, String[] args) throws BrokerCoreException {
		super(brokerOption, true);
		
		brokerMsgCnt = 0;
		for( String bo : brokerOption ) {
			System.out.println("brokerOption: " + bo);
		}
		options = new HashMap<String, String>();
		options.put(BROKER_NUMBER, null);
		options.put(PUB_INFO_FILE, null);
		options.put(BROKER_URI, null);
		//options.put(EXECUTE_TIME, null);
		options.put(SYNC_TIME, null);
		options.put(RECORD_PERIOD, "30");
		options.put(ADV_WAIT_TIME, "60");
		
		
		for(int i=0;i<args.length;i++) {
			String key = args[i];
			if (key.startsWith("-")) {
				String o = key.substring(1);
				if (options.containsKey(o)) {
					options.put(o, args[i+1]);
				}
			}
		}
		
		System.err.println("Broker Number: " + options.get(BROKER_NUMBER));
		System.err.println("Publisher Information File: " + options.get(PUB_INFO_FILE));
		System.err.println("Broker URI: " + options.get(BROKER_URI));
		//System.err.println("Execution Time: " + options.get(EXECUTE_TIME));
		System.err.println("Sync Time: " + options.get(SYNC_TIME));
		System.err.println("Adv time: " + options.get(ADV_WAIT_TIME));
		
		pesudoClientID = "Client" + options.get(BROKER_NUMBER);
		brokerURI = options.get(BROKER_URI);
	}
	
	static String[] extractBrokerOption(String[] args) {
		ArrayList<String> inputBrokerOption = new ArrayList<String>();
		String[] brokerOptions = BrokerConfig.getCommandLineKeys();
		int argsSize = args.length;
		for (int i=0;i<argsSize;i++) {
			String inputKey = args[i];
			if (inputKey.startsWith("-") == false)
				continue;
			
			String key = inputKey.substring(1);
			for(String brokerOption: brokerOptions) {
				if(  brokerOption.contains(key) && brokerOption.endsWith(":") ) {
					inputBrokerOption.add(args[i]);
					inputBrokerOption.add(args[i+1]);
					break;
				} else if( brokerOption.contains(key) ){
					inputBrokerOption.add(args[i]);
					break;
				}
			}
		}
		
		int x = inputBrokerOption.size();
		String[] ret = new String[x];
		ret = inputBrokerOption.toArray(ret);
		
		return ret;
	}
	
	public String createSubscribeMsg(int idx, int value) {
		String subMsg = "s [class,eq,'c" + idx + "']," +
				"[name,eq,'v" + idx + "']," +
				"[value,eq,'"+value+"']";
		return subMsg;
	}
	
	public String createCompositeSubscribeMsg(int idx1, int idx2, int value) {
		String csubMsg = "cs {{" +
						"[class,eq,'c"+idx1+"'],"+
						"[name,eq,'v"+idx1+"'],"+
						"[value,eq,'"+value+"']"+
						"}&{"+
						"[class,eq,'c"+idx2+"'],"+
						"[name,eq,'v"+idx2+"'],"+
						"[value,eq,'"+value+"']"+
						"}}";
		return csubMsg;
	}
	
	public void readClassInformation() {
		try {
			@SuppressWarnings("resource")
			Scanner pubInfoScanner = new Scanner(new File(options.get(PUB_INFO_FILE)));
			int nPub = pubInfoScanner.nextInt();
			classNumber = new ArrayList<Integer>();
			classValue = new ArrayList<Integer>();
			currentValue = new ArrayList<Integer>();
			msgCount = new ArrayList<Integer>();
			
			for(int i=0;i<nPub;i++) {
				classNumber.add(pubInfoScanner.nextInt());
				currentValue.add(1);
				msgCount.add(0);
			}
			for(int i=0;i<nPub;i++)
				classValue.add(pubInfoScanner.nextInt());
			
		} catch(Exception e) {
			System.err.println("Error: readClassInformation");
			e.printStackTrace();
		}
	}

	public CommandResult handleCommand(String commandString) throws ParseException {
		CommandResult cmdResult = new CommandResult(commandString);
		return cmdResult;
	}
	protected String getNextMessageID(Integer msgCount) {
		return String.format("CM-%s-%s-%d", pesudoClientID, brokerURI, msgCount);
	}
	public void process() throws Exception {
		//atomic subscription Test
		int nClass = classNumber.size();

		System.out.println("Sub Start / Time: " + System.currentTimeMillis());
		for(int i=0;i<nClass;i+=2) {
			if( i+1 == nClass ) continue;
			
			int maxValue1 = classValue.get(i);
			int maxValue2 = classValue.get(i+1);
			String csubMsgStr = null;
			for(int j=0;j<=maxValue1 && j<=maxValue2;j++){
				csubMsgStr = createCompositeSubscribeMsg(classNumber.get(i), classNumber.get(i+1), j);
				CommandResult cmd = null;
				try {
					cmd = handleCommand(csubMsgStr);
				} catch (ParseException e1) {
					System.err.println("Error: handleCommand fails to create cmdResult with adv");
					System.err.println("---------------------------------------------------");
					e1.printStackTrace();
					return;
				}
				try {
					////////////////////Composite subscription
					CompositeSubscription newCS = new CompositeSubscription(csubMsgStr);
					if (newCS.getSubscriptionMap() == null || newCS.getSubscriptionMap().size() == 0) {
						throw new ClientException("Composite Subscription syntax error");
					}
						
					MessageDestination clientDest = MessageDestination.formatClientDestination(pesudoClientID,
							brokerURI);
					CompositeSubscriptionMessage newCSMsg = new CompositeSubscriptionMessage(newCS,
							getNextMessageID(brokerMsgCnt++), clientDest);
					
					inputQueue.addMessage(newCSMsg);
					
					/////////////////////
					/*Subscription newSub = MessageFactory.createSubscriptionFromString(cmd.cmdData[0]);
					if (newSub.getClassVal() == null) {
						throw new Exception("Subscription syntax error");
					}
					MessageDestination clientDest = MessageDestination.formatClientDestination(pesudoClientID, brokerURI);
					SubscriptionMessage subMsg = new SubscriptionMessage(newSub,
							getNextMessageID(brokerMsgCnt), clientDest);
					
					inputQueue.addMessage(subMsg);*/

					//System.err.println(csubMsgStr); disable print composite subscription
				} catch (Exception e) {
					System.err.println("Error: MessageFactory fails to create advertisement");
					System.err.println("---------------------------------------------------");
					e.printStackTrace();
				}
			}
		}
		System.out.println("Sub done / Time: " + System.currentTimeMillis());
		isSubscriptionEnrolled = true;
		//((QueueManagerTest)this.queueManager).timer
		synchronized( ((QueueManagerTest)this.queueManager).timer ){
			//Thread.currentThread().wait();
			System.out.println("This thread will be sleeping and waiting for Recoder");
			((QueueManagerTest)this.queueManager).timer.wait();
			System.out.println("Now, this thread is awake.");
		}
		System.out.println("This thread is notified");
	}

	public Integer getSyncTime() {
		String syncTime = options.get(SYNC_TIME);
		return (Integer.parseInt(syncTime));
	}
	public Integer getAdvWaitTime() {
		String advWaitTime = options.get(ADV_WAIT_TIME);
		return (Integer.parseInt(advWaitTime));
	}
	
	public static void main(String[] args) {
				
		try {
			
			String[] brokerOption = extractBrokerOption(args);
			BrokerCoreSubTest subBrokerCore = new BrokerCoreSubTest(brokerOption, args);
			
			subBrokerCore.readClassInformation();
			subBrokerCore.initialize();
			
			try { Thread.sleep(subBrokerCore.getSyncTime() * 1000); } catch(Exception e) { }
			try { Thread.sleep(subBrokerCore.getAdvWaitTime() * 1000); } catch(Exception e) { }
			
			subBrokerCore.process();
			
			subBrokerCore.shutdown();
			
			System.out.println("---subBrokerCore Done---");
			//((QueueManagerTest)subBrokerCore.queueManager).timer.cancel();
			ConcurrentHashMap<MessageDestination, Long> queue = ((QueueManagerTest)subBrokerCore.queueManager).msgsPerQueue;
			long totalMsg = 0;
			for(Map.Entry<MessageDestination, Long> entry : queue.entrySet()) {
				MessageDestination key = entry.getKey();
				Long count = entry.getValue();
				
				if( key.toString().contains("Client") ) {
					System.out.println("Count: " + count + " " + key.toString());
					totalMsg += count;
				}
			}
			long lastMsg = ((QueueManagerTest)subBrokerCore.queueManager).lastMsg;
			long firstMsg = ((QueueManagerTest)subBrokerCore.queueManager).firstMsg;
			long difference = (lastMsg - firstMsg) / 1000;
			System.out.println("Last Time: " + lastMsg);
			System.out.println("First Message Time: " + firstMsg);
			System.out.println("Last Time - First Message Time: " +  (lastMsg - firstMsg));
			System.out.println("Total Receive Message: " + totalMsg);
			System.out.println("Throughput: " + (((double)totalMsg) / ((double)difference)));
			
			System.exit(0);
		} catch (Exception e) {
			// log the error the system error log file and exit
			Logger sysErrLogger = Logger.getLogger("SystemError");
			if (sysErrLogger != null)
				sysErrLogger.fatal(e.getMessage() + ": " + e);
			e.printStackTrace();
			System.exit(1);
		}

	}

}
