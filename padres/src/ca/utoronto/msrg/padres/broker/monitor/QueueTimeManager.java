// =============================================================================
// This file is part of The PADRES Project.
//
// For more information, see http://www.msrg.utoronto.ca
//
// Copyright (c) 2003 Middleware Systems Research Group, University of Toronto
// =============================================================================
// $Id$
// =============================================================================
/*
 * Created on Nov 11, 2003
 */
package ca.utoronto.msrg.padres.broker.monitor;

/**
 * @author Alex Cheung
 *
 * Manages the enqueue and dequeue time of messages
 * This class fixes the synchronization problem of getting the queuing time 
 * of a message before the message's enqueue time is recorded
 */

import java.util.HashMap;

public class QueueTimeManager {

	private HashMap<String, Long> timeOfEnqueue;

	private HashMap<String, Long> timeOfDequeue;

	// For synchronization purposes only
	private final Object enqueueMutex = new Object();

	private final Object dequeueMutex = new Object();

	public QueueTimeManager() {
		timeOfEnqueue = new HashMap<String, Long>();
		timeOfDequeue = new HashMap<String, Long>();
	}

	public void setEnqueueTime(String messageID) {
		synchronized (enqueueMutex) {
			timeOfEnqueue.put(messageID, System.currentTimeMillis());
			enqueueMutex.notify();
		}
	}

	public void setDequeueTime(String messageID) {
		synchronized (dequeueMutex) {
			timeOfDequeue.put(messageID, System.currentTimeMillis());
			dequeueMutex.notify();
		}
	}

	public long removeEnqueueTime(String messageID) {
		synchronized (enqueueMutex) {
			int count = 0;
			while (!timeOfEnqueue.containsKey(messageID) && count < 5) {
				try {
					count++;
					enqueueMutex.wait(5);
				} catch (Exception e) {
				}
			}
		}
		
		if(timeOfEnqueue.containsKey(messageID))
			return timeOfEnqueue.remove(messageID);
		else
			return -1;
	}

	public long removeDequeueTime(String messageID) {
		synchronized (dequeueMutex) {
			int count = 0;
			while (!timeOfDequeue.containsKey(messageID) && count < 5) {
				try {
					count++;
					dequeueMutex.wait(5);
				} catch (Exception e) {
				}
			}
		}
		
		if(timeOfDequeue.containsKey(messageID))
			return timeOfDequeue.remove(messageID);
		else
			return -1;
	}
}
