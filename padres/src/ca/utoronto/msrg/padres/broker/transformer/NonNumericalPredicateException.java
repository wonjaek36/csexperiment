package ca.utoronto.msrg.padres.broker.transformer;

import ca.utoronto.msrg.padres.broker.brokercore.BrokerCoreException;

public class NonNumericalPredicateException extends BrokerCoreException {

	private static final long serialVersionUID = -8620809112975502032L;

	public NonNumericalPredicateException(String key){
		super("Non-numerical predicate " + key + " is being compared");
	}
}
