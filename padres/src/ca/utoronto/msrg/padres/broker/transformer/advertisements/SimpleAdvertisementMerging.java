package ca.utoronto.msrg.padres.broker.transformer.advertisements;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ca.utoronto.msrg.padres.broker.transformer.MergingException;
import ca.utoronto.msrg.padres.broker.transformer.NonNumericalPredicateException;
import ca.utoronto.msrg.padres.broker.transformer.PredDiffResults;
import ca.utoronto.msrg.padres.broker.transformer.Transformer;
import ca.utoronto.msrg.padres.common.message.AdvertisementMessage;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.MessageType;

public class SimpleAdvertisementMerging implements Transformer {

	private double threshold;
	private int missing;
	private List<AdvertisementMessage> adsList;
	
	public SimpleAdvertisementMerging(double threshold, int missing){
		this.threshold = threshold;
		this.missing = missing;
		this.adsList = new LinkedList<AdvertisementMessage>();
	}
	
	@Override
	public void transform(Set<Message> messageSet) {
		List<AdvertisementMessage> toAdd = new LinkedList<AdvertisementMessage>();
		for(Message m : messageSet){
			boolean found = false;
			if(m.getType().equals(MessageType.ADVERTISEMENT)){
				AdvertisementMessage adMessage = (AdvertisementMessage)m;
				for(AdvertisementMessage target : adsList){
					try {
						PredDiffResults results = target.getAdvertisement().compareDifference(adMessage.getAdvertisement());
						if(results.isSameClass() && results.getScore() <= threshold && results.getDifferentPredicates() <= missing){
							adMessage.getAdvertisement().mergeFrom(target.getAdvertisement());
							target.setAdvertisement(adMessage.getAdvertisement());
							found = true;
							break;
						}
					} catch (NonNumericalPredicateException e) {
						continue;
					} catch (MergingException e) {
						continue;
					}			
				}
				
				if(!found){
					toAdd.add(adMessage);
				}
			}
		}
		
		adsList.addAll(toAdd);
	}

	@Override
	public String getId() {
		return "Simple-Ads";
	}

}
