package ca.utoronto.msrg.padres.broker.transformer;

import ca.utoronto.msrg.padres.broker.brokercore.BrokerCoreException;

public class MergingException extends BrokerCoreException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4891745593091576187L;

	public MergingException(String error){
		super(error);
	}
}
