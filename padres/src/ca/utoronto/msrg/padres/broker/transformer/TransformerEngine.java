package ca.utoronto.msrg.padres.broker.transformer;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ca.utoronto.msrg.padres.common.message.Message;

public class TransformerEngine {
	private List<Transformer> transformerPipeline;
	
	public TransformerEngine(){
		transformerPipeline = new LinkedList<Transformer>();
	}
	
	public void registerTransformer(Transformer transformer){
		transformerPipeline.add(transformer);
	}
	
	public void unregisterTransformer(String id){
		List<Transformer> toRemove = new LinkedList<Transformer>();
		
		for(Transformer t : transformerPipeline){
			if(t.getId().equals(id))
				toRemove.add(t);
		}
		
		transformerPipeline.remove(toRemove);
	}
	
	public void transform(Set<Message> messageSet){
		Set<Message> outputSet = messageSet;
		
		for(Transformer t : transformerPipeline){
			t.transform(outputSet);
		}
	}
}
