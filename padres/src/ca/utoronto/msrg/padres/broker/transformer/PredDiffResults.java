package ca.utoronto.msrg.padres.broker.transformer;

/**
 * Container class for holding ads/subs comparisons.
 * @author kzhang
 *
 */
public class PredDiffResults {
	private double score;
	private int differentPredicates;
	private boolean isSameClass;
	
	public PredDiffResults(double score, int differentPredicates){
		this.score = score;
		this.differentPredicates = differentPredicates;
		this.isSameClass = true;
	}

	public PredDiffResults(int score, int differentPredicates, boolean isSameClass) {
		this(score, differentPredicates);
		this.isSameClass = isSameClass;
	}

	public double getScore() {
		return score;
	}

	public int getDifferentPredicates() {
		return differentPredicates;
	}

	public boolean isSameClass() {
		return isSameClass;
	}

}
