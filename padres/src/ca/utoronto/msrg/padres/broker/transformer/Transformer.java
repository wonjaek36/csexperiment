package ca.utoronto.msrg.padres.broker.transformer;

import java.util.Set;

import ca.utoronto.msrg.padres.common.message.Message;

public interface Transformer {
	public void transform(Set<Message> messageSet);
	public String getId();
}
