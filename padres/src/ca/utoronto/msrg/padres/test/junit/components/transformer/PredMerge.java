package ca.utoronto.msrg.padres.test.junit.components.transformer;

import ca.utoronto.msrg.padres.broker.transformer.MergingException;
import ca.utoronto.msrg.padres.broker.transformer.NonNumericalPredicateException;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import junit.framework.TestCase;

public class PredMerge  extends TestCase {
	
	private Advertisement adv1, adv2;
	
	public void testDifferentClass(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50],[volume,>,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test2'],[price,<,30],[volume,<,40]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
		} catch (MergingException e) {
			return;
		} catch (NonNumericalPredicateException e) {}
		
		fail();
	}
	
	public void testNonNumerical(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50],[stock,eq,true]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30],[stock,eq,true]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
		} catch (MergingException e) {} catch (NonNumericalPredicateException e) {
			return;
		}
		
		fail();
	}
	
	public void testSanity(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50],[volume,=,4]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,30],[high,<>,5]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),4);
			assertTrue(adv1.getPredicateMap().get("class").getOp().equals("eq"));
			assertTrue(adv1.getPredicateMap().get("class").getValue().equals("test"));
			assertTrue(adv1.getPredicateMap().get("class").toString().equals("eq test"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
	}
	
	public void testSimplePredicateUp(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("> 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("> 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>=,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals(">= 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>=,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("> 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals(">= 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals(">= 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>=,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals(">= 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
	}
	
	
	public void testSimplePredicateDown(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,30]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("< 50"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("< 50"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<=,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("<= 50"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<=,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("< 50"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
	}
	
	public void testCornerCases(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("<= 50"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,30]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals(">= 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<>,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,=,30]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("isPresent 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
		
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<>,30]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<>,50]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			adv1.mergeFrom(adv2);
			assertEquals(adv1.getPredicateMap().size(),2);
			assertTrue(adv1.getPredicateMap().get("price").toString().equals("isPresent 30"));
		} catch (NonNumericalPredicateException e) {
			fail();
		} catch (MergingException e) {
			fail();
		}
	}
}
