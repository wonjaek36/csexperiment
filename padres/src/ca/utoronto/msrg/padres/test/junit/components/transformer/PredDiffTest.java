package ca.utoronto.msrg.padres.test.junit.components.transformer;

import junit.framework.TestCase;
import ca.utoronto.msrg.padres.broker.transformer.NonNumericalPredicateException;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;

public class PredDiffTest extends TestCase {

	private Advertisement adv1, adv2;
		
	public void testDifferentClass(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50],[volume,>,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test2'],[price,<,30],[volume,<,40]");
		} catch (ParseException e) {
			fail();
		}
		
		try {
			assertFalse(adv1.compareDifference(adv2).isSameClass());
		} catch (NonNumericalPredicateException e) {
			fail();
		}
	}
	
	public void testSimplePredicateGap(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,30]");
		} catch (ParseException e1) {
			fail();
		}
		
		try {
			assertEquals(adv1.compareDifference(adv2).getScore(), 20.0);
		} catch (NonNumericalPredicateException e) {
			fail();
		}
	}
	
	public void testSimplePredicateCoverDifferent(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,<,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
		} catch (ParseException e1) {
			fail();
		}
		
		try {
			assertEquals(adv1.compareDifference(adv2).getScore(), 0.0);
		} catch (NonNumericalPredicateException e) {
			fail();
		}
	}
	
	public void testSimplePredicateCoverSame(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30]");
		} catch (ParseException e1) {
			fail();
		}
		
		try {
			assertEquals(adv1.compareDifference(adv2).getScore(), 0.0);
		} catch (NonNumericalPredicateException e) {
			fail();
		}
	}
	
	public void testNonNumericalPredicate() {
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50],[stock,eq,true]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,30],[stock,eq,true]");
		} catch (ParseException e1) {
			fail();
		}

		try {
			adv1.compareDifference(adv2).getScore();
		} catch (NonNumericalPredicateException e) {
			return;
		}
		
		fail();
	}
	
	public void testMultiplePredicate(){
		try {
			adv1 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>,50.5],[volume,<=,23.1],[time,=,9.0],[value,<>,-23.0],[high,<,55.0],[low,=,-3.0],[medium,=,66.0],[stock,isPresent,-1.0]");
			adv2 = MessageFactory.createAdvertisementFromString("[class,eq,'test'],[price,>=,30.8],[volume,>,-5.0],[time,>,10.0],[value,=,-23.0],[high,>,60.0],[low,=,4.0],[medium,>,77.0],[stock,>=,4.0]");
		} catch (ParseException e1) {
			System.out.println(e1);
			fail();
		}

		try {
			assertEquals(adv1.compareDifference(adv2).getScore(),24.0);
		} catch (NonNumericalPredicateException e) {
			System.out.println(e);
			fail();
		}
	}
}
