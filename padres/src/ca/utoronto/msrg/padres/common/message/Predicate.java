// =============================================================================
// This file is part of The PADRES Project.
//
// For more information, see http://www.msrg.utoronto.ca
//
// Copyright (c) 2003 Middleware Systems Research Group, University of Toronto
// =============================================================================
// $Id$
// =============================================================================
/*
 * Created on Jul 7, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package ca.utoronto.msrg.padres.common.message;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import ca.utoronto.msrg.padres.broker.transformer.NonNumericalPredicateException;

/**
 * @author efidler
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Predicate implements Serializable {
	public static final long serialVersionUID = 1;

	// Subscriptions consist of HashMaps of Predictes, so that attributes are
	// the hash keys
	private String op;

	private Serializable value;

	// Each value type is allowed certain operators. These are chosen to match
	// Jess function calls

	// Numbers (Integers, Shorts, Longs, Floats, or Doubles)
	// Allowed: less than ("<"), less than or equal to ("<="), equal to ("="),
	// greater than or equal to (">="),
	// greater than (">"), not equal to ("<>")
	static private String[] validNumberOperatorsArray = new String[] { "<", "<=", "=", ">=", ">",
			"<>", "isPresent" };

	static private List<String> validNumberOperators = Arrays.asList(validNumberOperatorsArray);

	// Strings (all comparisons are lexographic)
	// Allowed: less than ("str-lt"), less than or equal to ("str-le"), equal to
	// ("eq"), greater than or equal to ("str-ge"),
	// greater than ("str-gt"), not equal to ("neq"), contains ("str-contains"),
	// is within ("str-index")
	static private String[] validStringOperatorsArray = new String[] { "str-lt", "str-le", "eq",
			"str-ge", "str-gt", "neq", "str-contains", "str-prefix", "str-postfix", "isPresent" };

	static private List<String> validStringOperators = Arrays.asList(validStringOperatorsArray);

	static private String[] validDateOperatorsArray = new String[] { "before", "after" };

	static private List<String> validDateOperators = Arrays.asList(validDateOperatorsArray);

	/**
	 * Default constructor
	 */
	public Predicate() {
	}

	/**
	 * Constructor with initializers
	 * 
	 * @param o
	 *            refer to valid operators above to see what is allowed
	 * @param val
	 */
	public Predicate(String o, Serializable val) {
		op = o;
		value = val;
	}

	/**
	 * 
	 */
	public String toString() {
		return op + " " + value.toString();
	}

	/**
	 * @return
	 */
	public String getOp() {
		return op;
	}

	/**
	 * @return
	 */
	public Serializable getValue() {
		return value;
	}

	/**
	 * @param object
	 */
	public void setValue(Serializable object) {
		value = object;
	}

	/**
	 * @param string
	 */
	public void setOp(String string) {
		op = string;
	}

	/**
	 * 
	 * @return true if operator is allowed for that value, false otherwise
	 */
	public boolean isValid() {
		Class<? extends Serializable> valueClass = value.getClass();
		if (valueClass == Integer.class || valueClass == Double.class || valueClass == Float.class
				|| valueClass == Long.class || valueClass == Short.class) {
			return validNumberOperators.contains(op);
		} else if (valueClass == String.class) {
			return validStringOperators.contains(op);
		} else if (valueClass.equals(Date.class)) {
			return validDateOperators.contains(op);
		}
		return false;
	}

	public Predicate duplicate() {
		Predicate newPredicate = new Predicate();
		newPredicate.setOp(this.getOp());
		newPredicate.setValue((Serializable) this.getValue());
		return newPredicate;
	}

	public static boolean isNumericOperator(String op) {
		return validNumberOperators.contains(op);
	}

	public static boolean isStringOperator(String op) {
		return validStringOperators.contains(op);
	}

	/**
	 * <code>
	public boolean equals(Map m1, Map m2) {
		if (m1.size() != m2.size()) {
			return false;
		} else {
			for (Iterator i = (m1.keySet()).iterator(); i.hasNext();) {
				String attribute = (String) i.next();
				if (!m2.containsKey(attribute)) {
					return false;
				} else {
					if (m1.get(attribute).getClass().equals(Predicate.class)) {
						// deal with predicateMap for adv and sub
						Predicate p1 = (Predicate) m1.get(attribute);
						Predicate p2 = (Predicate) m2.get(attribute);
						if (!equals(p1, p2)) 
							return false;
						}
					} else {
						// deal with pairMap for pub
						Object v1 = (Object) m1.get(attribute);
						Object v2 = (Object) m2.get(attribute);
						if (!v1.equals(v2)) {
							return false;
						}
					}
				}
			}
			return true;
		}
	}
	 * </code>
	 */

	/**
	 * @param p1
	 *            p2
	 * @return true if their op and value are equal seperately false if not
	 */

	public boolean equals(Predicate p1, Predicate p2) {
		if (p1.getOp().equals(p2.getOp()) && (p1.getValue().equals(p2.getValue()))) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method returns the gap between numerical predicates in the space they cover when merged.
	 * For example, ">5" and ">6" has no gap. "<3" and ">5" has a gap of 2 since merging both would create a continuous range.
	 * @param target
	 * @return
	 */
	public double getDifference(Predicate target){
		if(Predicate.isNumericOperator(this.getOp())){
			double gap = 0.0;
			Serializable v1 = this.getValue(), v2 = target.getValue();
			String o1, o2;
			
			Class<? extends Serializable> valueClass = value.getClass();
			//TODO: use reflection
			if (valueClass == Integer.class) {
				gap = (Integer)v1 - (Integer)v2;
			} else if (valueClass == Double.class) {
				gap =  (Double)v1 - (Double)v2;
			} else if (valueClass == Float.class) {
				gap = (Float)v1 - (Float)v2;
			} else if (valueClass == Long.class) {
				gap = (Long)v1 - (Long)v2;
			} else if (valueClass == Short.class) {
				gap = (Short)v1 - (Short)v2;
			}
							
			if(gap > 0){
				o1 = this.getOp();
				o2 = target.getOp();
			} else if(gap < 0) {
				o1 = target.getOp();
				o2 = this.getOp();
				gap = -gap;
			} else { // gap == 0
				return gap;
			}
			
			if(o1.equals("<") || o1.equals("<=") || o2.equals(">") || o2.equals(">=") || o1.equals("<>") || o1.equals("isPresent") || o2.equals("<>") || o2.equals("isPresent"))
				gap = 0.0;
			
			return gap;
		}
		
		return -1.0;
	}

	public void merge(String key, Predicate target) throws NonNumericalPredicateException {
		if(Predicate.isNumericOperator(this.getOp())){
			Serializable[] v = new Serializable[2];
			v[0] = this.getValue();
			v[1] = target.getValue();
			
			String[] o = new String[2]; 
			o[0] =	this.getOp();
			o[1] = target.getOp();
			
			boolean down = false, up = false, equal = false;
			
			for(int i = 0; i < 2; i++){
				if(o[i].equals("isPresent")){
					down = true;
					up = true;
					equal = true;
				} else if(o[i].equals("<>")){
					down = true;
					up = true;
				}	
				else if(o[i].equals("<") || o[i].equals("<=")){
					down = true;
					
					if(o[i].equals("<=")){
						equal = true;
					}
				} else if(o[i].equals(">") || o[i].equals(">=")){
					up = true;
					
					if(o[i].equals(">=")){
						equal = true;
					}
				} else if(o[i].equals("="))
					equal = true;
			}
			
			if(o[0].equals("<>") && o[1].equals("<>")){ // corner-case <>
				if(!v[0].equals(v[1])){
					this.setOp("isPresent");
				}
				return;
			}
			
			if((down && up)){
				if(v[0].equals(v[1])){
					if(!equal){ // corner case, same value one >, one <, then result is <>
						this.setOp("<>");
						return;
					}
				}
				this.setOp("isPresent");
				return;
			}
			
			if(v[0].equals(v[1])){ // corner case example: same value, one is > one is >=, merging should be >=
				if(up){
					if(equal)
						this.setOp(">=");
					else
						this.setOp(">");
				} else if(down) {
					if(equal)
						this.setOp("<=");
					else
						this.setOp("<");
				} // if it's not both, then it's =, which means no change
				
				return;
			}
			
			
			if(down){
				if(this.isGreater(v[1], v[0])){
					this.setValue(v[1]);
					if(o[1].equals("="))
						this.setOp("<=");
					else
						this.setOp(o[1]);
				} else {
					this.setValue(v[0]);
					if(o[0].equals("="))
						this.setOp("<=");
					else
						this.setOp(o[0]);
				}
			} else {
				if(this.isGreater(v[0], v[1])){
					this.setValue(v[1]);
					if(o[1].equals("="))
						this.setOp(">=");
					else
						this.setOp(o[1]);
				} else {
					this.setValue(v[0]);
					if(o[0].equals("="))
						this.setOp(">=");
					else
						this.setOp(o[0]);
				}
			}
			
		} else {
			throw new NonNumericalPredicateException(key);
		}		
	}
	
	private boolean isGreater (Serializable s1, Serializable s2) {		
		Class<? extends Serializable> valueClass = value.getClass();
		//TODO: use reflection
		if (valueClass == Integer.class) {
			if((Integer)s1 > (Integer)s2)
				return true;
			else
				return false;
		} else if (valueClass == Double.class) {
			if((Double)s1 > (Double)s2)
				return true;
			else
				return false;
		} else if (valueClass == Float.class) {
			if((Float)s1 > (Float)s2)
				return true;
			else
				return false;
		} else if (valueClass == Long.class) {
			if((Long)s1 > (Long)s2)
				return true;
			else
				return false;
		} else if (valueClass == Short.class) {
			if((Short)s1 > (Short)s2)
				return true;
			else
				return false;
		} else
			return false;
	}
}
