package ca.utoronto.msrg.padres.tools.fabre;

import java.util.LinkedList;

public class SimpleWorkflow {
	private LinkedList<SimpleWorkflowTask> tasks = new LinkedList<SimpleWorkflowTask>();
	
	public void addTask(SimpleWorkflowTask task){
		tasks.addFirst(task);
	}
	
	public LinkedList<SimpleWorkflowTask> getTasks() {
		return tasks;
	}
	
}
