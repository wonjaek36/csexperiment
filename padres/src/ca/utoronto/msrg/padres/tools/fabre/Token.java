package ca.utoronto.msrg.padres.tools.fabre;

public class Token {
	public String workflowid;
	public String taskid;
	public long starttime;
	public long finishtime;
	public long arrivaltime;
	public String serviceName;
	public String instanceid;
	public String triggeredby;
	public String triggers;
}
