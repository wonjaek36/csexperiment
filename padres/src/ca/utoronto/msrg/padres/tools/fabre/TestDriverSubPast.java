package ca.utoronto.msrg.padres.tools.fabre;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentHashMap;

import ca.utoronto.msrg.padres.client.Client;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.Predicate;
import ca.utoronto.msrg.padres.common.message.Publication;
import ca.utoronto.msrg.padres.common.message.Subscription;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;

public class TestDriverSubPast extends Client{

	private int numServices = 0;	
	private ConcurrentHashMap<String, SimpleService> servicePool
		= new ConcurrentHashMap<String, SimpleService>();
	
	/*
	public TestDriverSubPast(int numServices, String clientID) throws ClientException{
		super(clientID);
		this.numServices = numServices;
		for (int i = 0; i < numServices; i++) {
			servicePool.put("s" + i, new SimpleService("s" + i, 1)); 
		}
	}
	*/
	public TestDriverSubPast(String clientID) throws ClientException {
		super(clientID);
		// TODO Auto-generated constructor stub
	}

	public void processMessage(Message msg) {
		super.processMessage(msg);
		System.out.println(msg.getMessageTime());
		System.out.println("Received pub: " + msg);
	}
	
	public void executeTask(String serviceName, SimpleWorkflowTask task) {
		SimpleService s = servicePool.get(serviceName);
		try {
			s.triggerService(task);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String [ ] args) throws ClientException {
		try {
			//TestDriverSubPast driver = new TestDriverSubPast(2, "TestDriver");
			TestDriverSubPast driver = new TestDriverSubPast("TestSubPast2");
			String brokerURI = args[0]; 
			driver.connect(brokerURI);
			String ipAddress = InetAddress.getLocalHost().getHostAddress();
			for(String arg :args)
				if(arg.startsWith("rmi") || arg.startsWith("socket")){
					brokerURI = arg.replace("localhost", ipAddress);
					break;
				}
			System.out.println("brokerURI: " + brokerURI);
			/*
			System.out.println("Setting up database");
			
			Advertisement dbAdv = new Advertisement();
			dbAdv.addPredicate("class", new Predicate("eq","DB_CONTROL"));
			dbAdv.addPredicate("command", new Predicate("isPresent","any"));
			dbAdv.addPredicate("content_spec", new Predicate("isPresent","any"));
			dbAdv.addPredicate("database_id", new Predicate("isPresent","any"));
			driver.advertise(dbAdv, brokerURI);
			Thread.sleep(10000);
			
			Publication dbPub = new Publication();
			dbPub.addPair("class", "DB_CONTROL");
			dbPub.addPair("command", "STORE");
			dbPub.addPair("content_spec", "[class,eq,rohEvent],[be,isPresent,any],[type,isPresent,any]");
			dbPub.addPair("database_id", brokerURI + "-DB");
			driver.publish(dbPub, brokerURI);
			Thread.sleep(10000);
			
			System.out.println("Client publications");
			
			Advertisement clientAdv = new Advertisement();
			
			clientAdv.addPredicate("class", new Predicate("eq", "rohEvent"));
			clientAdv.addPredicate("be", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("type", new Predicate("isPresent", "any"));
			
			driver.advertise(clientAdv, brokerURI);
			
			Publication clientPub = new Publication();
			
			clientPub.addPair("class", "ServiceInvocation");
			
			System.out.println("WAITING 5 SECONDS BEFORE PUBLISHING FROM Test Driver:");
			
			Thread.sleep(2000);
			
			driver.publish(clientPub, brokerURI);
			
			System.out.println("WAIT 5 SECONDS BEFORE SUBSCRIBING Test Driver:");
			
			Thread.sleep(2000);
			*/
			Subscription clientSub = new Subscription();
			clientSub.addPredicate("class", new Predicate("eq", "historic"));
			clientSub.addPredicate("subclass", new Predicate("eq", "ServiceInvocation"));
			clientSub.addPredicate("_query_id", new Predicate("eq", "any"));			
			clientSub.addPredicate("type", new Predicate("isPresent", "any"));
			clientSub.addPredicate("workflowid", new Predicate("isPresent", "any"));
			clientSub.addPredicate("taskid", new Predicate("isPresent", "any"));
			clientSub.addPredicate("instanceid", new Predicate("isPresent", "any"));
			clientSub.addPredicate("triggeredby", new Predicate("isPresent", "any"));
			clientSub.addPredicate("triggers", new Predicate("isPresent", "any"));
			
			
			
			//driver.subscribe(clientSub, brokerURI);
			/*
			Subscription clientSub2 = new Subscription();
			clientSub2.addPredicate("class", new Predicate("eq", "rohEvent"));
			driver.subscribe(clientSub2, brokerURI);
			driver.publish(clientPub, brokerURI);
			*/
			
			
			Subscription clientSub2 = MessageFactory.createSubscriptionFromString("[class,eq,'historic']");
					/*
					"[subclass,eq,'ServiceInvocation'],[_query_id,eq,'q1']," +
					"[servicename,isPresent,'any'],[arrivaltime,>,0]" +
					"[finishtime,>,0],[starttime,>,0],[workflowid,isPresent,'any']" +
					"[taskid,isPresent,'any']," +
					"[instanceid,isPresent,'any'],[triggeredby,isPresent,'any']" +
					"[triggers,isPresent,'any']");*/
					
			/*
			Subscription clientSub2 = new Subscription("[class,eq,'historic']," +
					"[subclass,eq,'ServiceInvocation'],[_query_id,isPresent,'any']");
			*/
			/*
			clientSub2.addPredicate("class", new Predicate("eq", "historic"));
			clientSub2.addPredicate("subclass", new Predicate("eq", "ServiceInvocation"));
			clientSub2.addPredicate("_query_id", new Predicate("eq", "q1"));
			clientSub2.addPredicate("arrivaltime", new Predicate (">", 10));
			clientSub2.addPredicate("servicename", new Predicate ("eq", "s1"));
			*/
			driver.subscribe(clientSub);
			
			System.out.println("Done!");
			
			
			/*
			String subStr2 = "[class,eq,'stock']";
			System.out.println("Sending sub: " + subStr2);
			driver.subscribe(subStr2, brokerURI);
			Thread.sleep(1000);
			driver.publish(pubStr2, brokerURI);
			System.out.println("Sending pub: " + pubStr2);
			driver.publish(pubStr2, brokerURI);
			System.out.println("Sending pub: " + pubStr2);
			*/
			/*
			SimpleWorkflow wf1 = new SimpleWorkflow();
			SimpleWorkflow wf2 = new SimpleWorkflow();
			SimpleWorkflow wf3 = new SimpleWorkflow();
			SimpleWorkflowTask wf3t1 = new SimpleWorkflowTask("wf3", "t1");
			SimpleWorkflowTask wf1t1 = new SimpleWorkflowTask("wf1", "t1");
			SimpleWorkflowTask wf1t2 = new SimpleWorkflowTask("wf1", "t2");
			SimpleWorkflowTask wf2t1 = new SimpleWorkflowTask("wf2", "t1");
			SimpleWorkflowTask wf2t2 = new SimpleWorkflowTask("wf2", "t2");
			
			wf3t1.setExecutionTime(10000);
			wf1t1.setExecutionTime(10000);
			wf1t1.setExecutionTime(10000);
			wf1t2.setExecutionTime(5000);
			wf2t1.setExecutionTime(10000);
			wf2t2.setExecutionTime(5000);
			driver.executeTask("s1", wf3t1);
			Thread.sleep(1000);
			driver.executeTask("s1", wf1t1);
			Thread.sleep(1000);
			Token receivedToken = driver.executeTask("s1", wf2t1);
			
			if (!(receivedToken.taskid == "t2" 
					&& receivedToken.workflowid == "wf1")) {
				System.out.println("Scheduling is not correct");
				System.exit(0);
			}
			receivedToken = driver.executeTask("s2", wf1t2);
			if (!(receivedToken.taskid == "t2" 
				&& receivedToken.workflowid == "wf1")) {
				System.out.println("Scheduling is not correct");
				System.exit(0);
			}
			receivedToken = driver.executeTask("s2", wf2t2);
			if (!(receivedToken.taskid == "t2" 
				&& receivedToken.workflowid == "wf2")) {
				System.out.println("Scheduling is not correct");
				System.exit(0);
			}
			*/
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
