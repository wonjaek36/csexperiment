package ca.utoronto.msrg.padres.tools.fabre;

public class SimpleWorkflowTask {

	private long executionTime = 0;
	private long arrivalTime = 0;
	private long startTime = 0;
	private long finishTime = 0;
	private String type = "";
	private String taskID = "";
	private String workflowID = "";
	private String instanceID = "";
	private String triggers = "";
	private String triggeredby = "";
	private String serviceName = "";
		
	
	public SimpleWorkflowTask() {
	}
	
	public void setTaskID(String taskID) {
		this.taskID = taskID;
	}

	public long getExecutionTime() {
		return executionTime;
	}
	
	public void setArrivalTime(long i) {
		// TODO Auto-generated method stub
		this.arrivalTime = i;
		
	}
	
	public void setStartTime(long i) {
		// TODO Auto-generated method stub
		this.startTime = i;
		
	}

	public void setFinishTime(long i) {
		this.finishTime = i;
	}
	
	public void setExecutionTime(int i) {
		// TODO Auto-generated method stub
		this.executionTime = i;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return this.type;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
		
	}

	public void setInstanceID(String instanceID) {
		this.instanceID = instanceID;
	}
	
	public void setTriggeredBy(String triggeredby) {
		this.triggeredby = triggeredby;
	}
	
	public void setTriggers(String triggers) {
		this.triggers = triggers;
	}
	
	public void setWorkflowID(String workflowID) {
		this.workflowID = workflowID;
	}
	
	public long getArrivalTime() {
		return arrivalTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getFinishTime() {
		return this.finishTime;
	}

	public String getWorkflowID() {
		return this.workflowID;
	}

	public String getTaskID() {
		return this.taskID;
	}
	
	public String getInstanceID() {
		return this.instanceID;
	}
	
	public String getTriggeredBy() {
		return this.triggeredby;
	}
	
	public String getTriggers() {
		return this.triggers;
	}
	
	public String getServiceName(){
		return this.serviceName;
	}

}
