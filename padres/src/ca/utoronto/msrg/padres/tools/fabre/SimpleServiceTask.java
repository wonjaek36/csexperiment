package ca.utoronto.msrg.padres.tools.fabre;

import java.util.LinkedList;

import ca.utoronto.msrg.padres.common.message.parser.ParseException;

public class SimpleServiceTask extends Thread{

	private long sleepTime;
	 
    private String threadName; // name of thread
 
    private LinkedList<SimpleWorkflowTask> inSystem;
    
    private SimpleService simpleService;
    
    public SimpleServiceTask(String threadName, long sleepTime, 
    		LinkedList<SimpleWorkflowTask> inSystem,
    		SimpleService simpleService) {
    	this.sleepTime = sleepTime;
    	this.threadName = threadName;
    	this.inSystem = inSystem;
    	this.simpleService = simpleService;
    }
    
    public void run()
    {
        try
        {
            System.out.printf("%s going to sleep for %d milliseconds.\n",
                    threadName, sleepTime);
 
            Thread.sleep(sleepTime); // put thread to sleep
            
            simpleService.removeFromInSystem();
        } 
        catch (InterruptedException exception)
        {
            exception.printStackTrace();
        }
        catch (ParseException pexception) {
        	pexception.printStackTrace();
        }
        System.out.printf("%s done sleeping\n", threadName);
    }
}
