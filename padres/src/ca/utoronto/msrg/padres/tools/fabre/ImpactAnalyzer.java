package ca.utoronto.msrg.padres.tools.fabre;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ca.utoronto.msrg.padres.client.Client;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.message.Message;
import ca.utoronto.msrg.padres.common.message.Predicate;
import ca.utoronto.msrg.padres.common.message.PublicationMessage;
import ca.utoronto.msrg.padres.common.message.Subscription;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;

public class ImpactAnalyzer extends Client{
	
	private LinkedList<ServiceInvocation> inQueue 
		= new LinkedList<ServiceInvocation>();
	private boolean termination = false;
	
	/**
	 * Add the element to the queue in the required order. It uses a binary search to locate the
	 * correct position. Notifies blocked threads.
	 * 
	 * @param obj
	 *            object need to be added
	 */
	private synchronized void add(ServiceInvocation s) {
		inQueue.add(s);
		notify();
	}
	/**
	 * Remove an element from the queue, blocking until an element is available.
	 * 
	 * @return The first element in the queue.
	 */
	private synchronized ServiceInvocation blockingRemove() {
		// retry until an element is obtained
		while (true) {
			if (inQueue.size() > 0) {
				return inQueue.remove(0);
			} else {
				// block until notified that an element is in the queue
				try {
					wait();
				} catch (InterruptedException ie) {
				}
			}
		}
	}
	
	private ServiceInvocation listen() {
		ServiceInvocation s = null;
		while(true) {
			s =  blockingRemove();
			break;
		}
		return s;
	}
	
	public ImpactAnalyzer() throws ClientException {
		super();		
	}

	public ImpactAnalyzer(String clientID) throws ClientException {
		super(clientID);
	}

	public class TopTwoTime {
		public long earliestTerminationTime = 0;
		public long secondEarliestTerminationTime = 0;
		public long latestArrivalTime = 0;
	}
	
	
	
	public void setTermination(boolean termination) {
		this.termination = termination;
	}
	
	public boolean getTermination() {
		return this.termination;
	}
	/*
	 * Overwrite the processMessage of the parent class.
	 * This is called when a message is arrived from a broker.
	 */
	public void processMessage(Message msg) {
		// Create a ServiceInvocation object from the 
		// publication message.
		ServiceInvocation s = new ServiceInvocation();
		s.setArrivalTime(1);
		s.setServiceFinishTime(11);
		s.setWFID("wf1");
		this.add(s);
		//inQueue.add(s);
	}
	
	public LinkedList<ServiceInvocation> getOverlap (String serviceName, ServiceInvocation s, int capacity) {
		LinkedList<ServiceInvocation> overlapped = null;
		try {
			overlapped = new LinkedList<ServiceInvocation>();
			Subscription clientSub = MessageFactory.createSubscriptionFromString("[class,eq,'historic'],[subclass,eq,'ServiceInvocation'],[_query_id,eq,'q1'],[servicename,eq,'s1'],[arrivaltime,<,0]");
			/*
			Subscription clientSub = new Subscription();
			
			clientSub.addPredicate("class", new Predicate("eq", "historic"));
			clientSub.addPredicate("subclass", new Predicate("eq", "ServiceInvocation"));
			clientSub.addPredicate("_query_id", new Predicate("eq", "q1"));
			clientSub.addPredicate("arrivaltime", new Predicate (">", s.getServiceFinishTime()));
			clientSub.addPredicate("servicename", new Predicate ("eq", serviceName));
			*/
		
			this.subscribe(clientSub);
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (ParseException pe) {
			pe.printStackTrace();
		}
		
		// SELECT * from ServiceInvocations 
		// where s.getArrivalTime <= finishtime <= s.getServiceFinishTime 
		// reverse ordered by finishtime
		// OR
		// where s.getArrivalTime <= starttime <= s.getServiceFinishTime 
		// ordered by starttime
		//ServiceInvocation si = listen();
		//overlapped.add(si);
		return overlapped;
	}
	
	public ServiceInvocation getInterfered(String serviceName, long latestArrivalTime, long earliestFinishTime) {
		/*
		if (latestArrivalTime < earliestFinishTime) {
			System.out.println("Temporal reasonining error: latestArrivalTime " +
					"cannot happen before earliestFinishTime");
		}
		*/
		// SELECT * from ServiceInvocations 
		// where s.getArrivalTime <= finishtime <= s.getServiceFinishTime 
		// reverse ordered by finishtime
		// OR
		// where s.getArrivalTime <= starttime <= s.getServiceFinishTime 
		// ordered by starttime
		//Subscription clientSub = new Subscription();
		String subString = "[class,eq,'historic'],[subclass,eq,'ServiceInvocation'],[_query_id,eq,'q2'],[servicename,eq,'s1']";//,[arrivaltime,<,"+ earliestFinishTime +"]";
		//Subscription clientSub = new Subscription("[class,eq,'historic'],[subclass,eq,'ServiceInvocation'],[_query_id,eq,'q2'],[servicename,eq,'s1'],[arrivaltime,<,0]");
		Subscription clientSub;
		try {
			clientSub = MessageFactory.createSubscriptionFromString(subString);
			/*
			clientSub.addPredicate("class", new Predicate("eq", "historic"));
			clientSub.addPredicate("subclass", new Predicate("eq", "ServiceInvocation"));
			clientSub.addPredicate("_query_id", new Predicate("eq", "q1"));
			clientSub.addPredicate("arrivaltime", new Predicate ("<", earliestFinishTime));
			clientSub.addPredicate("servicename", new Predicate ("eq", serviceName));
			 */
			this.subscribe(clientSub);
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		ServiceInvocation interfered;
		if (earliestFinishTime < 20)
			interfered = listen();
		else {
			interfered = null;
		}
		return interfered;
	}
	
	public void addedExpectedDelay (ServiceInvocation invocation, long time) {
		long delayAmount = time - invocation.getArrivalTime();
		System.out.println("Start time of the service invocation by task1 of wf1 is delayed by " + delayAmount);
		invocation.delayServiceStartTime(delayAmount);		
		invocation.delayServiceFinishTime(delayAmount);
	}
	
	public LinkedList<ServiceInvocation> fillInitialSlidingWindow(String serviceName, int capacity, LinkedList<ServiceInvocation> affected, 
								  //LinkedList<ServiceInvocation> inSystem, 
								  ConcurrentHashMap<String, Boolean> consideredWFs) {
		TopTwoTime twoTime = new TopTwoTime();
		LinkedList<ServiceInvocation> inSystem = new LinkedList<ServiceInvocation>();
		LinkedList<ServiceInvocation> overlapped = getOverlap(serviceName, affected.getFirst(), (capacity - 1));
		if (overlapped.size() < capacity) {
			for (int i = 0; i < capacity; i++) {
				inSystem.addFirst(affected.removeFirst());
			}
		}
		else {
			inSystem = overlapped;
		}
		twoTime = this.computeTopTwoTime(inSystem);		
		//return twoTime;
		return inSystem;
	}		
			
	public LinkedList<ServiceInvocation> moveSlidingWindow(String serviceName, LinkedList<ServiceInvocation> inSystem,
			ConcurrentHashMap<String, Boolean> consideredWFs, int capacity) {
		ServiceInvocation first = inSystem.getFirst();
		TopTwoTime time = this.computeTopTwoTime(inSystem);
		LinkedList<ServiceInvocation> updatedWindow = new LinkedList<ServiceInvocation>();
		while (inSystem.size() != 0) {			
			// Fill up the window.
			// Query for the service invocations that fall in-between,
			// the latestArrivalTime and the earliestTerminationTime
			ServiceInvocation overlapped = getInterfered(serviceName, time.latestArrivalTime, time.earliestTerminationTime);
			if (overlapped == null) {
				inSystem.removeFirst();
			}
			else if(overlapped != null 
					&& !consideredWFs.contains(overlapped.getWFID())){
				inSystem.addLast(overlapped);
				// Make sure inSystem size is equal to the capacity.
				if (inSystem.size() == capacity) {
					break;
				}
			}
		}
		
		if (inSystem.size() < capacity) {
			// There was no overlapping service invocations to fill up
			// the sliding window.
			// This indicates that it is safe to move to the next round,
			// or consider the remaining set of service invocation changes.
			time = null;
		}
		else { 
			// Sliding window is filled again. 
			// It can return to the local impact analysis framework
			// and search for the inteferred service invocations in the past.
			time = this.computeTopTwoTime(inSystem);
		}
		return inSystem;
		//return time;
	}
	
	public LinkedList<ServiceInvocation> assessAddChangeImpact(String serviceName, int capacity, TopTwoTime twoTimeInstants, 
							 LinkedList<ServiceInvocation> inSystem){
		LinkedList<ServiceInvocation> newlyDelayedInThisRound = new LinkedList<ServiceInvocation>();
		while (true) {
			ServiceInvocation interfered 
				= getInterfered(serviceName, twoTimeInstants.latestArrivalTime, 
					twoTimeInstants.earliestTerminationTime);
			if (interfered == null) {
				inSystem.removeFirst();	
				// Return to the local impact analyzing framework to move the 
				// sliding window.
				// Update the earliestTermination time.
				twoTimeInstants.earliestTerminationTime = twoTimeInstants.secondEarliestTerminationTime;
				break;
			}
			long arrivaltime = interfered.getArrivalTime();
			interfered.setServiceStartTime(arrivaltime);
			// Something got interfered. Assess its impact.
			addedExpectedDelay(interfered, twoTimeInstants.earliestTerminationTime);
			// Remove the top service invocation from the sliding window.
			inSystem.removeFirst();
			// Add the interfered service invocation to the last, as its arrival
			// time is the latest one.
			inSystem.addLast(interfered);
			
			newlyDelayedInThisRound.add(interfered);
			twoTimeInstants = this.computeTopTwoTime(inSystem);
			
		}
		return newlyDelayedInThisRound;
	}
	
	private TopTwoTime computeTopTwoTime(LinkedList<ServiceInvocation> inSystem) {
		TopTwoTime time = new TopTwoTime();
		time.earliestTerminationTime = 0;
		time.latestArrivalTime = 0;
		for (ServiceInvocation s : inSystem) {
			long currentEarliestTerminationTime = s.getServiceFinishTime();
			long currentLatestArrivalTime = s.getArrivalTime();
			if (time.earliestTerminationTime == 0 || 
				time.earliestTerminationTime > currentEarliestTerminationTime) {
				time.secondEarliestTerminationTime = time.earliestTerminationTime;
				time.earliestTerminationTime = currentEarliestTerminationTime;
			}
			if (time.latestArrivalTime == 0 || 
				time.latestArrivalTime < currentLatestArrivalTime) {
				time.latestArrivalTime = currentLatestArrivalTime;
			}
		}
		return time;
	}
	
	public LinkedList<ServiceInvocation> impactAnalysisPerService(String serviceName, LinkedList<ServiceInvocation> affected){
		int capacity = 1; //REVISE THIS!!!!
		
		// Phase I: Fill the initial sliding window
		LinkedList<ServiceInvocation> inSystem = null;// = new LinkedList<ServiceInvocation>();
		ConcurrentHashMap<String, Boolean> consideredWFs 
			= new ConcurrentHashMap<String, Boolean>();	
		//TopTwoTime twoTimeInstants = fillInitialSlidingWindow(serviceName, capacity, affected, inSystem, consideredWFs);
		inSystem = fillInitialSlidingWindow(serviceName, capacity, affected, consideredWFs);
		TopTwoTime twoTimeInstants = this.computeTopTwoTime(inSystem);
		LinkedList<ServiceInvocation> newlyDelayedInThisRound = null;		
		
		// Phase II: See if there is impact on other service invocation instances.
		if (inSystem.size() < capacity) { // System not in full capacity
			// Nothing gets delayed
			// Check whether it is safe to terminate
			// Actually, move to the next service invocation
			// return null list.
		}
		else { // We now have a full *initial* sliding window.
			while (twoTimeInstants != null) {
				ServiceInvocation interfered 
					= getInterfered(serviceName, twoTimeInstants.latestArrivalTime, 
									twoTimeInstants.earliestTerminationTime);
				if (interfered == null) {
					// No interference with the service invocation with the earliestTerminationTime.
					// Remove the first invocation from the sliding window and fill it up again,
					// by looking for an invocation that overlaps with the secondEarliestTerminationTime.
					if (!inSystem.isEmpty()) {
						inSystem.removeFirst();	
						inSystem 
							= moveSlidingWindow(serviceName, inSystem, consideredWFs, capacity);
						// If twoTimeInstants is null
						// then there is no more overlapping service invocations
						// with this time window.
						// Move to the next added service invocation
						// Wait! all expected service invocations are added at once! 
						twoTimeInstants = this.computeTopTwoTime(inSystem);
					}
					else {
						break;
					}
				}
				else {
					// There is some interfered service invocation.
					// Update the list of service invocations that are delayed
					newlyDelayedInThisRound = assessAddChangeImpact(serviceName, capacity, twoTimeInstants, inSystem);
					// Move the sliding window and repeat the above process.
					// But, remote the first one first.
					if (!inSystem.isEmpty()) {
						inSystem.removeFirst();
						inSystem = moveSlidingWindow(serviceName, inSystem, consideredWFs, capacity);
						// If the sliding window cannot be filled, or it cannot find a service invocation
						// under a workflow that is not considered in this framework,
						// then exit this framework.
						twoTimeInstants = this.computeTopTwoTime(inSystem);
					}
					else {
						break;
					}
				}
			}
		}
		return newlyDelayedInThisRound;
	}
	
	public LinkedList<ServiceInvocation> getSubsequentlyDelayed(LinkedList<ServiceInvocation> delayed){
		LinkedList<ServiceInvocation> subsequentlyDelayed = new LinkedList<ServiceInvocation>();
		for (ServiceInvocation s : delayed) {
			// select * from where invocation.arrival_time < s.termination_time and preceding task=s.
			ServiceInvocation sub = new ServiceInvocation();
			sub.setWFID("wf2");
			//this.addedExpectedDelay(invocation, time)
			System.out.println("Arrival time of service invocation by task2 of wf1 delayed by " + 10);
			subsequentlyDelayed.add(sub);
		}		
		return subsequentlyDelayed;
		
	}
	
	public ConcurrentHashMap<String, LinkedList<ServiceInvocation>> 
	globalImpactAnalysisFramework(ConcurrentHashMap<String, LinkedList<ServiceInvocation>> change) {
		ConcurrentHashMap<String, LinkedList<ServiceInvocation>> allSubsequentlyDelayed 
			= new ConcurrentHashMap<String, LinkedList<ServiceInvocation>>();
		
		// 	For each affected service invocations (at the beginning, the very first service invocation)
		// 	Detect interference and assess impact.		
		Iterator<Map.Entry<String, LinkedList<ServiceInvocation>>> entries = change.entrySet().iterator();
		while (entries.hasNext()) {
		    Map.Entry<String, LinkedList<ServiceInvocation>> entry = entries.next();
		    String serviceName = entry.getKey();
		    LinkedList<ServiceInvocation> changesPerService = entry.getValue();
		    LinkedList<ServiceInvocation> delayed = this.impactAnalysisPerService(serviceName, changesPerService);
		    if (delayed != null) {
		    	// Some service invocations are impacted by the change
		    	LinkedList<ServiceInvocation> subsequentlyDelayed = getSubsequentlyDelayed(delayed);
		    	allSubsequentlyDelayed.put((String)entry.getKey(), subsequentlyDelayed);
		    }
		   // System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
		//return allSubsequentlyDelayed;
		ConcurrentHashMap<String, LinkedList<ServiceInvocation>> emptyList = new ConcurrentHashMap<String, LinkedList<ServiceInvocation>>();
		return emptyList;
	}
	
	public static void main(String [ ] args) {
		try {
			// Create an ImpactAnalyzer instance
			// and connect to a broker
			ImpactAnalyzer analyzer = new ImpactAnalyzer("fabre");
			
			String brokerURI = args[0]; 
			analyzer.connect(brokerURI);
			
			ConcurrentHashMap<String, LinkedList<ServiceInvocation>> change 
				= new ConcurrentHashMap<String, LinkedList<ServiceInvocation>>();
			ServiceInvocation s = new ServiceInvocation();
			s.setArrivalTime(0);
			s.setServiceFinishTime(10);
			s.setWFID("wf1");
			LinkedList<ServiceInvocation> changeInstance = new LinkedList<ServiceInvocation>(); 
			changeInstance.add(s);
			change.put("s1", changeInstance);
			
			while (true) {
				// For each service invocation introduced
				ConcurrentHashMap<String, LinkedList<ServiceInvocation>> furtherChange
					= analyzer.globalImpactAnalysisFramework(change);					
				if (furtherChange.isEmpty()) {
					break;
				}
				
			}
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
