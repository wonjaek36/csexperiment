package ca.utoronto.msrg.padres.tools.fabre;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ca.utoronto.msrg.padres.client.Client;
import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.message.Advertisement;
import ca.utoronto.msrg.padres.common.message.Predicate;
import ca.utoronto.msrg.padres.common.message.Publication;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.tools.eqo.MobileClient.ILogicType;
import ca.utoronto.msrg.padres.tools.eqo.MobileClient.MClient;
import ca.utoronto.msrg.padres.tools.eqo.MobileClient.MClientServer;
import ca.utoronto.msrg.padres.tools.eqo.MobileClient.TaskInstance;

public class SimpleService{
	
	private LinkedList<SimpleWorkflowTask> waitQueue
		= new LinkedList<SimpleWorkflowTask>();
	private LinkedList<SimpleWorkflowTask> inSystem
		= new LinkedList<SimpleWorkflowTask>();
	private int capacity = 0;
	private String serviceName;
	private boolean serviceFull = false;
	private String agentID;
	public ExecutorService exec;
	
	public final static String SVC_ARRIVAL = "arrive";
	public final static String SVC_START = "start";
	public final static String SVC_FINISH = "finish";
	
	public SimpleService(String serviceName, int capacity, String agentID) {
		try {
			this.capacity = capacity;
			this.serviceName = serviceName;
			this.exec = Executors.newFixedThreadPool(capacity);
			this.agentID = agentID;
			
			Advertisement clientAdv = new Advertisement();
			clientAdv.addPredicate("class", new Predicate("eq", "ServiceInvocation"));
			clientAdv.addPredicate("type", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("servicename", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("workflowid", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("taskid", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("instanceid", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("triggeredby", new Predicate("isPresent", "any"));
			clientAdv.addPredicate("triggers", new Predicate("isPresent", "any"));
			
			MClientServer engine = MClientServer.getInstance();
			MClient agent = engine.getClient(agentID);
			System.out.println("agentID: " + agentID + " going to advertise " + clientAdv);
		
			agent.advertise(clientAdv);
		} catch (ClientException e) {
			e.printStackTrace();
		}
	}
	
	public SimpleService() {
		// TODO Auto-generated constructor stubrsist
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName; 
	}
	
	public void associateWithAgent(String agentID) {
		this.agentID = agentID;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public synchronized void executeTask(SimpleWorkflowTask task, long executionTime) {
		inSystem.addFirst(task);
		exec.execute(new SimpleServiceTask("thread1", executionTime, inSystem, this));
	}
	
	public boolean isServiceFull() {
		if (inSystem.size() == capacity) {
			serviceFull = true;
		}
		else {
			serviceFull = false;
		}
		return serviceFull;
	}
	
	
	
	public synchronized void triggerService(SimpleWorkflowTask task) throws ParseException {
		task.setType(SVC_ARRIVAL);
		this.persistServiceInvocationInfo(task);		
		if (isServiceFull()) {
			System.out.print("Service is full: ");
			System.out.println("Adding task to wait queue: " + task.getTaskID() + " " + task.getWorkflowID());
			addToWaitQueue(task);
		}
		else {
			task.setType(SVC_START);
			this.persistServiceInvocationInfo(task);
			System.out.print("Task can be executed right away: ");
			System.out.println("Executing task: " + task.getTaskID() + " " + task.getWorkflowID());
			executeTask(task, task.getExecutionTime());
		}
	}
	
	public synchronized void addToWaitQueue(SimpleWorkflowTask s) {
		waitQueue.addFirst(s);
	}
	
	public synchronized void persistServiceInvocationInfo(SimpleWorkflowTask completedTask) throws ParseException {
		String pubMsg = "[class,ServiceInvocation]," +
				"[type," + completedTask.getType() + "]," +
				"[workflowid," + completedTask.getWorkflowID() + "]," +
				"[taskid," + completedTask.getTaskID() + "]," +
				"[instanceid," + completedTask.getInstanceID() + "]," +
				"[servicename," + completedTask.getServiceName() + "]," +				
				"[triggeredby," + completedTask.getTriggeredBy() + "]," +
				"[triggers," + completedTask.getTriggers() + "]";
		Publication pub = MessageFactory.createPublicationFromString(pubMsg);
		Date date = new Date();
		System.out.println("type: " + completedTask.getType() + "time it persisted: " + date);
		MClientServer engine = MClientServer.getInstance();
		MClient agent = engine.getClient(agentID);
		try {
			agent.publish(pub);
		} catch (ClientException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void removeFromInSystem() throws ParseException {
		SimpleWorkflowTask completedTask = inSystem.removeLast();
		completedTask.setType(SVC_FINISH);
		this.persistServiceInvocationInfo(completedTask);
		MClientServer engine = MClientServer.getInstance();
		MClient agent = engine.getClient(agentID);
		String instanceid = completedTask.getInstanceID();
		agent.GetInstance(instanceid).moveOn();
		
		// Execute the next pending task if there is any.
		while (inSystem.size() < capacity && waitQueue.size() > 0) {
			SimpleWorkflowTask task = waitQueue.removeLast();
			task.setType(SVC_ARRIVAL);
			this.persistServiceInvocationInfo(task);
			System.out.println("Executing pending task: " + task.getTaskID() + " " + task.getWorkflowID());
			task.setType(SVC_START);
			this.persistServiceInvocationInfo(task);
			this.executeTask(task, task.getExecutionTime());
		}
	}

	public String getServiceName() {
		return this.serviceName;
	}
}
