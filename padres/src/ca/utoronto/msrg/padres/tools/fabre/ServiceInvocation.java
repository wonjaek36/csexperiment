package ca.utoronto.msrg.padres.tools.fabre;

public class ServiceInvocation {
	private long arrivalTime = 0;
	private long serviceStartTime = 0;
	private long serviceFinishTime = 0;
	private String wfid = "";
	
	public long getArrivalTime() {
		return this.arrivalTime;
	}
	
	public long getServiceStartTime() {
		return this.serviceStartTime;
	}
	
	public long getServiceFinishTime() {
		return this.serviceFinishTime;
	}
	
	public void setArrivalTime (long arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public void setServiceStartTime (long serviceStartTime) {
		this.serviceStartTime = serviceStartTime;
	}
	
	public void setServiceFinishTime (long serviceFinishTime) {
		this.serviceFinishTime = serviceFinishTime;
	}
	
	public void delayArrivalTime(long delayAmount) {
		this.arrivalTime += delayAmount;
	}
	
	public void delayServiceStartTime(long delayAmount) {
		this.serviceStartTime += delayAmount;
	}
	
	public void delayServiceFinishTime(long delayAmount) {
		this.serviceFinishTime += delayAmount;
	}

	public void setWFID(String wfid) {
		this.wfid = wfid;
	}
	
	public String getWFID() {
		return wfid;
		
	}
}
