package ca.utoronto.msrg.padres.tools.eqo.MobileClient;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.log4j.Logger;

import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.common.comm.CommSystem.HostType;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIAddress;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIServerInterface;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIMessageListenerInterfce;
import ca.utoronto.msrg.padres.client.*;

	
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;


public class ProcessDeployer extends Client implements ActionListener {	
		
	// save pairs of ProcessId and Current InstanceId
	private static HashMap ProcessInstMap = new HashMap();
	// Save pairs of ProcessId&InstanceId and IssueTime;
	private Map InstIssueTimeMap = new HashMap();
	private int msgCount = 0;
	private String brokerURI = "";
	private MessageDestination clientDest;
	private RMIServerInterface rmiConnection = null;
	private JTextField input;
	private static JTextArea output;
	private JFrame mainFrame = null;	
	
	protected JTextPane notifications;
	protected JTextPane history;	
	
	public ProcessDeployer() throws RemoteException, ClientException {
		super("deployer");		
	}

	public void actionPerformed(ActionEvent e) {
		String command = input.getText();
		log(command + "\n");
		input.selectAll();
		if(command.startsWith("ptrigger"))
		{
			int index = command.indexOf(":");
			String str = command.substring(index+1);
			StreamTokenizer st = new StreamTokenizer(new StringReader(str));
			st.parseNumbers();
			st.whitespaceChars(',', ',');
			long interval = 0; 
			int times = 1;
			try {
				st.nextToken();
				Long inter = new Long(Math.round(st.nval));
				interval = inter.longValue();
				st.nextToken();
				inter = new Long(Math.round(st.nval));
				times = inter.intValue();
			} 
			catch (IOException error){
				
			}
			
			triggerProcessWInterval(interval, times);
			
			
		}else if (command.startsWith("move")){
			
			int index = command.indexOf(":");
			String str = command.substring(index+1);
			//StreamTokenizer st = new StreamTokenizer(new StringReader(str));
			StringTokenizer st = new StringTokenizer(str, " ");
			//st.parseNumbers();
			//st.whitespaceChars(',', ' ');
			String clientId=null, source=null, target=null; 

			try {
				/*
				st.nextToken();
				clientId = st.sval;
				st.nextToken();
				source = st.sval;
				st.nextToken();
				target = st.sval;
				*/
				clientId = st.nextToken();
				source = st.nextToken();
				target = st.nextToken();
				
			} 
			catch (Exception error){
				
			}
			triggerMovement(clientId, source, target);
			
		}
		else{
			try {
				handleCommand(command);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private String getNewMessageID() {
		return String.format("%s-%d", clientDest.toString(), msgCount++);
	}
	
	
	public void notify(Publication pub) {
		long receiveTime = System.currentTimeMillis();
		log("Got Publication: " + pub + " at (" + receiveTime +") ");
		Map pairMap = new HashMap();
		pairMap = pub.getPairMap();
		String ProcessInstId = (String)pairMap.get(ILogicType.PROCESS_ID);
		ProcessInstId += "&";
		ProcessInstId += (String)pairMap.get(ILogicType.INSTANCE_ID);
		if(InstIssueTimeMap.containsKey(ProcessInstId))
		{
			Long issuetime = (Long)InstIssueTimeMap.get(ProcessInstId);
			long latency = receiveTime - issuetime.longValue();
			log("Latency of (" + ProcessInstId + ") is:" + latency +" \n");
		}
		
	}
	
	public void issueInstance(String processId, String instanceId, String receivers, Variable payLoad )
	{		
		Publication pub;
		try {
			pub = MessageFactory.createPublicationFromString(
					"[class," + ILogicType.TASK_TRIGGER +"]," 
					+ 
					"[" + ILogicType.PROCESS_ID +","+  processId + "],"
					+
					"[" + ILogicType.INSTANCE_ID +","+instanceId +  "],"
					+
					"[" + ILogicType.SENDER_ID +  ","+ "START" + "],"
					+
					"[" + ILogicType.RECEIVER_ID + ","+ receivers + "]"
					);
		
					
			if(payLoad != null && !payLoad.var.isEmpty())
			{
				pub.setPayload(payLoad);
			}
			System.out.println(" trigger a process: " + pub + "\n");
			System.out.println("======================================");		    			
	    
			publish(pub);
			log("publish a process trigger ....\n");
			log("->" + pub + "at (" +System.currentTimeMillis() + ") \n");
			Long time = new Long(System.currentTimeMillis());
			this.InstIssueTimeMap.put((processId + "&" + instanceId), time);
	    
		} catch (ClientException e) {
			System.out.println(" Error occurs in process trigger pub"+e);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	/*reag process file, decide how to trigger the process
	 * 
	 */
	public void triggerProcess()
	{
		String triggerFile = this.chooseFile();
		if (triggerFile == "")
		{
			log("Loading trigger process canceled!" + "\n");
			return;
		}

	    String line, processId=null, receiver=null;
	    int instanceNum = 0;
	    Variable var = null;
	    int numTriggers = 1;
	    
		// handle the choosed deploy file
		try{
		    BufferedReader br = new BufferedReader(new FileReader(triggerFile));
		    log("Process file: " + triggerFile + " is being opened..." + "\n");

		    while((line = br.readLine()) != null ){
		    	if (line.startsWith("#") || line.length() == 0)
		    	{}
		        // skip # comment line or empty line
		    	else if (line.startsWith("ProcessID") ){
		     
		        	int index = line.indexOf(":");  //index or -1
		            processId  = line.substring(index+1);
		            if(!ProcessInstMap.containsKey(processId))
		            {
		            	log("Process file: " + triggerFile + " has not been deployed. Please deploy it at first!" + "\n");
		            	return;
		            }
		             
		            Integer num = (Integer)ProcessInstMap.get(processId);
		            instanceNum =num.intValue();
	        	}
		    	else if (line.startsWith("START") ){
		        	int index = line.indexOf(":");
		        	receiver  = line.substring(index+1);
		        	instanceNum += 1;
		        }
		    	else if (line.startsWith("NUMTRIGGERS") ){
		        	int index = line.indexOf(":");
		        	numTriggers  = Integer.valueOf(line.substring(index+1));
		        }
		    	else if (line.startsWith("GLOBALVARIABLES") ){
		        	int index = line.indexOf(":");
		        	String  str = null;
		        	if(index!=-1&&(str = line.substring(index+1))!=null)
		        	{
		        		
	            	    //global variables,like a 100 b 60. etc.
		            	String temp[] = str.split(" ");
		            	Map map = new HashMap();
		            	for(int i=0; i< (temp.length); i+=2)
		            	{
		            		map.put(temp[i], temp[i+1]);
		            	}
		            	var = new Variable();
		            	var.var = map;
		            	///issueInstance(processId, "INSTANCE_"+instanceNum, receiver,var);
		        	}
	            	break;
		        }
		    }
		}
		catch (FileNotFoundException ef){
			log("file " + triggerFile + " is not found!");
		}catch (IOException ei) {
		        ei.printStackTrace();
		}

		for (int i = 0; i < numTriggers; i++) {
			issueInstance(processId, "INSTANCE_"+instanceNum, receiver, var);
			ProcessInstMap.remove(processId);
	    	ProcessInstMap.put(processId,new Integer(instanceNum));
	    	instanceNum++;
		}
		
	}
	
	
	/* trigger a process for many times, with particular interval 
	 * 
	 */
	public void triggerProcessWInterval(long interval, int times)
	{
		String triggerFile = this.chooseFile();
		if (triggerFile == "")
		{
			log("Loading trigger process canceled!" + "\n");
			return;
			
		}
		triggerProcessWInterval(triggerFile, interval, times);
	}
	
	public void triggerProcessWInterval(String triggerFile, long interval, int times)
	{
	    String line, processId=null, receiver=null;
	    int instanceNum = 0;
	    Variable var = null;
	    
		// handle the chosen deploy file
		try{
		    BufferedReader br = new BufferedReader(new FileReader(triggerFile));
		    log("Process file: " + triggerFile + " is being opened..." + "\n");

		    while((line = br.readLine()) != null ){
		    	if (line.startsWith("#") || line.length() == 0)
		    	{}
		        // skip # comment line or empty line
		    	else if (line.startsWith("ProcessID") ){
		     
		        	int index = line.indexOf(":");  //index or -1
		            processId  = line.substring(index+1);
		            if(!ProcessInstMap.containsKey(processId))
		            {
		            	//log("Process file: " + triggerFile + " has not been deployed. Please deploy it at first!" + "\n");
		            	//return;
		            	// HACK to make it work with command line.
		            	ProcessInstMap.put(processId, new Integer(0));
		            }
		             
		            //Integer num = (Integer)ProcessInstMap.get(processId);
		            //instanceNum =num.intValue();
	        	}
		    	else if (line.startsWith("START") ){
		        	int index = line.indexOf(":");
		        	receiver  = line.substring(index+1);
		        	//instanceNum += 1;
		        	
		        }
		    	else if (line.startsWith("GLOBALVARIABLES") ){
		        	int index = line.indexOf(":");
		        	String  str = null;
		        	if(index!=-1&&(str = line.substring(index+1))!=null)
		        	{
		        		
	            	    //global variables,like a 100 b 60. etc.
		            	String temp[] = str.split(" ");
		            	Map map = new HashMap();
		            	for(int i=0; i< (temp.length); i+=2)
		            	{
		            		map.put(temp[i], temp[i+1]);
		            	}
		            	var = new Variable();
		            	var.var = map;
		            	issueInstance(processId, "INSTANCE_"+instanceNum, receiver,var);
		        	}
	            	break;
		        	
		        }
		    }
		}
		catch (FileNotFoundException ef){
			log("file " + triggerFile + " is not found!");
		}catch (IOException ei) {
		        ei.printStackTrace();
		}
		
		Integer num = (Integer)ProcessInstMap.get(processId);
        instanceNum = num.intValue();
		for(int i=0; i<times; i++){
	        
	        instanceNum += 1;
			issueInstance(processId, "INSTANCE_"+instanceNum, receiver,var);
			try  {
				Thread.sleep(interval);}
			catch (InterruptedException ie){
				ie.printStackTrace();
			}			
		}
		ProcessInstMap.remove(processId);
    	ProcessInstMap.put(processId,new Integer(instanceNum));
	}	
	
	
	public void initiate() throws ParseException
	{
		//issues deployment advertisement
		Advertisement  adv = new Advertisement (
				"[class,eq," + ILogicType.TASK_DEPLOYER +"]," + 
				"[" + "ENGINE_NAME" + ",isPresent," + "'ATM'" + "]");
		System.out.println(" Adv TASK_DEPLOYER message: " + adv + "\n");
	    System.out.println("======================================");
	    try{
	    	advertise(adv);
	    	log("advertisement deployment:");
	    	log(adv + "\n");
	   } catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //issues process trigger advertisement
		Advertisement  adv1 = new Advertisement (
				"[class,eq," + ILogicType.TASK_TRIGGER +"]," + 
				"[" + ILogicType.PROCESS_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.INSTANCE_ID + ",isPresent," +"ATM" +  "],"
				+
				"[" + ILogicType.SENDER_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.RECEIVER_ID + ",isPresent," + "ATM" + "]"
				);
		System.out.println(" Adv TASK_DEPLOYER message: " + adv1 + "\n");
	    System.out.println("======================================");
	    try{
	    	advertise(adv1);
	    	log("advertisement instance trigger:");
	    	log(adv1 + "\n");	   
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	    //issue a process subscription to subscribe finished instances
	    Subscription  sub1 = MessageFactory.createSubscriptionFromString(
				"[class,eq," + ILogicType.TASK_TRIGGER +"]," + 
				"[" + ILogicType.PROCESS_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.INSTANCE_ID + ",isPresent," +"ATM" +  "],"
				+
				"[" + ILogicType.SENDER_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.RECEIVER_ID + ",eq," + "END" + "]"
				);
		System.out.println(" Sub Instance END message: " + sub1 + "\n");
	    System.out.println("======================================");
	    try{
	    	subscribe(sub1);
	    	log("Subscribe instance END message:");
	    	log(sub1 + "\n");	    
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //***********************************************
	    //issue an ADV for triggering client movement
	    Advertisement  adv2 = new Advertisement (
	    		"[class,eq,"+ MobilityCoordinator.MOVEMENT +"]," 
				+ 
				"[" + MobilityCoordinator.MESSAGETYPE +",=,"+  MobilityCoordinator.M_Trigger + "],"
				+ 
				"[" + MobilityCoordinator.SOURCESERVER +",isPresent,"+ "ATM" + "],"
				+
				"[" + MobilityCoordinator.TARGETSERVER +",isPresent,"+ "ATM" +  "],"
				+
				"[" + MobilityCoordinator.MClientID +",isPresent,"+ "ATM" +  "]"
				);
	    System.out.println(" Adv Client MOVEMENT message: " + adv2 + "\n");
	    System.out.println("======================================");
	    try{
	    	advertise(adv2);
	    	log("advertisemtent client Movement trigger:");
	    	log(adv2 + "\n");
	  
	  
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void testMovement()
	{
		Publication pub;
		try {
			pub = MessageFactory.createPublicationFromString(
					"[class," + MobilityCoordinator.MOVEMENT +"]," 
					+ 
					"[" + MobilityCoordinator.MESSAGETYPE + "," + MobilityCoordinator.M_Trigger + "],"
					+ 
					"[" + MobilityCoordinator.SOURCESERVER +","+ "A" + "],"
					+
					"[" + MobilityCoordinator.TARGETSERVER +","+ "B" +  "],"
					+
					"[" + MobilityCoordinator.MClientID +","+ "TASK_1" +  "]"
					);
		
			System.out.println(" Publish Client MOVEMENT trigger message: " + pub + "\n");
			System.out.println("======================================");
	    
	    	publish(pub);
	    	log("publish a client Movement trigger:");
	    	log(pub + "\n");
	  
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	private void triggerMovement(String clientId, String source, String target)
	{
		
		try {
			Publication pub;
			pub = MessageFactory.createPublicationFromString(
					"[class," + MobilityCoordinator.MOVEMENT +"]," 
					+ 
					"[" + MobilityCoordinator.MESSAGETYPE + "," + MobilityCoordinator.M_Trigger + "],"
					+ 
					"[" + MobilityCoordinator.SOURCESERVER +","+ source + "],"
					+
					"[" + MobilityCoordinator.TARGETSERVER +","+ target +  "],"
					+
					"[" + MobilityCoordinator.MClientID +","+ clientId +  "]"
					);
		
			System.out.println(" Publish Client MOVEMENT trigger message: " + pub + "\n");
			System.out.println("======================================");
	    	publish(pub);
	    	log("publish a client Movement trigger:");
	    	log(pub + "\n");	    	
		} catch (ClientException e) {
			System.out.println(" Error occurs in Movement trigger PUB"+e);			
			e.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	private String chooseFile()
	{
		String file=null;
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File("."));
		
	
		//chooser.setDialogTitle(IDeployResources.FILE_CHOOSER_TITLE);
		chooser.setFileFilter(new javax.swing.filechooser.FileFilter() { 
		    public boolean accept(File f) {
		        String name = f.getName().toLowerCase();
		        return name.endsWith(".txt") || name.endsWith(".app") || f.isDirectory();
			}
	 
		    public String getDescription() {
		        return null;
		    }
		});
	
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION){
			file = chooser.getSelectedFile().getAbsolutePath();
		}else{
			file = "";
		}
		return file;
		
	}

	private void deploy(){
		
		String deployFile = this.chooseFile();
		if(deployFile=="")
		{
			log("Loading Deployment file canceled!" + "\n");
			return;
		}
		deploy(deployFile);
	}
	
	private void deploy(String deployFile){
		
		// handle the chosen deploy file
		try{
		    BufferedReader br = new BufferedReader(new FileReader(deployFile));
		    log("Process file: " + deployFile + " is being opened..." + "\n");
		 
		    String line;
		    String strProcId = null;
		    TaskContainer task =  null;
		    int index;
		    Advertisement adv = null;
		    Subscription sub = null;
		    CompositeSubscription cs = null;
		    Postcondition post = null;
		    
		    String targetEngineName = null;
		    
		    while((line = br.readLine()) != null ){
		        // skip # comment line or empty line
		        if (line.startsWith("#") 
		        		|| line.length() == 0 
		        		|| line.startsWith("START")
		        		|| line.startsWith("GGLOBALVARIABLES"))
		        {
		            // do nothing
		        } 
		        else
		        {
		            // handle only pub/sub/adv/unsub/unadv
		        	index = line.indexOf(":");  //index or -1
		            if (line.startsWith("ProcessID"))
		            {			            	
		            	strProcId = line.substring(index+1);	
		            	ProcessInstMap.put(strProcId,new Integer(0));
		            }
		            else if (line.startsWith("<TASK->") ||
		            		 line.startsWith("<TASK>"))
		            {
		            	task = new TaskContainer();
		            	task.m_idProcess = strProcId;
		            	task.m_objPrecondition = new Vector();
		            }
		            else if (line.startsWith("TaskID"))
		            {
		               	task.m_idTask = line.substring(index + 1);		            	
  	
		            }
		            else if (line.startsWith("PreNode"))
		            {
		            	task.m_typePreNode = line.substring(index + 1);
		            	
		            }
		            else if (line.startsWith("PostNode"))
		            {
		            	task.m_typePostNode = line.substring(index + 1);
		            	
		            }
		            else if (line.startsWith("Wait"))
		            {
		            	task.m_objAction = new Action(ILogicType.ACTION_WAIT);
		            }
		            else if (line.startsWith("Invoke"))
		            {
		            	task.m_objAction = new Action(ILogicType.ACTION_SERINVOKE);
		            	String str = line.substring(index+1);
		            	// refernum refervalue receiverIDs "else" receiverIds
		            	String temp[] = str.split(" ");
		            	String serviceName = temp[0];
		            	int capacity = Integer.parseInt(temp[1]);
		            	task.serviceName = serviceName;
		            	task.capacity = capacity;
		            }
		            else if (line.startsWith("Decision"))
		            {
		            	task.m_objAction = new Action(ILogicType.ACTION_DECISION);
		            }
		            else if (line.startsWith("PostIDs"))
		            {
		            	String str = line.substring(index+1); //may contain more than one Senders
		            	adv =  new Advertisement(
								"[class,eq," + ILogicType.TASK_TRIGGER +"]," 
								+ 
								"[" + ILogicType.PROCESS_ID + ",eq," + task.m_idProcess + "],"
								+
								"[" + ILogicType.INSTANCE_ID + ",isPresent," +"ATM"+  "],"
								+
								"[" + ILogicType.SENDER_ID + ",eq," + task.m_idTask + "],"
								+
								///"[" + ILogicType.RECEIVER_ID + ",str-le," + str + "]"
								"[" + ILogicType.RECEIVER_ID + ",isPresent," + str + "]"
								);
		            }
		            else if (line.startsWith("PostPubCondition"))
		            {
		            	//to indicate the condition of or_split
		            	//if the type post condition is not or_split, this filed is null
		            	if (index ==-1) 
		            	{
		            		post = new Postcondition(task.m_typePostNode, adv, null, null);
		            		task.m_objPostcondition = post;
		            		continue;
		            	}
		            	String str = line.substring(index+1);
		            	// refernum refervalue receiverIDs "else" receiverIds
		            	String temp[] = str.split(" ");
		            	String variable = temp[0];
		            	
		            	Map map = new HashMap();
		            	for(int i=1; i< (temp.length-1); i+=2)
		            	{
		            		map.put(temp[i], temp[i+1]);
		            	}
		            	post = new Postcondition(task.m_typePostNode, adv, variable, map);
		            	task.m_objPostcondition = post;
		            	
		            }
		            else if (line.startsWith("PreSubCondition"))
		            {             
					
		            	String str = line.substring(index+1);
		            	
		            	//senderId(s), split by ";"
	            		String temp[]= str.split(" ");
	            		String subStr = null;
	            					            		
	            		for (int i =0; i<temp.length; i++)
	            		{
	            			subStr = "[class,eq," + ILogicType.TASK_TRIGGER +"]," 
							+ 
							"[" + ILogicType.PROCESS_ID + ",eq," + task.m_idProcess + "],"
							+
							"[" + ILogicType.INSTANCE_ID + ",isPresent," +"ATM" +  "],"
							+
							"[" + ILogicType.SENDER_ID + ",eq," + temp[i] + "],"
							+
							"[" + ILogicType.RECEIVER_ID + ",str-contains," + task.m_idTask  + "]";
	            			sub = MessageFactory.createSubscriptionFromString(subStr);
	            			task.m_objPrecondition.add(sub);
	            		}
		            }
		            else if (line.startsWith("TargetEngine"))
		            {
		            	targetEngineName = line.substring(index+1);
		            	
		            }
		            else if(line.startsWith("<-TASK>") ||
		            		line.startsWith("</TASK>"))
		            {
		            	
		    			Publication pub = MessageFactory.createPublicationFromString(
		    					"[class," + ILogicType.TASK_DEPLOYER +"]," + 
		    					"[" + "ENGINE_NAME," + targetEngineName + "]");
		    			System.out.println(" Deploy to A TASK_DEPLOYER message: " + pub + "\n");
		    		    System.out.println("======================================");

		    		    pub.setPayload(task);
		    			
		    		    try{
		    		    	publish(pub);
		    		    	log("publish task :" + task.m_idTask);
		    		    	log(pub + "\n");  		    
		    		   
		    			} catch (ClientException e) {
		    				System.out.println(" Error occurs in Deployment task :" + task.m_idTask + " : " + e);
						
		    			} 
		    			
						task = null;
						targetEngineName = null;
						cs=null;
						sub=null;
						index = 0;
						adv = null;
						post = null;
		            	
		            } 
	
		        }
		    }
		} catch (FileNotFoundException ef){
			log("file " + deployFile + " is not found!");
	    } catch (IOException ei) {
	        ei.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	public JFrame createJFrame() {
		final JFrame frame = new JFrame("Process Deployer");
				
		final JMenuBar mb = new JMenuBar();
		final JMenu mainMenu = new JMenu("Main");
		//final JMenu triggerMenu = new JMenu("Trigger");
		final JMenu applMenu = new JMenu("Deployer");
		//final JMenu postProMenu = new JMenu("Others");
		
		
		JMenuItem exitItem = new JMenuItem("Exit");	
		mainMenu.add(exitItem);	
		mb.add(mainMenu);
		

		
		JMenuItem deployAppl = new JMenuItem("Deploy process");
		applMenu.add(deployAppl);

		
		JMenuItem issueTriggerItem = new JMenuItem("Trigger process");
		applMenu.addSeparator();
		applMenu.add(issueTriggerItem);
		
		JMenuItem issueMovementItem = new JMenuItem("Move Client");
		//applMenu.addSeparator();
		//applMenu.add(issueMovementItem);
		
		mb.add(applMenu);
		frame.getRootPane().setJMenuBar(mb);
		
		exitItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame.dispose();
						//rmiConnection.disconnect();
						disconnectAll();
						System.exit(0);
					}
				});
					
		issueTriggerItem.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										System.out.println("issue trigger...");
										//trigger a process
																		
										//triggerProccessTest();
										triggerProcess();
									
									}
								});
		
		deployAppl.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										System.out.println("deploying...");
										//generateDeploymentTest();
										deploy();
									   }
								});
		
		issueMovementItem.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									System.out.println("Moving...");
									testMovement();
									
								   }
							});
		
		

		JPanel contents = new JPanel();
		contents.setLayout(new BorderLayout());
		
		output = new JTextArea(20, 50);
		output.setEditable(false);
		JScrollPane outputpane = new JScrollPane(output);
		contents.add(outputpane, BorderLayout.NORTH);
			
		
		input = new JTextField(50);
				
		input.setEditable(true);
		input.addActionListener(this);
		contents.add(input, BorderLayout.SOUTH);
				
				
		frame.setContentPane(contents);
				
		mainFrame = frame;
				
		return frame;
	}

	public static JTextArea getOutput(){
		return output;
	}
	
	public RMIServerInterface getConnection(){
		return rmiConnection;
	}
	
	protected void printClientAction(String outString, Color txtColor) {
		try {
			Document notificationText = history.getDocument();
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);
			mainFrame.repaint();
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	protected void printNotification(String outString, Color txtColor) {
		try {
			Document notificationText = notifications.getDocument();
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);
		} catch (BadLocationException e) {
			//printClientAction(e.getMessage(), Color.RED);
		}
	}
	
	protected void connectToBroker(String uri) {
		brokerURI = uri;
		if (brokerURI != null) {
			try {
				//printClientAction("Connecting to broker: " + brokerURI + " ...", Color.BLACK);
				BrokerState brokerState = connect(brokerURI);
				String result = String.format("Connected to %s", brokerState.getBrokerAddress().getNodeURI());
				System.out.println("result: " + result);
				//printClientAction(result, Color.GREEN);
			} catch (ClientException e) {
				//printClientAction(e.getMessage(), Color.RED);
			}
		}
	}
	
	protected void disconnectToBroker() {
		List<String> brokerURIList = new ArrayList<String>();
		for (BrokerState brokerState : brokerStates.values()) {
			brokerURIList.add(brokerState.getBrokerAddress().getNodeURI());
		}
		String[] brokerList = brokerURIList.toArray(new String[0]);
		String brokerURI = brokerList[0];
		if (brokerURI != null) {
			try {
				//printClientAction("Disconnecting from broker: " + brokerURI + " ...", Color.BLACK);
				BrokerState brokerState = disconnect(brokerURI);
				String result = String.format("Connected to %s", brokerState.getBrokerAddress().getNodeURI());
				//printClientAction(result, Color.GREEN);
			} catch (ClientException e) {
				//printClientAction(e.getMessage(), Color.RED);
			}
		}
	}
	
	public void exitClient() {
		try {
			shutdown();
		} catch (ClientException e) {
			//printClientAction("Error in shutting down client: " + e.getMessage(), Color.RED);
		}
	}
	
	public void log(String s) {
		System.out.print(s);
		if (output != null)
			output.append(s);
	}

	public static void main(String[] args) throws RemoteException {

		if (args.length < 1) {
			System.out.println("Usage: ProcessDeployer <broker uri>");
			System.out.println("Usage: ProcessDeployer <broker uri> deploy <deployfile>");
			System.out.println("Usage: ProcessDeployer <broker uri> trigger <deployfile> <interval> <instances>");
			System.exit(1);
		}

		boolean gui = true;
		String cmd = null;
		if (args.length > 1) {
			cmd = args[1];
			gui = false;
		}
		
//		System.setSecurityManager(new RMISecurityManager() {
//			public void checkConnect(String host, int port) {
//			}
//			public void checkConnect(String host, int port, Object context) {
//			}
//		});

		JFrame.setDefaultLookAndFeelDecorated(true);
		
		try {
			final ProcessDeployer c = new ProcessDeployer();		
		
			if (gui) {
				JFrame frame = c.createJFrame();
				frame.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						c.exitClient();
						System.exit(0);
					}
				});
				frame.pack();
				frame.setVisible(true);
			}
			c.connectToBroker(args[0]);
			c.initiate();
			
			if (cmd == null) {
				// Nothing to do.
			} else if (cmd.equals("deploy")) {
				String deployFile = args[2];
				c.deploy(deployFile);
				c.disconnectAll();
				System.exit(0);
			} else if (cmd.equals("trigger")) {
				String triggerFile = args[2];
				long interval = Long.parseLong(args[3]);
				int times = Integer.parseInt(args[4]);
				c.triggerProcessWInterval(triggerFile, interval, times);				
				c.disconnectAll();
				System.exit(0);
			}
		} catch (ClientException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
