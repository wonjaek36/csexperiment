package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.io.Serializable;
import java.util.List;

public class TaskContainer implements Serializable {
	public static final long serialVersionUID = 1;
	public String m_idProcess; // id of the process it belongs to
	public String m_idTask; //id of the node of the process if avalible.
	//structure type of current node precondition;
	// OR_JOINT  : any incomming publication will trigger the task 
	// AND_JOINT : only when all publications related to one instance arrives, the task can be triggerd
	// WHILE     : looks like OR_JOINT;  //but this one need special disposal, the instance should 
	// SEQUENCE  : any incomming publication will trigger the task	
	public String m_typePreNode; 
	//structure type of Post condition of current node ;
	// SEQUENCE
	// AND-SPLIT
	// OR_SPLIT
	// AND_OR_SPLIT
	public String m_typePostNode;

	//sub specification:
	//basic information:
	//<Map of Predicate> predicateMap: 
	//	String Process_ID: id of the process ( == current ProcessID)
	//	String Instance_ID: id of the instance ( == $x (this is to let different element of cs share same instance id) )
	//	String Sender_Task_ID: id of the previous task (== predefined previous task Id)
	//  String Reciever_Task_ID: id of current task (==Current task Id)
	//	othePredicate information: can describe the incoming Pub schema.
	public List m_objPrecondition; // maintains a sub or serveral sub object to identify precondition of task 
	
	//PostCondition
	public Postcondition m_objPostcondition;

	//maintains task information
	public Action m_objAction; 
	
	public List advList;//list of adv msg ids, in form of String
	public List subList;//list of sub msg ids, in form of String, correspoingding to list "m_objprecondition"

	public String serviceName;
	public int capacity;
}
