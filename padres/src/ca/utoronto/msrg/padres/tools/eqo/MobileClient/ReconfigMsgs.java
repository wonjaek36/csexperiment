package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

import ca.utoronto.msrg.padres.common.message.MessageDestination;

public class ReconfigMsgs implements Serializable {
	public List advList; //this one is the Ids of advs, in form of string.
	public List subList;
	public Map subMap; //this one contains list of subscription msgs ids. 
	public ReconfigMsgs(){
		advList = new Vector();
		subMap = new HashMap();		
	}
	public MessageDestination lastHop;
	
	public void setLastHop(MessageDestination md) {
		lastHop = md;
	}
	public boolean isEmpty(){
		
		if((advList==null||advList.isEmpty())&&
				(subMap==null||subMap.isEmpty())){
			return true;
		}
		else{
			return false;
		}
	}
}
