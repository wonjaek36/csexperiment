package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import sun.management.resources.agent;

import ca.utoronto.msrg.padres.client.ClientException;
import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.tools.fabre.SimpleService;
import ca.utoronto.msrg.padres.tools.fabre.SimpleWorkflowTask;
import ca.utoronto.msrg.padres.tools.fabre.Token;

/* this class contains information and potential queue of an instance
 * this class should be a thread?
 */

public class TaskInstance implements Serializable, Comparable {

	public static final long serialVersionUID = 1;
	private String m_idTaskInstance;  // id of current task TaskInstance
	private int m_intPubNum = 1;   // only for AND_JOINT-> it = cs.getSubNum();otherwie , it =1; 
	private String agentId;
	private long waitTime = 1000;
	private StdRandom r = new StdRandom();
	private boolean serviceCallReturned = false;
	
	//if the precondition type is AND_JOINT, the TaskInstance should wait until 
	//it gets all the publications
	private List m_listPubs ; // list of incoming publications

	private int state; //indicate that if the TaskInstance is still buffering
  
	
	public TaskInstance()
	{		
		m_idTaskInstance = null;
		m_listPubs =new Vector();
		agentId=null;
		state=ILogicType.INST_BUFFERING;
	}
	
	public TaskInstance(String instId, String aId) {
		//super();
		// TODO Auto-generated constructor stub
		m_idTaskInstance = instId;
		m_listPubs =new Vector();
		agentId=aId;
		state=ILogicType.INST_BUFFERING;
	}

	public TaskInstance(int pubNum, String instId, String aId) {
		//super();
		// TODO Auto-generated constructor stub
		m_intPubNum = pubNum;
		m_listPubs = new Vector();
		m_idTaskInstance = instId;
		agentId = aId;
		state=ILogicType.INST_BUFFERING;
		

	} 
	
	public void setTaskInstanceId(String id)
	{
		m_idTaskInstance = id;
	}
	
	public void setHostClientId(String id)
	{
		agentId = id;
	}
	public void setPubNum(int num)
	{
		m_intPubNum = num;
	}

	
	public int getState()
	{
		return state;
		
	}
	
	//append a publication message
	public void addPub(Publication p){
		m_listPubs.add(p);
		if (m_listPubs.size()== m_intPubNum){
			//handle action
			handleAction();
		}
		
	}
	
	/*
	 *  The TaskInstance should be deleted after it has finish its own action.
	 */
	
	public void handleAction(){
		
		state=ILogicType.INST_RUNNING;
		Object obj = null;
		
		MClientServer engine = MClientServer.getInstance();
		MClient agent = engine.getClient(agentId);
		TaskContainer t  = agent.getTask();
		Action action = t.m_objAction;
		Postcondition post = t.m_objPostcondition;
		Publication pub = null;
		Variable var = null;
		Map mapVar = new HashMap();

		// withdraw global variables. in this version, suppose all pubs arriving at current node have same payload
		obj = m_listPubs.get(0);
		if (!obj.getClass().equals(Publication.class))
			return;// need exception handling
		pub = (Publication)obj;
		var = (Variable)pub.getPayload();
		
		if (var!=null)
		{
			mapVar = var.var;
		}
		else
		{
			if(t.m_typePostNode == ILogicType.OR_SPLIT||t.m_typePostNode == ILogicType.AND_OR_SPLIT)
			{
				if(var.var.isEmpty())
				{
					System.out.println("ERROR: no variable for Choice \n");
					System.exit(0);
				}
			}			
		}
				
		//withdraw
		int actionType = action.getType();
		switch(actionType)
		{
		case ILogicType.ACTION_WAIT: //only wait, do not invoke service
		
			/*
			obj = new Object();
			synchronized(obj){
				try{
					obj.wait(1000);
				}
				catch(InterruptedException e){
					System.out.println("'n error in TaskInstance execution/n");
				}
			
			}
			*/
			//for system test
			double a = 2;
			for(long i =0;i<=1000000;i++)
			{
				a = a*2;		
				a = a/2;
			}
			
			break;

		case ILogicType.ACTION_DECISION: //only wait, do not invoke service
			// Increment "count" variable
			int count = 0;
			String value = (String)mapVar.get("count");
			if (value != null) {
				count = Integer.parseInt(value);
				mapVar.put("count", Integer.toString(++count));
			}

			// Switch bcond and ccond after some activities
			int bruns = 1;
			int cruns = 100;
			int bcount = 2 * bruns + 1;
			int ccount = bcount + 2 * cruns - 1;
			if (count == bcount) {
				mapVar.put("bcond", "false");
			}
			else if (count == ccount) {
				mapVar.put("ccond", "false");
			}

			// Set bcond and ccond based on probabilities
			value = (String)mapVar.get("bprob");
			if (value != null) {
				float prob = Float.parseFloat(value);
				float rand = new Random().nextFloat();
				if (rand < prob)
					mapVar.put("bcond", "true");
				else
					mapVar.put("bcond", "false");
			}
			value = (String)mapVar.get("cprob");
			if (value != null) {
				float prob = Float.parseFloat(value);
				float rand = new Random().nextFloat();
				if (rand < prob)
					mapVar.put("ccond", "true");
				else
					mapVar.put("ccond", "false");
			}
			
			// Switch "foo" variable between "left" and "right"
			value = (String)mapVar.get("foo");
			if (value != null) {
				if (value.equals("left")) {
					mapVar.put("foo", "right");
				} else {
					mapVar.put("foo", "left");
				}
			}
			//change values of the variables
			var.var = mapVar;
			break;

		case ILogicType.ACTION_SERINVOKE:
			// Invoke service
			// Get execution time.
			/*
			
			String workflowid = (String) pairMap.get(ILogicType.PROCESS_ID);
			String taskid = (String) pairMap.get(ILogicType.RECEIVER_ID);
			String instanceid = this.m_idTaskInstance;
			
			
			String servicename = (String) pairMap.get(ILogicType.SERVICE_ID);
			*/
			Map<String, Serializable> pairMap =  pub.getPairMap();
			String triggeredby = (String) pairMap.get(ILogicType.SENDER_ID);
			String triggers = (String) pairMap.get(ILogicType.RECEIVER_ID);
			
			System.out.println("triggered by: " + triggeredby);
			System.out.println("triggers: " + triggers);
			System.out.println("Process: " + t.m_idProcess);
			System.out.println("Service name: " + t.serviceName);
			System.out.println("Service capacity: " + t.capacity);
			System.out.println("Task id: " + t.m_idTask);
			System.out.println("Instance id: " + m_idTaskInstance);
			
			SimpleWorkflowTask wf = new SimpleWorkflowTask();
			wf.setExecutionTime(5000);
			wf.setServiceName(t.serviceName);
			wf.setInstanceID(m_idTaskInstance);
			wf.setTaskID(t.m_idTask);
			wf.setWorkflowID(t.m_idProcess);
			wf.setArrivalTime(System.currentTimeMillis());
			wf.setTriggeredBy(triggeredby);
			wf.setTriggers(triggers);
			
			
			invokeService(wf); // Should not wait.	wait a minute it should wait....	
			listen();
			break;
			//in this version, only consider value
			/*
			List lstInputVariable = new Vector();
			List lstOutputVariable = new Vector();
			String Url = action.getUrlService();
			String ActionName = action.getStrActionName();		
			lstInputVariable = action.getInputParas();
			lstOutputVariable = action.getOutputParas();
	
			List lstServiceInput =  new Vector();
			List lstServiceOutput =  new Vector();
	
			String strVar;
			for (int i=0; i< lstInputVariable.size(); i++) {
				strVar = lstInputVariable.get(i).toString();
				if (!mapVar.containsKey(strVar)){
					continue;
				}
				lstServiceInput.add(mapVar.get(strVar));
			}
		    int outputNum = lstOutputVariable.size();
		    int capacity = 0; // Get it from workflow specification?
		    lstServiceOutput = this.invokeService(lstServiceInput,outputNum, Url,ActionName); 
	
		    if(lstServiceOutput.size()!= lstOutputVariable.size())
		    {
		    	//the outputvariable is the names of the serviceOutput, so they have same size.
		    	return;
		    }
		    
		    for (int i=0; i< lstOutputVariable.size(); i++) {
				strVar = lstOutputVariable.get(i).toString();
				if (!mapVar.containsKey(strVar)){
					continue;
				}
				mapVar.remove(strVar);
				mapVar.put(strVar, lstServiceOutput.get(i)); //change value of correspond variable
			}
		  
		    //change values of the variables
		    var.var = mapVar;
		    break;
		    */
			
		}
		
		Publication pubNext = post.getPublications(mapVar, m_idTaskInstance);
		// Log sender, receiver
		
	
		
		//add payload
		if (var!=null&&!var.var.isEmpty())
			pubNext.setPayload(var);

		//ask agent to sends publication
		agent.sendMessage(ILogicType.PUB,pubNext);
		state=ILogicType.INST_FINISHED;
		
		agent.RemoveInstance(m_idTaskInstance);
		
		
	}
	
	private synchronized void listen() {
		// retry until an element is obtained
		while (true) {
			if (serviceCallReturned) {
				break;
			}
			else {
				try {
					wait();
				} catch (InterruptedException ie) {
				}
			}
		}
	}

	public synchronized void moveOn() {
		serviceCallReturned = true;
		notify();
		
	}

	private void invokeService(SimpleWorkflowTask task) {
		MClientServer server = MClientServer.getInstance();
		moveOn();
		// Do nothing for now.
		// server.triggerService(task);				
	}
	
	
	private List invokeService(List lstInput, int num, String Url, String ActionName){
		List lstOutput = null;
	
		//call Service....
		//use URL as a service name
		
		//generate output list...
		//Here we use a sample
		Double d;
		double value;
		for (int i=0;i< num;i++)
		{
			d = (Double)lstInput.get(0);
			value = d.doubleValue()*2;
			d = new Double(value);
			lstOutput.add(d);
		}
		
		return lstOutput;
	}

	

	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void distory(){
		
		//clear all information int the TaskInstance.
		m_listPubs.clear();
		
		
	}
	
	public String toString()
	{
		String str = null;
		return str;
		
	}
	
	public static void main(String[] args) throws RemoteException{
		/*
		TaskInstance inst = new TaskInstance();
		inst.setHostClientId("1");
		inst.setTaskInstanceId("1");
		//System.out.println("exp: " + TaskInstance.exp(1.0));
		*/
	}

}


