package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.lang.String;

//import brokercore.BrokerCore;


/*
 * For now we suppose that every agent only contains one task.
 * But, in fact, we can at least cope with sequential tasks in one agent.
 */
public class Action implements Serializable, Comparable {
	
	public static final long serialVersionUID = 1;
	
	private int m_intType; // to identify the type of this task: 1<wait>, 1<service invoke>. 
	//private int m_id; //task id of the predefined process;

	//list of the name of input variables
	private List m_listInputParas;  
	
//	list of the name of input variables
	private List m_listOutputParas; 

	
	
	//for webservice call, all parameters should be translated to String.
	private String urlService; 
	private String strActionName;
	

	public Action() {
		super();
		// TODO Auto-generated constructor stub
		m_listInputParas = new Vector();
		m_listOutputParas = new Vector();
	}
	
	public Action(int type) {
		super();
		// TODO Auto-generated constructor stub
		m_intType = type;
		m_listInputParas = new Vector();
		m_listOutputParas = new Vector();
	}
	
	public void setAction(int type, List input, List output, String urlSer, String serName) {
		// TODO Auto-generated constructor stub
		m_intType = type;
		m_listInputParas = input;
		m_listOutputParas = output;
		this.strActionName = serName;
		this.urlService = urlSer;
	}
	
	public List getOutputParas()
	{
		return m_listOutputParas;
		
	}

	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public List getInputParas() {
		return m_listInputParas;
	}

	public void setInputParas(List inputParas) {
		m_listInputParas = inputParas;
	}
	
	public String getStrActionName() {
		return strActionName;
	}

	public void setStrActionName(String strActionName) {
		this.strActionName = strActionName;
	}

	public String getUrlService() {
		return urlService;
	}

	public void setUrlService(String urlService) {
		this.urlService = urlService;
	}


	public int getType() {
		return m_intType;
	}


	public void setType(int type) {
		m_intType = type;
	}


}
