/**
 * 
 */
package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import ca.utoronto.msrg.padres.common.message.*;

import java.util.Iterator;

/**
 *  Rhe objective of this class is to manage postcondition of the agent
 *  It decides ADVs according to the publications
 *     decides which publication should be sent out according to condition expression
 */
public class Postcondition implements Serializable, Comparable {

	public static final long serialVersionUID = 1;

	/*	 for post condition
	 *public final static int AND_SPLIT = 5; 
	 *public final static int OR_SPLIT = 6; 
	 *public final static int AND_OR_SPLIT = 7;
	 * public final static int SEQUENCE = 1;  // has only one publication
	*/
	private String m_strType; // to identify the type of the Postcondition:

    

	//Adv specification:
	//basic information:
	//<Map of Predicate> predicateMap: 
	//	String Process_ID: id of the process ( == current ProcessID)
	//	String Instance_ID: id of the instance ( isPresent) "ATM")
	//	String Sender_Task_ID: id of the previous task (== Current task Id)
	//  String Reciever_Task_ID: id of current task (eq : composite string of received ids split by ";", )
	//	othePredicate information: can describe the incoming Pub schema, or it can be a payload (a List of Object).
	private Advertisement m_objAdv; //to maintains 

/*
	adv = new Advertisement(
	"[class,eq," + ILogicType.TASK_TRIGGER +"]," 
	+ 
	"[" + ILogicType.PROCESS_ID + ",eq," + processId + "],"
	+
	"[" + ILogicType.INSTANCE_ID + ",isPresent,'ATM'" +  "],"
	+
	"[" + ILogicType.SENDER_ID + ",eq," + currentTaskId + "],"
	+
	"[" + ILogicType.RECEIVER_ID + ",eq," + taskIdArray + "]"
	);
*/

	//Publication specification:
	//basic information:
	//<Map of Predicate> predicateMap: 
	//	String Process_ID: id of the process ( current ProcessID)
	//	String Instance_ID: id of the instance ( current instanceID ) 
	//	String Sender__ID: id of the previous task (Current task Id)
	//  String Reciever__ID: id of current task (is a subset of : composite string of received ids split by ";", ) (this is the only difference)
	//	A Payload (a List of Object, know it is equal to output of service invokation, all publication has same payload).

	//***Note that suppose a output dataflow of a Task is same for every cases.
	
	
	//the name of the referenced globle variable
	//this paramter will be used to check which brench should be followed
	private String strReferVariable; 
	
	
	//condition-RECIVER_ID pairs 
	//string for condition value of referenced parameter
	//this one is used for OR_SPLIT  
	//for ADN_SPLIT, it is null
	//    eg: OR_SPLIT 
	//700,  TASK10;TASK9;TASK8
	//else,  TASK22;
	private Map m_mapCondition; 
	
	/**
	 * 
	 */
	public Postcondition() {
		super();
		// TODO Auto-generated constructor stub
		m_mapCondition = new HashMap();
		m_objAdv = null;
	}
	
	public Postcondition(String type, Advertisement adv, String variable, Map condition) {
		super();
		this.m_strType = type;
		this.m_objAdv = adv;
		this.strReferVariable = variable;
		this.m_mapCondition = condition;
	}
	
	/*given the value of outputList, get publication list
	 * 
	 */
	public Publication getPublications(Map objMap, String strInstID)
	{
		Publication pub  = generatePubByAdv();
		Map pubMap = new HashMap();
		pubMap = pub.getPairMap();
		pubMap.remove(ILogicType.INSTANCE_ID);
		pubMap.put(ILogicType.INSTANCE_ID,strInstID);
		if(this.m_strType.equalsIgnoreCase(ILogicType.AND_SPLIT) ||  this.m_strType.equalsIgnoreCase(ILogicType.SEQUENCE))
		{
			pub.setPairMap(pubMap);
			return pub;
		}
			
		String value = null;
		
		//support (if, else if,else) and (swich case)
		//if(this.m_strType == ILogicType.OR_SPLIT )
		if(!objMap.containsKey(strReferVariable))
		{
			return null;
			
		}
		Object obj = objMap.get(strReferVariable);
		if ( m_mapCondition.containsKey(obj))
		{
			value = m_mapCondition.get(obj).toString();
		}
		else
		{
			value = m_mapCondition.get("else").toString();
			
		}
		pubMap.remove(ILogicType.RECEIVER_ID);
		pubMap.put(ILogicType.RECEIVER_ID,value);
		pub.setPairMap(pubMap);
		
		return pub;
	}

	public Publication generatePubByAdv()
	{
		Publication pub= new Publication();
		
		Map advMap = new HashMap();
		Map pubMap = new HashMap();
		advMap = this.m_objAdv.getPredicateMap();

		String attribute = null, value = null;
		Predicate pred = null;
		
		for (Iterator j = (advMap.keySet()).iterator(); j.hasNext();) {
			attribute = (String) j.next();
			pred = (Predicate)advMap.get(attribute);
			value = pred.getValue().toString();
			pubMap.put(attribute, value);
		} 
		pub.setPairMap(pubMap);
	
		return pub;
	}


	
	public String getType() {
		return m_strType;
	}


	public void setType(String type) {
		m_strType = type;
	}


	public Advertisement getAdv() {
		return m_objAdv;
	}

	public void setAdv(Advertisement adv) {
		m_objAdv = adv;
	}

	public Map getCondition() {
		return m_mapCondition;
	}

	public void setCondition(Map condition) {
		m_mapCondition = condition;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString()
	{
		String str = "";
		str += "Type: " + m_strType + "\n";
		str += "Adv: " + this.m_objAdv.toString() + "\n";
		str += "Reference value: " + strReferVariable ; 
		return str;
	}

}
