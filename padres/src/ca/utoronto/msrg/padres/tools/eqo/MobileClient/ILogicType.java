package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

public interface ILogicType {
	
	/*those are used to identify the node type of process
	 * sequence, OR/AND split, OR/AND joint, while, etc.
	 */
	// for both
	public final static String SEQUENCE = "SEQUENCE";
	
	// for precondition
	public final static String OR_JOINT = "OR_JOINT";
	public final static String AND_JOINT = "AND_JOINT";
	public final static String WHILE = "WHILE";

	// for post condition
	public final static String AND_SPLIT = "AND_SPLIT";
	public final static String OR_SPLIT = "OR_SPLIT";
	public final static String AND_OR_SPLIT = "AND_OR_SPLIT";
	
	
	//________________________________________________________________
	/*those are used to indentify particular fields for  pub/sub/adv
	 *  
	 * */
	public final static String PROCESS_ID = "PROCESS_ID";
	public final static String INSTANCE_ID = "INSTANCE_ID";
	public final static String SENDER_ID = "SENDER_TASK_ID";
	public final static String RECEIVER_ID = "RECEIVER_TASK_ID";

	
    //________________________________________________________________
	/*those are used to indentify trigger types
	 *  as class
	 * */
	public final static String TASK_DEPLOYER = "TASK_DEPLOYER";
	public final static String PROCESS_TRIGGER = "PROCESS_TRIGGER";
	public final static String TASK_TRIGGER = "TASK_TRIGGER";
	public final static String PROCESS_END = "PROCESS_END";

	
    //________________________________________________________________
	/*Message type
	 *  as class
	 * */
	public final static int ADV = 1;
	public final static int SUB = 2;
	public final static int PUB = 3;
	public final static int CS = 4;
	public final static int UNSUB = 5;
	
	
	//_______________________________________________________________
	/*state for agent movement
	 * 
	 */
	public final static int  START= 0; //has been created, but does not send sub/adv.
	public final static int INITIALIZED = 1; //has been run, and has sent sub/adv, but do not started to recieve publication.
	public final static int STARTED = 2; //start to recieve notification(publication)
	//public final static int MOVING = 2; //a movement transaction on this agent has been triggered
	//public final static int FINISH = 3; //the new copy of agent has been started in the target executionEngine.
	public final static int PAUSED = 3; //the agent stop to recieve any notificaion, but still cope with running instances
	
	public final static int FINISHING = 4;//the state right after PAUSE,begin to finishing all instances, after finishing,change state to be stopped. 
	//public final static int RESUME = 3; //resume to recieve notificaitons 
	public final static int STOPPED = 5; // stop the agent, the agent can only be stopped after all instances has been finished.


	//_______________________________________________________________
	/*state for Instance
	 * 
	 */
	public final static int INST_BUFFERING = 0; //has been created, but does not start.
	public final static int INST_RUNNING = 1; //start to cope with action
	public final static int INST_FINISHED = 2; //finished execution of action
	

	public final static int ACTION_WAIT = 0;
	public final static int ACTION_SERINVOKE = 1;
	public final static int ACTION_DECISION = 2;
}
