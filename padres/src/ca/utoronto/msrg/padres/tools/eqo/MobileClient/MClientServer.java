package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

//import ca.utoronto.msrg.padres.client.universal.BrokerState;
//import ca.utoronto.msrg.padres.client.universal.ClientException;
import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIAddress;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIServerInterface;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIMessageListenerInterfce;

import ca.utoronto.msrg.padres.common.comm.MessageQueue;
import ca.utoronto.msrg.padres.client.*;
import ca.utoronto.msrg.padres.tools.fabre.SimpleService;
import ca.utoronto.msrg.padres.tools.fabre.SimpleWorkflowTask;

public class MClientServer extends Client
{
	public static final long serialVersionUID = 1;
	private static MClientServer instance = null; // for Singleton	
	private String m_strId;
	private String brokerAddress;
	private String brokerURI;
	MobilityCoordinator mCoordinator;
	
	protected JTextPane notifications;
	protected JTextPane history;
	
	//map of taskid and corresponding agent object
	//task id is used to route publication to agent's queue.
	private Map mcMap;
	
	//counter for agentName
	private int counter;
	
	private ConcurrentHashMap<String, SimpleService> serviceMap = new ConcurrentHashMap<String, SimpleService>();
	
	
	public static JTextArea output;
	JFrame mainFrame = null;

	
	public MClientServer(String id) throws RemoteException, ClientException {
		super(id);
		instance = this;		
		mcMap = new HashMap();
		mCoordinator = new MobilityCoordinator(this);
		mCoordinator.start();
	}
	
	public void setBrokerAddress(String[] args)
	{
		brokerAddress = args[0];
		
	}
	
	public JTextArea getOutputArea()
	{
		return output;
		
	}
	
	public MobilityCoordinator getCoordinator()
	{
		return mCoordinator;
		
	}
	
	public void processMessage(Message msg){
		Publication pub = ((PublicationMessage)msg).getPublication();
		try {
			notify(pub);
		} catch (RemoteException e) {			
			e.printStackTrace();
		}
	}

	public synchronized void removeClient(String clientId)
	{
		mcMap.remove(clientId);
		
	}
	public void notify(Publication pub) throws RemoteException {
	
		System.out.println("recieved message "+ pub.getPubID());
		System.out.println("message is: " + pub + "\n");
		
		log("\n" + "******************************************************"+ "\n" ) ;
		log("recieved message: " + pub.getPubID() +"..."+ "\n" ) ;
		log("message is:" + pub + "\n" );
		log("******************************************************"+ "\n" ) ;
    	   	
		Map pairMap = new HashMap();
		pairMap = pub.getPairMap();		
		
		//if get deployment publication from outside
		/* create an new agent, and assign this task to it
		 * 
		 */

		if(pairMap.get("class").equals(ILogicType.TASK_DEPLOYER))
		{
			handleDeployment(pub);
		}		
		
		else if (pairMap.get("class").equals(MobilityCoordinator.MOVEMENT)){
			handleMovement(pub);
		
		}
	}
	
	public void handleDeployment(Publication pub) throws RemoteException
	{
		/* task information
		 * class,eq,DEPLOYMENT
		 * target,eq,current ExecutionEngine id
		 * then content of taskMap as payload
		 */
	
		TaskContainer task = new TaskContainer();
		//get task information
		task = (TaskContainer) pub.getPayload();
		
		System.out.println("Mobile Client Server" + m_strId 
				+ "get task : process Id = " 
				+ task.m_idProcess + " task Id = " 
				+ task.m_idTask);
	
		log("Mobile Client Server(" + m_strId + ") get task..." + "\n");
		log("Task information : process Id = " 
				+ task.m_idProcess + "; task Id = " 
				+ task.m_idTask + "; PreNode = " 
				+ task.m_typePreNode + "; PostNode = " 
				+ task.m_typePostNode+"\n");
		
		Subscription sub = new Subscription();
		for (int i =0;i<task.m_objPrecondition.size();i++)
		{
			sub = (Subscription)task.m_objPrecondition.get(i);
			log("Precondition (" +i + ") :" + sub +"\n");
		}
		log("Postcondition ..." +"\n");
		log(task.m_objPostcondition +"\n" );
		
		String type  = task.m_objPrecondition.getClass().getName();
		log("Type of task.m_objPrecondition = " + type  +"\n");
    	
		log ("Service(capacity: " + task.capacity + "): to be invoked: " + task.serviceName);
    	
		//create an new MClient
		MClient mc = createClient(task);
		mc.initialize();
		//client.setState(ILogicType.STARTED);
		mc.startClient();
		
	}
	
	
	public MClient createClient(TaskContainer task) throws RemoteException
	{
		//String id = m_strId + this.counter++;
		//String id = m_strId + "-" + task.m_idTask;
		String id = task.m_idTask;
		log("_________________________________________________"+ "\n" ) ;
		log("creating client: " +id +"...\n");
		MClient client = null;
		try {
			client = new MClient(id);		
			//client.waitUntilStarted();
			client.setTask(task);
			client.setClientId(id);
			brokerURI = client.connect2Broker(this.brokerAddress);
			//client.initialize();
			//client.setState(ILogicType.STARTED);
			//client.startClient();
			mcMap.put(id,client);
			
			if (!this.serviceMap.containsKey(task.serviceName)) {
				this.serviceMap.put(task.serviceName, new SimpleService(task.serviceName, task.capacity, id));
				log("Created service interface: " + task.serviceName);
			}
			//client.setService(task.serviceName, task.capacity);
			
			/*
			log("Sending advertisement for service invocation info persistence");
			Advertisement adv = new Advertisement();
			adv.addPredicate("class", new Predicate("eq", "rohEvent"));
			adv.addPredicate("be", new Predicate("isPresent", "any"));
			adv.addPredicate("type", new Predicate("isPresent", "any"));
			client.advertisePersistence(adv);
			*/
		
		
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return client;
	}
	
	
	/*the id of the client is unified
	 * 
	 */
	public MClient acceptClient(String clientId, TaskContainer task) throws RemoteException
	{
		log("_________________________________________________"+ "\n" ) ;
		log("Accept client: " +clientId +"...\n");
		MClient client = null;
		try {
			
			client = new MClient(clientId + "_v1");			
			client.setTask(task);	
			client.setClientId(clientId);
			brokerURI = client.connect2Broker(this.brokerAddress);			
			log("brokerURI: " + brokerURI + "\n");
			client.initialize();
			mcMap.put(clientId, client);
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	public void handleMovement(Publication pub)
	{
		//invoke Coordinator to cope with movement
		PublicationMessage pubMsg = new PublicationMessage(pub);
		this.mCoordinator.addMessage(pubMsg);
		
	}
	
	protected void printClientAction(String outString, Color txtColor) {
		try {
			Document notificationText = history.getDocument();
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);			
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	protected void printNotification(String outString, Color txtColor) {
		try {
			Document notificationText = notifications.getDocument();
			System.out.println("notificationTExt: " + notificationText.toString());
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);
		} catch (BadLocationException e) {
			//printClientAction(e.getMessage(), Color.RED);
		}
	}
	
	/*connect to the broker idenfied by args
	 *  build rmiconnection
	 */
	public void Connect2Broker(String[] args) throws RemoteException
	{
		String brokerURI = ""; 
		brokerURI = args[0];
		/*
		if (args.length == 1) {
			brokerURI = "rmi://" + args[0] + "/RMIBindingServer";
		} else {
			brokerURI = "rmi://" + args[0] + ":" + args[1] + "/" + args[3];
		}
		*/	
		if (brokerURI != null) {
			System.out.println("brokerURI: " + brokerURI);
			try {
				//printClientAction("Connecting to broker: " + brokerURI + " ...", Color.BLACK);
				BrokerState brokerState = connect(brokerURI);
				String result = String.format("Connected to %s", brokerState.getBrokerAddress().getNodeURI());
				System.out.println("result: " + result);
				//printClientAction(result, Color.GREEN);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				//printClientAction(e.getMessage(), Color.RED);
			}
		}		
	}
	
	
	
	//for MobileCoordinator using pub/sub messaging system
	public void sendMessage(int type, Object obj)
	{
		
		try{

			switch(type){
				case ILogicType.CS:
					//sent CS 
					subscribeCS((CompositeSubscription)obj);
					log("MobileCoordinator" +  ": trigger a CS... ") ;
			    	log((CompositeSubscription)obj + "\n");
					break;
				case ILogicType.SUB:
					//sent sub
					subscribe((Subscription)obj);
					log("MobileCoordinator" + ": trigger a Sub... ") ;
			    	log((Subscription)obj + "\n");
					break;
				case ILogicType.PUB:
					//set pub
					publish((Publication)obj);
					log("MobileCoordinator" + ": trigger a Pub... ") ;
			    	log((Publication)obj + "\n");
					break;
				case ILogicType.ADV:
					//set pub
					advertise((Advertisement)obj);
					log("MobileCoordinator" + ": trigger a Adv... ") ;
			    	log((Advertisement)obj + "\n");
					break;
				}
		}
		catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public JFrame createJFrame() {
		final JFrame frame = new JFrame("Mobile Client Server");
				
		final JMenuBar mb = new JMenuBar();
		final JMenu mainMenu = new JMenu("Main");
			
		JMenuItem exitItem = new JMenuItem("Exit");	
		mainMenu.add(exitItem);	
		mb.add(mainMenu);
		

		frame.getRootPane().setJMenuBar(mb);
		
		exitItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame.dispose();
						try {
							//m_rmiConnection.disconnect();
							disconnect(brokerURI);
						} catch (ClientException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						System.exit(0);
					}
				});
					

		JPanel contents = new JPanel();
		contents.setLayout(new BorderLayout());
		
		output = new JTextArea(20, 50);
		output.setEditable(false);
		JScrollPane outputpane = new JScrollPane(output);
		contents.add(outputpane, BorderLayout.NORTH);

		frame.setContentPane(contents);
		
		JPanel notificationBox = new JPanel(new BorderLayout());
		notificationBox.setBorder(BorderFactory.createTitledBorder("Broker Notifications"));
		notifications = new JTextPane();
		notifications.setPreferredSize(new Dimension(200, 200));
		notifications.setEditable(false);
		JScrollPane notificationPane = new JScrollPane(notifications);
		notificationBox.add(notificationPane);
		contents.add(notificationBox);
				
		mainFrame = frame;
				
		return frame;
	}
	
	private void initialize() throws ParseException{
		
		//subscribe corresponding task deployment publication
		Subscription sub;
		try {
			sub = MessageFactory.createSubscriptionFromString(
					"[class,eq," + ILogicType.TASK_DEPLOYER +"]," + 
					"[" + "ENGINE_NAME" + ",eq," + m_strId + "]");
		
			System.out.println(m_strId + " subscribe all TASK_DEPLOYER message: " + sub + "\n");
			System.out.println("======================================");
	    
			log(m_strId + " subscribe all TASK_DEPLOYER message: " + sub + "\n");
			log("======================================\n");

			// send out the subscription with AGENT NAME
			subscribe(sub);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		//initialize for mobile coordinator
		mCoordinator.initialize();
		
		/*
		Subscription subTrigger = new Subscription(

				"[class," +",eq,"+ MobileCoordinator.MOVEMENT +"]," 
				+ 
				"[" + MobileCoordinator.MESSAGETYPE +",eq,"+  MobileCoordinator.M_Trigger + "],"
				+ 
				"[" + MobileCoordinator.SOURCESERVER +",isPresent,"+ "ATM" + "],"
				+
				"[" + MobileCoordinator.TARGETSERVER +",isPresent,"+ "ATM" +  "],"
				+
				"[" + MobileCoordinator.MClientID +",isPresent,"+ "ATM" +  "]"
				);
		try{
			m_rmiConnection.subscribe(subTrigger);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		*/
		
		/*
		//subscribe corresponding task trigger publication: this one is used for debugging, can be deleted when release
		Subscription sub1 = new Subscription(
				"[class,eq," + ILogicType.TASK_TRIGGER +"]," + 
				"[" + ILogicType.PROCESS_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.INSTANCE_ID + ",isPresent," +"ATM" +  "],"
				+
				"[" + ILogicType.SENDER_ID + ",isPresent," + "ATM" + "],"
				+
				"[" + ILogicType.RECEIVER_ID + ",isPresent," + "ATM" + "]"
				);
		System.out.println(m_strId + " subscribe all TASK_Trigger message: " + sub1 + "\n");
	    System.out.println("======================================");

	    log(m_strId + " subscribe all TASK_Trigger message: " + sub1 + "\n");
	    log("======================================\n");
		// send out the subscription with AGENT NAME
		try{
			m_rmiConnection.subscribe(sub1);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		*/
		
	}


	public static synchronized MClientServer getInstance() {

		if (instance == null) {
			System.out.println("ERROR: ExecutionEngine not instantiated");
			System.exit(1);
		}
		return instance;
	}

	
	
	//*******************************************************************************
	//Client management operations
	
	public boolean deleteClient(String id)
	{
		
		MClient mc = (MClient) mcMap.get(id);
		if(mc.getState()!=ILogicType.STOPPED){
			System.out.println("the MClient is not stopped, it can not be deleted");
			return false;
		}
		mc = null;
		mcMap.remove(id);
		return true;
		
	}
	
	public boolean changeState(String id, int state)
	{
		MClient agent = (MClient) mcMap.get(id);
		switch(state)
		{
			case ILogicType.PAUSED:
				break;
			case ILogicType.STARTED:
				break;
			case ILogicType.STOPPED:
				Object stopLocker = new Object();
				agent.stopClientSyn(stopLocker);
				synchronized (stopLocker){
					while (agent.getState() != ILogicType.STOPPED)
					{ 
						try{
							stopLocker.wait();
						}
						catch(InterruptedException e){
							System.out.println("'n error in lishener/n");
						}
					}
				}
				break;
			
		}

		return true;
	}
	

	public String getBrokerId() {
		return this.brokerURI;
	}
	
	public  MClient getClient(String id)
	{
	
		MClient mc = (MClient)mcMap.get(id); 
		return mc;
		
	}

	public void exitClient() {
		try {
			shutdown();
		} catch (ClientException e) {
			//printClientAction("Error in shutting down client: " + e.getMessage(), Color.RED);
		}
	}
	
	
	public String getId() {
		return m_strId;
	}


	public void setId(String id) {
		m_strId = id;
	}

	public void log(String s) {
		System.out.print(s);
		if (output != null)
			output.append(s);
	}

	public void triggerService(SimpleWorkflowTask task) throws ParseException {
		if (!this.serviceMap.containsKey(task.getServiceName())) {
			System.out.println("Service (" + task.getServiceName() + ") does not exist");
		}
		else {
			this.serviceMap.get(task.getServiceName()).triggerService(task);
		}
	}


	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws RemoteException{
		if (args.length < 2) {
			System.out.println("Usage: rmi://localhost:1099/BrokerA ID");
			System.exit(1);
		}


		boolean showGUI = true;

		try {
			final MClientServer server = new MClientServer(args[1]);
			
			server.setId(args[1]);
	
			if (showGUI)
			{
				JFrame.setDefaultLookAndFeelDecorated(true);
				JFrame frame = server.createJFrame();
				
				frame.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						server.exitClient();
						System.exit(0);
					}
				});
				frame.pack();
				frame.setVisible(true);
			}
			
			server.Connect2Broker(args);
			server.setBrokerAddress(args);
			server.initialize();
			
		} catch (Exception e) {
			System.out.println("Unable to construct new engine" + e);
		}
	}
}
