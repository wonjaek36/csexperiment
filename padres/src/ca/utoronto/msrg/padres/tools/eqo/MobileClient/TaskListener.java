package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.common.comm.MessageQueue;




public class TaskListener extends Thread{
	
	
	private boolean started = false;
	private int state = -1;
	private Object started_lock = new Object();
	private MessageQueue inQueue;
	
	private TaskContainer task = null;
	
	//private String idHostClient;
			
	//to maintain runing instances
	//once we get a new Pub/CP, a new instance is generated and add to this map as (instanceId,instance obj).
	//after that instance finish its task, it will be deleted from this map.
	//private Map m_mapTaskInst; 
	
	private int m_intCounter = 0;
	
	private MClient mcHost;
	
	
	public TaskListener(MClient mc)
	{
		//m_mapTaskInst = new HashMap();
		//inQueue = new MessageQueue();
		mcHost = mc;
			
	}
    public void waitUntilStarted() throws InterruptedException {
        synchronized (started_lock) {
        	while (started!=true) {
	            started_lock.wait();
        	}
    	}
    }
    
    public MessageQueue getQueue()
    {
    	return inQueue;
    	
    }
       
    public void setQueue(MessageQueue msq)
    {
    	inQueue = msq;
    	
    }
    /*
    public void setHostClientId(String id)
    {
    	idHostClient = id;
    	
    }
    public Map getTaskInst()
    {
    	return m_mapTaskInst;
    	
    }
    
    
    public void setInstMap(Map instMap)
    {
    	this.m_mapTaskInst = instMap;
    	
    }
    */
    
    /*
    public void AddMessage(Publication pub)
    {
    	 inQueue.add(pub);
    	
    }
    */
    
    public void setTask(TaskContainer t)
    {
    	task = t;
    	
    }
    public TaskContainer getTask()
    {
    	return task;
    	
    }
    
    /*
    public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	*/
    
    
    public void run()
	{
    	//first time to start the thread
    	if( started == false)
    	{
			synchronized (started_lock) {
				started=true;
				started_lock.notifyAll();
			}
    	}
    	listen(); 
	
	}


	public void listen()  {  
		// TODO Auto-generated method stub
		
		
		//if the pub' class type is process trigger, then generate a instance number
		//else get instance Id from the pub

		/*
		if (inQueue.isEmpty()){
			return;
		}
		*/
		while(true)
		{
	        PublicationMessage pubMsg = (PublicationMessage) inQueue.blockingRemove();
	        Publication pub = pubMsg.getPublication();
			String instId = null;
			Map pairMap = new HashMap();
			pairMap = pub.getPairMap();
			
			/*
			if(task.m_idTask.equalsIgnoreCase("TASK_4"))
			{
				int a = 1;
				a=a++;
				
			}
			*/
			
			if(pairMap.get("class").equals(ILogicType.PROCESS_TRIGGER)) 			{
				instId= mcHost.getClientId()+ "INST" + m_intCounter++;
			}else{
				instId = (String)pairMap.get("INSTANCE_ID");
			}
			TaskInstance inst = null;
			// we need to guarantee that only publications with same instance number can be combined together.
					
			if (!task.m_typePreNode.equalsIgnoreCase(ILogicType.AND_JOINT)){
				//for any single incoming publication, generate an instance using the task information; add the instance into runnig instance list
				
				inst = new TaskInstance();
				inst.setHostClientId(mcHost.getClientId());
					inst.setTaskInstanceId(instId);
				
				inst.setPubNum(1);
				//m_mapTaskInst.put(instId, inst);
				mcHost.AddInstance(instId, inst);
				inst.addPub(pub);
	
			}else{
				//for And_joint, the precondition must be at least two subs
				
				// iterator the instance map to see if the incoming pub contains a new instance nubmer
				// if yes: gerate an instance; add the instance into runnig instance map
				// else: add this publication to an exist instance.
				
				
				if (mcHost.ContainsInstance(instId)){
					//inst = (TaskInstance)m_mapTaskInst.get(instId);
					inst = mcHost.GetInstance(instId);
					inst.addPub(pub);
				}else{
					
					int subMessageNum = task.m_objPrecondition.size();
					inst = new TaskInstance(subMessageNum, instId, mcHost.getClientId());
					//m_mapTaskInst.put(instId, inst);
					mcHost.AddInstance(instId, inst);
					inst.addPub(pub);
				}
			}
		}

	}
}
