package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;
import ca.utoronto.msrg.padres.common.comm.MessageQueue;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


public class MobilityCoordinator extends Thread{
	
	private boolean started = false;
	private Object started_lock = new Object();
	
	public final static String MOVEMENT = "MOVEMENT";
	public final static String SOURCESERVER = "SOURCESERVER";
	public final static String TARGETSERVER = "TARGETSERVER";
	
	//the Id of this client should be unified all through the network? 
	public final static String MClientID = "MClientID";   
	
	public final static String MESSAGETYPE = "MESSAGETYPE";
	public final static int M_Trigger = 0;
	public final static int M_Negotiate = 1;
	public final static int M_Approved = 2;
	public final static int M_Rejected = 3;
	public final static int M_StateTrans = 4;
	public final static int M_OldRTsCleanUp = 5;
	
	//state for sender
	public final static String ST_NegoSent = "SendNegotiation";
	public final static String ST_ReplyReceived = "ReceivedReply";
	public final static String ST_StateSent = "SendState";
	
	public final static String ST_NegoReceived = "ReceiveNegotiation";
	public final static String ST_Rejected = "RejectNegotiation";
	public final static String ST_Approved = "ApproveNegotiation";
	public final static String ST_stateReceived = "ReceiveState";
	
	
	private MessageQueue inQueue;
	//This one should maintain the MClients who are in Mobile transaction. 
	//should maintain the transaction State.
	private Map mcsInMoveTX;  // map of clientId and current TX state 
	private MClientServer MServer;
	

	public MobilityCoordinator (MClientServer mSer)
	{
		MServer = mSer;
		mcsInMoveTX = new HashMap();
		inQueue = new MessageQueue();
			
	}
	
	public void addMessage(PublicationMessage pub)
	{
		inQueue.add(pub);
		
	}
	
    public void waitUntilStarted() throws InterruptedException {
        synchronized (started_lock) {
        	while (started != true) {
	            started_lock.wait();
        	}
    	}
    }
	
    public boolean isInMovingTX(String clientId){
    	if(this.mcsInMoveTX.containsKey(clientId)){
    		return true;}
    	else{
    		return false;}
    	
    }
    
	public void initialize() throws ParseException
	{
		//This one should:
		
		//***For Sender Triggering Mode***
		//For Sender
		//	Adv: SendNegotiation, and SendState. (receiver is present)
		//	Sub: ReceiveNegotiation. (sender is present, receiver is current broker)
		//FOR Receiver
		//	Sub: SendNegotiation and SendState
		//	Adv: ReceiveNegotiation
		Advertisement adv = new Advertisement(
				"[class,eq,"+ MobilityCoordinator.MOVEMENT +"]," 
				+ 
				"[" + MobilityCoordinator.MESSAGETYPE +",>=,"+  "0" + "],"
				+ 
				"[" + MobilityCoordinator.SOURCESERVER +",eq,"+ MServer.getId() + "],"
				+
				"[" + MobilityCoordinator.TARGETSERVER +",isPresent,"+ "ATM" +  "],"
				+
				"[" + MobilityCoordinator.MClientID +",isPresent,"+ "ATM" +  "]"
				);
		MServer.sendMessage(ILogicType.ADV, adv);		

		Subscription sub = MessageFactory.createSubscriptionFromString(
				"[class,eq,"+ MobilityCoordinator.MOVEMENT +"]," 
				+ 
				"[" + MobilityCoordinator.MESSAGETYPE +",>,"+  "0" + "],"
				+
				"[" + MobilityCoordinator.SOURCESERVER +",isPresent,"+ "ATM" +  "],"
				+ 
				"[" + MobilityCoordinator.TARGETSERVER +",eq,"+ MServer.getId() + "],"
				+
				"[" + MobilityCoordinator.MClientID +",isPresent,"+ "ATM" +  "]"
				);
		MServer.sendMessage(ILogicType.SUB, sub);
		
		//subscribe the trigger of the movement of particular client
		Subscription subTrigger = MessageFactory.createSubscriptionFromString(
				"[class,eq,"+ MobilityCoordinator.MOVEMENT +"]," 
				+ 
				"[" + MobilityCoordinator.MESSAGETYPE +",=,"+  M_Trigger + "],"
				+ 
				"[" + MobilityCoordinator.SOURCESERVER +",eq,"+ MServer.getId() + "],"
				+
				"[" + MobilityCoordinator.TARGETSERVER +",isPresent,"+ "ATM" +  "],"
				+
				"[" + MobilityCoordinator.MClientID +",isPresent,"+ "ATM" +  "]"
				);
		MServer.sendMessage(ILogicType.SUB, subTrigger);
		
		
		//***For Receiver triggering Mode***
		//For Receiver
		//	Adv: NeedMClient
		//For Sender
		//	Sub: NeedMClient
		
	}
	
	
	 public synchronized final void run()
	{
		 
    	//first time to start the thread
		if( started == false)
    	{
			synchronized (started_lock) {
				started=true;
				started_lock.notifyAll();
			}
    	}		

    	listen();      	
    	
	}

	public void listen()
	{
		while(true)
		{
			PublicationMessage pubMsg = (PublicationMessage) inQueue.blockingRemove();
			Publication pub = pubMsg.getPublication();
			Map pairMap = new HashMap();
			pairMap = pub.getPairMap();
			String strInt = pairMap.get(MobilityCoordinator.MESSAGETYPE).toString();
			Integer Int = new Integer(strInt);
			int messageType = Int.intValue();
			switch(messageType)
			{
				case M_Trigger:
					triggerMovement(pub);
					break;
				case M_Negotiate:
					receiveNegotiation(pub);
					break;
				case M_Approved: 
					onApproved(pub);
					break;
				case M_Rejected:
					this.onRejected(pub);
					break;
				case M_StateTrans:
					receiveStateInfo(pub);
					break;
			}
		}
	}	
	
	// Trigger movement by incoming message 
	public void triggerMovement(Publication pub)
	{
		Map pairMap = new HashMap();
		pairMap = pub.getPairMap();
		String clientId = pairMap.get(MClientID).toString();
		String targetServerId = pairMap.get(TARGETSERVER).toString();
		this.sendNegotiation(clientId, targetServerId);
	}	
	
	//Operation for sender
	private boolean sendNegotiation(String clientId, String targetServerId)
	{
		MClient mc = this.MServer.getClient(clientId);
		if(mcsInMoveTX.containsKey(clientId))  	{  // If the client ID is current moving
			return false;
		}

		// Pause this client, if it is not paused
		mc.pauseClient();
		// Adds current client to moveTX list
		
		mc.setTargetServerId(targetServerId); // Let the MClient know the target server id
		
		mcsInMoveTX.put(clientId,MobilityCoordinator.ST_NegoSent);	

		TaskContainer t = mc.getTaskWithMsgIds();
		// Generates sendNegotiation  publication
		Publication pub = generateMessage(M_Negotiate, targetServerId, mc.getClientId());
		pub.setPayload(t);

		// Send this publication using MServerConnection
		MServer.sendMessage(ILogicType.PUB, pub);
		return true; 
	}
	
	/* If the receiver refuse to accept the client
	 * shut down current movement transaction
	 */
	private void onRejected(Publication pub)
	{
		Map pairMap = pub.getPairMap();
		String clientId = pairMap.get(MClientID).toString();
		MClient mc = this.MServer.getClient(clientId);
		mcsInMoveTX.remove(clientId);
		mc.resumeClient();
	}
	
	private void onApproved(Publication pub)
	{
		Map pairMap = pub.getPairMap();
		String clientId = pairMap.get(MClientID).toString();
		String targetServer = pairMap.get(SOURCESERVER).toString();
		MClient mc = this.MServer.getClient(clientId);
		sendStateInfo(targetServer, clientId);
		mc.stopClientASyn();		
	}
	
	private void sendStateInfo(String targetServer, String clientId)
	{
		MClient mc = this.MServer.getClient(clientId);
		ClientState cs =  mc.serializeState();
		Publication pubSent = this.generateMessage(this.M_StateTrans,targetServer,clientId);
		pubSent.setPayload(cs);
		MServer.sendMessage(ILogicType.PUB, pubSent);
		mcsInMoveTX.remove(clientId);
		mcsInMoveTX.put(clientId,ST_StateSent);
		
	}
	
	/*
	 * Send a message to clean the routing table inforamtion for the old adv
	 * Only cope with adv cleanup
	 */
	public boolean cleanUpOldRTs(String clientId)
	{
		if (!mcsInMoveTX.containsKey(clientId)
			||
			!mcsInMoveTX.get(clientId).toString().equalsIgnoreCase(ST_StateSent)) 
		{
			return false;
		}
		MClient mc = MServer.getClient(clientId);
		List advList = new Vector();
		String advId = mc.getAdvMsgId();
		System.out.println("advId: " + advId);
		advList.add(advId);
		String targetServer = mc.getTargetServerId();
		Publication pub = generateMessage(M_OldRTsCleanUp, targetServer, clientId);
		ReconfigMsgs rMsg = new ReconfigMsgs();
		rMsg.advList = advList;
		pub.setPayload(rMsg);
		MServer.sendMessage(ILogicType.PUB, pub);
		
		//since the client has been successfully moved to the target. remove it from the moving Map
		this.mcsInMoveTX.remove(clientId);
		return true;
		
	}
	
	/*
	 * Receive and process negotiation request
	 * In this version, always approve the acceptance of tasks
	 * Verify what ReconfgMsg does: Is it really necessary? Young, October 17, 2012
	 */
	private void receiveNegotiation(Publication pub)
	{		
		TaskContainer taskContainer = (TaskContainer)pub.getPayload();
		Map pairMap = pub.getPairMap();
		MClient mc = null;
		String clientId = pairMap.get(MClientID).toString();
		String mcHomeServer = pairMap.get(SOURCESERVER).toString();
		try	{
			mc = this.MServer.acceptClient(clientId, taskContainer);
		}
		catch(RemoteException e)
		{
			sendRejected(mcHomeServer, clientId);			
		}
		
		// Added for advertisement/sub reconfiguration
		ReconfigMsgs rMsgs = new ReconfigMsgs();
		rMsgs.advList = taskContainer.advList;
		rMsgs.subList = taskContainer.subList;
		
		if (taskContainer.subList != null && !taskContainer.subList.isEmpty()){
			for (int i = 0; i< taskContainer.subList.size(); i++){				
				rMsgs.subMap.put(taskContainer.subList.get(i),taskContainer.m_objPrecondition.get(i));
			}
		}
		
		sendApproved(mcHomeServer, clientId, rMsgs);
		mc.initializeNoAdv(taskContainer.advList.get(0).toString()); //start to buffer incoming messages
	}
	
	private void sendRejected(String targetServer, String clientId)
	{
		Publication pub = generateMessage(M_Rejected, targetServer, clientId);
		MServer.sendMessage(ILogicType.PUB, pub);
		//Since the server refuse to accept the client, the state do not need to be maintained		
	}
	
	private void sendApproved(String targetServer, String clientId, ReconfigMsgs rMsgs)
	{
		Publication pub = generateMessage(M_Approved, targetServer, clientId);
		String brokerId = MServer.getBrokerId();
		// New client ID should not overlap with the old one especially (Young, October 17, 2012)
		MessageDestination md = MessageDestination.formatClientDestination(clientId, brokerId);
		rMsgs.setLastHop(md);
		
		//Add advlist and subList information as payload, for adv/subreconfiguration
		if(!rMsgs.isEmpty())
		{
			pub.setPayload(rMsgs);
		}
		
		// Add advlist and subList information as payload, for adv/subreconfiguration
		// String.format("%s-%s", brokerID, clientID)
		MServer.sendMessage(ILogicType.PUB, pub);		
		// Add this client to TX map
		mcsInMoveTX.put(clientId,ST_Approved);	
		
	}
	private void receiveStateInfo(Publication pub)
	{
		Map pairMap = pub.getPairMap();
		String clientId = pairMap.get(MClientID).toString();
		MClient mc = MServer.getClient(clientId);
		ClientState cs =  (ClientState)pub.getPayload();
		mc.acceptStateInformation(cs);		
		mc.startClient();
		mcsInMoveTX.remove(clientId); //TX finished	
	}	

	private Publication generateMessage(int messageType, String targetSer, String clientId)
	{
		Publication pub = null;
		try {
			
			pub = MessageFactory.createPublicationFromString (
					"[class," + MobilityCoordinator.MOVEMENT +"]," 
					+ 
					"[" + MobilityCoordinator.MESSAGETYPE + "," + messageType+ "],"
					+ 
					"[" + MobilityCoordinator.SOURCESERVER +","+ MServer.getId() + "],"
					+
					"[" + MobilityCoordinator.TARGETSERVER +","+ targetSer +  "],"
					+
					"[" + MobilityCoordinator.MClientID +","+ clientId +  "]"
					);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pub;
		
				
	}
}
