package ca.utoronto.msrg.padres.tools.eqo.MobileClient;

import java.awt.Color;
import java.io.IOException;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import ca.utoronto.msrg.padres.common.message.*;
import ca.utoronto.msrg.padres.common.message.parser.MessageFactory;
import ca.utoronto.msrg.padres.common.message.parser.ParseException;

import ca.utoronto.msrg.padres.common.comm.CommSystem.HostType;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIAddress;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIServerInterface;
import ca.utoronto.msrg.padres.common.comm.rmi.RMIMessageListenerInterfce;
import ca.utoronto.msrg.padres.client.*;



import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import ca.utoronto.msrg.padres.common.comm.MessageQueue;
import ca.utoronto.msrg.padres.tools.fabre.SimpleService;

 
/////////////////
// This class is utilized to maintain agent information, including:
//	   Precondition of the task. <Sub or CS>
//     Task information, <invoke type, input parameters, output parameters>
//	   Postcondition: uses <ADVs/Publications> to describe the actions of the agent after finishing execution of the task.
////////////

public class MClient extends Client
{
	public static final long serialVersionUID = 1;
	
	private String id;   //id of this agent	
	private TaskListener listener = null; //this listener takes in charge of coping with messages.
	private RMIServerInterface m_rmiConnection;
	private MessageQueue inQueue;
	private int state = -1;
	private Map mapTaskInst;
	
	private List lstSubId = null;
	private String strAdvId = null;
	
	
	private String targetServerId = null; //will be a non empty string when moving
	
	protected JTextPane notifications;
	protected JTextPane history;
	
	private SimpleService service;
	
	
	public MClient(String id)  throws RemoteException, ClientException {
		super(id);		
		listener  = new TaskListener(this);
		inQueue = new MessageQueue();
		mapTaskInst = new HashMap();
		lstSubId = new Vector();
		listener.setQueue(inQueue); //share the inQueue
	}
	
	public List getSubMsgIds()
	{
		return lstSubId;
		
	}
	public String getAdvMsgId()
	{
		return strAdvId;
		
	}
	
	public void waitUntilStarted()  {

		listener.start();
		try {
			listener.waitUntilStarted();
		} catch (InterruptedException e) {
			System.out.println("ERROR: TaskListener failed to start: " + e);
			System.exit(1);
		}

	}

	public void notify(Publication pub) throws RemoteException {
	
		Map pairMap = new HashMap();
		pairMap = pub.getPairMap();
		MClientServer server = MClientServer.getInstance();
		//JTextArea output = server.getOutputArea();
		if (pairMap.get("class").equals(ILogicType.TASK_TRIGGER)){

			server.log("_____________________________________________________" + "\n");
			server.log(server.getId()+ " notify Agent(" + this.id + ") get task trigger... ");
			server.log(pub + "\n");
			server.log("_____________________________________________________" + "\n");
	    	//listener.AddMessage(pub);
			PublicationMessage pubMsg = new PublicationMessage(pub);
			inQueue.add(pubMsg);
		    
		}
	}
	
	public void processMessage(Message msg){
		Publication pub = ((PublicationMessage)msg).getPublication();
		Map pairMap = new HashMap();
		pairMap = pub.getPairMap();
		MClientServer server = MClientServer.getInstance();
		//JTextArea output = server.getOutputArea();
		if (pairMap.get("class").equals(ILogicType.TASK_TRIGGER)){

			server.log("_____________________________________________________" + "\n");
			server.log(server.getId()+ " processMsg Agent(" + this.id + ") get task trigger... ");
			server.log(pub + "\n");
			server.log("_____________________________________________________" + "\n");
	    	//listener.AddMessage(pub);
			PublicationMessage pubMsg = new PublicationMessage(pub);
			inQueue.add(pubMsg);
		    
		}
	}


	public void setTask(TaskContainer t){
		listener.setTask(t);
	}
	
	public TaskContainer getTask(){
		return listener.getTask();		
	}
	
	public TaskContainer getTaskWithMsgIds(){
		TaskContainer tsk = listener.getTask();
		tsk.advList = new Vector();
		tsk.advList.add(strAdvId);
		tsk.subList = lstSubId;
		return tsk;
	}
	
	//****************************************************************************
	//those methods is used to support retrieving state information of the client.
	public MessageQueue getQueue()
	{		
		return this.inQueue;
	}
	public Map getTaskInst()
	{
		return this.mapTaskInst;
		
	}
	//
	
	//get instances which are buffering
	public synchronized Map getBufferingInst()
	{
		Map BufferingInst = new HashMap();
		Map currentInst = new HashMap();
		currentInst = mapTaskInst;
		
		String attribute;
		TaskInstance inst;
		for (Iterator j = (currentInst.keySet()).iterator(); j.hasNext();) {
			attribute = (String) j.next();
			inst = (TaskInstance)currentInst.get(attribute);
			if(inst.getState()==ILogicType.INST_BUFFERING)
			{
				BufferingInst.put(attribute,inst);
			}
		} 
		return BufferingInst;
		
	}
	
	//get instances which are buffering, and remove those instances from current client
	public synchronized Map withdrawBufferingInst()
	{
		if(mapTaskInst.isEmpty())
		{
			return mapTaskInst;
		}
		Map BufferingInst = new HashMap();
		String attribute;
		TaskInstance inst;
		List removeId = new Vector();
		for (Iterator j = (mapTaskInst.keySet()).iterator(); j.hasNext();) {
			attribute = (String) j.next();
			inst = (TaskInstance)mapTaskInst.get(attribute);
			if(inst.getState()==ILogicType.INST_BUFFERING)
			{
				removeId.add(attribute);
			}
		} 
		
		for(int i=0;i<removeId.size();i++)
		{
			String instId = removeId.get(i).toString();			
			BufferingInst.put(instId,(TaskInstance)mapTaskInst.get(instId));
			mapTaskInst.remove(instId);
		}
		return BufferingInst;
		
	}
	
	public MessageQueue withdrawQueue()
	{
		MessageQueue newQueue = new MessageQueue();
		newQueue = inQueue;
		while (inQueue.size() > 0) {
			inQueue.blockingRemove();
		}
		//inQueue.clear();
		return newQueue;
	}
	
	//those methods is used to support retrieving state information of the client.
	//****************************************************************************
	
	
	
	/*
	 *  (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	
	protected void printClientAction(String outString, Color txtColor) {
		try {
			Document notificationText = history.getDocument();
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);			
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	protected void printNotification(String outString, Color txtColor) {
		try {
			Document notificationText = notifications.getDocument();
			SimpleAttributeSet attrSet = new SimpleAttributeSet();
			StyleConstants.setForeground(attrSet, txtColor);
			notificationText.insertString(notificationText.getLength(), outString + "\n", attrSet);
		} catch (BadLocationException e) {
			printClientAction(e.getMessage(), Color.RED);
		}
	}
	
	protected String connect2Broker(String address) {
		String brokerURI = ""; 
		brokerURI = address;
		/*
		if (args.length == 1) {
			brokerURI = "rmi://" + args[0] + "/RMIBindingServer";
		} else {
			brokerURI = "rmi://" + args[0] + ":" + args[1] + "/" + args[3];
		}	
		*/	
		if (brokerURI != null) {
			try {
				//printClientAction("Connecting to broker: " + brokerURI + " ...", Color.BLACK);
				System.out.println("Connecting to broker: " + brokerURI + " ...");
				BrokerState brokerState = connect(brokerURI);
				String result = String.format("Connected to %s", brokerState.getBrokerAddress().getNodeURI());
				System.out.println("result: " + result);
				//printClientAction(result, Color.GREEN);
			} catch (ClientException e) {
				System.out.println("exception: " + e.getMessage());
				//printClientAction(e.getMessage(), Color.RED);
			}
		}
		return brokerURI;
	}
	/*
	public void Connect2Broker(String[] args) throws RemoteException
	{
		RMIBindingServer rmiServer;

		try {
			if (args.length == 1) {
				rmiServer =
					(RMIBindingServer) Naming.lookup(
						"rmi://" + args[0] + "/RMIBindingServer");
			} else {
				rmiServer =
					(RMIBindingServer) Naming.lookup(
						"rmi://"
							+ args[0]
							+ ":"
							+ args[1]
							+ "/RMIBindingServer");
			}
			if (rmiServer != null)
				m_rmiConnection = rmiServer.getConnection();
		} catch (Exception e) {
			System.out.print("remote broker is not exist");
		}

		if (m_rmiConnection == null) {
			System.out.print("connetion to remote broker is not valid");
		}

		try {
			m_rmiConnection.addMessageListener(this);

		} catch (RemoteException e) {
			System.out.print("fail to add message listener");
		}
		
	}
*/


	public String sendMessage(int type, Object obj)
	{
		MClientServer server = MClientServer.getInstance();
		String strMsgId = null;
		try{
			switch(type){
				case ILogicType.CS:
					//sent CS 
					CompositeSubscriptionMessage csMsg = subscribeCS((CompositeSubscription)obj);
					strMsgId = csMsg.getMessageID();					
					server.log("Agent(" + this.id + ") trigger a CS... ") ;
			    	server.log((CompositeSubscription)obj + "\n");
					break;
				case ILogicType.SUB:
					//sent sub
					SubscriptionMessage subMsg = subscribe((Subscription)obj); 
					strMsgId = subMsg.getMessageID();
					server.log("Agent(" + this.id + ") trigger a Sub... ") ;
			    	server.log((Subscription)obj + "\n");
					break;
				case ILogicType.PUB:
					//set pub
					PublicationMessage pubMsg = publish((Publication)obj);
					strMsgId = pubMsg.getMessageID();
					server.log("Agent(" + this.id + ") trigger a Pub... ") ;
			    	server.log((Publication)obj + "\n");
					break;
				case ILogicType.ADV:
					//set adv
					AdvertisementMessage advMsg = advertise((Advertisement)obj);
					strMsgId = advMsg.getMessageID();
					server.log("Agent(" + this.id + ") trigger a Adv...") ;
			    	server.log((Advertisement)obj + "\n");
					break;
				case ILogicType.UNSUB:
					//sent unsub
					UnsubscriptionMessage unSubMsg = unSubscribe((String)obj);
					strMsgId = unSubMsg.getMessageID();
					server.log("Agent(" + this.id + ") trigger a UnSub... ") ;
			    	//server.log((Unsubscription)obj + "\n");
					System.out.println("unSubMsg: " + unSubMsg);
					server.log(obj + "\n");
					break;
				}			
			} catch (ClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return strMsgId;
		
	}

	public String getClientId() {
		return id;
	}

	public void setClientId(String ID) {
		id = ID;
	}

		
	/*
	 * initialize the Agent, send out subscription and advertisment
	 */
	public boolean initialize()  
	{
		TaskContainer t = new TaskContainer();
		t = listener.getTask();
		//if it has not been allocated tasks, return false
		if (t.m_idProcess ==null)
			return false;
		
		// to send subscription/cs and adv
		if(t.m_objPrecondition == null){
			System.out.print("/n no precondition exist right now");
			return false;
		}

		Subscription sub = new Subscription();
		for (int i =0;i<t.m_objPrecondition.size();i++)
		{
			sub = (Subscription)t.m_objPrecondition.get(i);
			String id = sendMessage(ILogicType.SUB,sub);
			lstSubId.add(id);			
		}

		strAdvId = sendMessage(ILogicType.ADV,t.m_objPostcondition.getAdv());
		
		state = ILogicType.INITIALIZED;
		// if it does not have task
		return true;
		// else send sub/cs and Adv message to hte broker
	}

	public boolean initializeNoAdv(String advId){
		TaskContainer t = new TaskContainer();
		TaskContainer temp = listener.getTask();
		if (temp != null) {
			t = temp;			
		}
		else {
			return false;
		}
		//if it has not been allocated tasks, return false
		if (t.m_idProcess ==null)
			return false;
		
		// to send subscription/cs and adv
		if(t.m_objPrecondition == null){
			System.out.print("/n no precondition exist right now");
			return false;
		}

		Subscription sub = new Subscription();
		for (int i =0;i<t.m_objPrecondition.size();i++)
		{
			sub = (Subscription)t.m_objPrecondition.get(i);
			String id = sendMessage(ILogicType.SUB,sub);
			lstSubId.add(id);			
		}
		

		strAdvId = advId;		
		
		state = ILogicType.INITIALIZED;
		return true;		
	} 
	
	public void startClient()
	{
		listener.start();		
		state = ILogicType.STARTED;		
	}

	public boolean pauseClient()
	{
		//force the thread to stop coping with new messages in the input queue
		//but the instances will keep on running
		if (state == ILogicType.STARTED)
		{
			this.listener.suspend();
			state = ILogicType.PAUSED;
			return true;
		}else	return false;			
	}
	
	public boolean resumeClient()
	{
		//force the thread to resume.
		//start coping with new messages in the input queue
		if(state == ILogicType.PAUSED)
		{
			this.listener.resume();
			state = ILogicType.STARTED;
			return true;			
		}else	return false;		
		
	}
	
	//Synchronized stop
	public boolean stopClientSyn(Object locker)
	{
		if(!stopClientASyn()){
			return false;
		}
		synchronized (locker) {
			
			while(state != ILogicType.STOPPED)
			{
				
				try{
					locker.wait();
					}
				catch(InterruptedException e){
					System.out.println("'n error in sender/n");
					return false;
				}
			}	
			locker.notifyAll();
		}
		return true;
	}
	
	//Asynchronized stop
	public boolean stopClientASyn()
	{
		if (state != ILogicType.PAUSED)
			return false;
		
		this.listener.stop();
		stopReceivingMessage();
		state = ILogicType.FINISHING;
		this.cleanUpClient();
		return true;
	}
	
	private void cleanUpClient()
	{
		if(state != ILogicType.FINISHING){
			return;
		}
			
		Thread th = Thread.currentThread();
		while(state != ILogicType.STOPPED)
		{
			if(mapTaskInst.isEmpty()!=true)
			{
				try{
					th.wait();
					}
				catch(InterruptedException e){
					System.out.println("'n error in client wait/n");
				}
			}
			else{
				state = ILogicType.STOPPED;	}
		}
		MClientServer mServer = MClientServer.getInstance();
	 
		// Trigger unadv
	    MobilityCoordinator mCoord = mServer.getCoordinator();
	    mCoord.cleanUpOldRTs(this.id);  
	    
		//remove reference of this client
	    mServer.removeClient(id);
	}
	
	
	public void stopReceivingMessage()
	{
		Unsubscription unsub;
		for (int i =0;i<this.lstSubId.size();i++)
		{
			String id = lstSubId.get(i).toString();
			unsub = new Unsubscription(id);
			sendMessage(ILogicType.UNSUB, id); // unsubscribe id			
		}
		
	}
	public synchronized ClientState serializeState()
	{
		ClientState cs = new ClientState();
		cs.inQueue = this.withdrawQueue();
		cs.bufferingInst = this.withdrawBufferingInst();
		return cs;
		
	}
	public synchronized boolean acceptStateInformation(ClientState cs) {
		if(this.state != ILogicType.INITIALIZED)
		{
			//only client without started can accepte stateInformation 
			return false;			
		}
		if (cs.bufferingInst != null && !cs.bufferingInst.isEmpty()){
			mapTaskInst.putAll(cs.bufferingInst);
		}
		if (cs.inQueue != null && cs.inQueue.size()!=0){
			MessageUnion(cs.inQueue);
		}
		return true;
	}
	
	private void MessageUnion(MessageQueue msq)
	{
		String attribute = null;
		Publication pub = null;
		MessageQueue temp = msq;
		Message[] tempArray = inQueue.toArray();		
		while (temp.size() > 0) {
			PublicationMessage pubMsg = (PublicationMessage)temp.blockingRemove();
			for (int i = 0; i < tempArray.length; i++) {
				if ((PublicationMessage)tempArray[i] == pubMsg) {
					inQueue.add(pubMsg);
					break;
				}
			}
		}	
	}
	
	public synchronized void RemoveInstance(String idInst)
	{
		mapTaskInst.remove(idInst);		
	}
	
	public synchronized void AddInstance(String idInst, TaskInstance inst)
	{
		mapTaskInst.put(idInst, inst);		
	}
	
	public synchronized TaskInstance GetInstance(String idInst)
	{
		TaskInstance inst = (TaskInstance)mapTaskInst.get(idInst);
		return inst;		
	}
	
	public synchronized boolean ContainsInstance(String idInst)
	{
		return mapTaskInst.containsKey(idInst);
		
	}
	
	public int getState() {
		return state;
	}

    //cope with state information the client
	//*********************************************************************


	public boolean move(String target){
		MClientServer mc = MClientServer.getInstance();
		//check to see if this one is moving.
		if(mc.getCoordinator().isInMovingTX(id))
			return false;
		String source = mc.getId();
		String clientId = id;
		try {
			Publication pub;
		
			pub = MessageFactory.createPublicationFromString (
					"[class," + MobilityCoordinator.MOVEMENT +"]," 
					+ 
					"[" + MobilityCoordinator.MESSAGETYPE + "," + MobilityCoordinator.M_Trigger + "],"
					+ 
					"[" + MobilityCoordinator.SOURCESERVER +","+ source + "],"
					+
					"[" + MobilityCoordinator.TARGETSERVER +","+ target +  "],"
					+
					"[" + MobilityCoordinator.MClientID +","+ clientId +  "]"
					);
		
		
			mc.handleMovement(pub);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}	
	
	
	public String getTargetServerId() {
		return targetServerId;
	}

	public void setTargetServerId(String targetServerId) {
		this.targetServerId = targetServerId;
	}

	public void setService(String serviceName, int capacity) {
		this.service = new SimpleService(serviceName, capacity, clientID);
	}
	public SimpleService getService() {
		return this.service;
	}

	public void advertisePersistence(Advertisement adv) {
		try {
			this.advertise(adv);
		} catch (ClientException e) {
			e.printStackTrace();
		}
	}
}


