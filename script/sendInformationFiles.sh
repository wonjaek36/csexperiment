#!/bin/bash
CURRENT_EXP_FOLDER=""
PUB_BROKER_SIZE=""
SUB_BROKER_SIZE=""
PUB_FILE_PREFIX=""

CURRENT_DIR="$(dirname "$0")"
. $CURRENT_DIR/broker_pubEdge.sh
. $CURRENT_DIR/broker_subEdge.sh

echo "---sendInformationFiles.sh---"

while [ $# -gt 0 ]; do
	case "$1" in
	( -current_exp_folder )
		CURRENT_EXP_FOLDER="$2"
		shift 2;;
	( -pub_broker )
		PUB_BROKER_SIZE=$2
		shift 2;;
	( -sub_broker ) 
		SUB_BROKER_SIZE=$2
		shift 2;;
	( -pub_file_name )
		PUB_FILE_PREFIX="$2"
		shift 2;;
	( -sub_file_name )
		SUB_FILE_PREFIX="$2"
		shift 2;;
	( * )
		echo "Invalid Parameter"
		echo "1: $1 2: $2"
		exit 1
	esac
done

for ((i=0;i<$PUB_BROKER_SIZE;i++ ))
do
	BROKER=${BROKERP_LIST[$i]}
	let k="$i + 1"
	FILE_AB_PATH="$CURRENT_EXP_FOLDER""/$PUB_FILE_PREFIX""$k"
	echo "send file to $BROKER"
	scp $FILE_AB_PATH $BROKER:/home/apl/$PUB_FILE_PREFIX
done

for ((i=0;i<$SUB_BROKER_SIZE;i++ ))
do
	BROKER=${BROKERS_LIST[$i]}
	let k="$i + 1"
	FILE_AB_PATH="$CURRENT_EXP_FOLDER""/$SUB_FILE_PREFIX""$k"
	echo "send file to $BROKER"
	scp $FILE_AB_PATH $BROKER:/home/apl/$SUB_FILE_PREFIX
done
