#!/bin/bash

CLASS_PREFIX=c
VARI_PREFIX=v
TYPE=discrete
NO_EVENT=0
FOLDER_NAME=pub
NO_VALUE=0
CURRENT_FOLDER=""
NUM=0

while [ $# -gt 0 ]; do
	case "$1" in
		( -number )
			NUM="$2"
			shift 2;;
		( -num_event )
			NO_EVENT="$2"
			shift 2;;
		( -num_value )
			NO_VALUE="$2"
			shift 2;;
		( -cur_folder )
			CURRENT_FOLDER="$2"
			shift 2;;
		( * )
			echo "Wrong parameter"
			exit 50
	esac
done

cd $CURRENT_FOLDER

CLASSID=$CLASS_PREFIX$NUM
VARIID=$VARI_PREFIX$NUM
FILE=pubTest$NUM

$PADRES_HOME/bin/createPublisherData.sh -c $CLASSID -v $VARIID -t $TYPE -l 0 -h $NO_VALUE -n $NO_EVENT -f ./$FILE --assending

cd ..
