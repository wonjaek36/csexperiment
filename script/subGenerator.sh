#!/bin/bash

CLASS_PREFIX=c
VARI_PREFIX=v
TYPE=discrete
NO_VALUE=0

while [ $# -gt 0 ]; do
	case "$1" in
		( -first )
			FIRST="$2"
			shift 2;;
		( -second )
			SECOND="$2"
			shift 2;;
		( -num_value )
			NO_VALUE="$2"
			shift 2;;
		( * )
			echo "Wrong parameter"
			exit 50
	esac
done

OPTION="-n 2 -a $NO_VALUE"
OPTION=$OPTION" -c(0) c$FIRST -v(0) v$FIRST -t(0) $TYPE -l(0) 0 -h(0) $NO_VALUE"
OPTION=$OPTION" -c(1) c$SECOND -v(1) v$SECOND -t(1) $TYPE -l(1) 0 -h(1) $NO_VALUE"
OPTION=$OPTION" --assending"

java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar \
	-Djava.security.policy=$PADRES_HOME/etc/java.policy \
	ca.utoronto.msrg.padres.demo.eventgenerator.SubGenerator \
	$OPTION
: '
for (( i=0;i<$NO_CLASS;i+=2 ))
do
	let j="$i + 1"
	OPTION="-n 2 -a $NO_VALUE"
	OPTION=$OPTION" -c(0) c$i -v(0) v$i -t(0) $TYPE -l(0) 0 -h(0) $NO_VALUE"
	OPTION=$OPTION" -c(1) c$j -v(1) v$j -t(1) $TYPE -l(1) 0 -h(1) $NO_VALUE"
	OPTION=$OPTION" --assending"

	COMMAND=$PREFIX" "$OPTION

	$COMMAND
done
'
