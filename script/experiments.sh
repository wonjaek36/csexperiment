#!/bin/bash

CURRENT_FOLDER=""
NO_RUN=0
WAIT_TIME=""
ADV_TIME=""
SUB_TIME=""
SUB_ETIME=""
PUB_ETIME=""
PUB_NUM=""
PUB_BROKER_NUM=""
PUB_FILE_PATH=""
SUB_BROKER_NUM=""
SUB_FILE_PATH=""
CURRENT_EXP_FOLDER=""
#default broker number : 8

echo "---experiments.sh---"
while [ $# -gt 0 ]; do
	case "$1" in
		( -num_run )
			NO_RUN="$2"
			shift 2;;
		( -w )
			WAIT_TIME="$2"
			shift 2;;
#		( -sub_e ) deprecated
#			SUB_ETIME="$2"
#			shift 2;;
		( -pub_e )
			PUB_ETIME="$2"
			shift 2;;
		( -pub_broker )
			PUB_BROKER_NUM="$2"
			shift 2;;
		( -sub_broker )
			SUB_BROKER_NUM="$2"
			shift 2;;
		( -pub_filepath )
			PUB_FILE_PATH="$2"
			shift 2;;
		( -sub_filepath )
			SUB_FILE_PATH="$2"
			shift 2;;
		( -current_exp_folder )
			CURRENT_EXP_FOLDER="$2"
			shift 2;;
		( -adv_time )
			ADV_TIME="$2"
			shift 2;;
		( -sub_time )
			SUB_TIME="$2"
			shift 2;;
		( * )
			echo "Wrong parameter"
			echo "$1 $2"
			exit 50
	esac
done

echo "Start Experiements"
for (( i=0;i<$NO_RUN;i++ )) 
do
	#$EXP_HOME/script/broker_down.sh 
	#$EXP_HOME/script/broker_up.sh \
	#	-pub_broker $PUB_BROKER_NUM \
	#	-sub_broker $SUB_BROKER_NUM

	/bin/bash $EXP_HOME/script/sendInformationFiles.sh \
		-current_exp_folder $CURRENT_EXP_FOLDER \
		-pub_broker $PUB_BROKER_NUM \
		-sub_broker $SUB_BROKER_NUM \
		-pub_file_name "PublisherInformation" \
		-sub_file_name "subInformation"

	/bin/bash $EXP_HOME/script/testcase.sh \
		-o $i \
		-pub_broker $PUB_BROKER_NUM \
		-sub_broker $SUB_BROKER_NUM \
		-w $WAIT_TIME \
		-adv_time $ADV_TIME \
		-sub_time $SUB_TIME \
		-pub_e $PUB_ETIME \
		-pub_filepath "/home/apl/PublisherInformation" \
		-sub_filepath "/home/apl/subInformation" 
#-sub_e $SUB_ETIME deprecated

	sleep 10
	/bin/bash $EXP_HOME/script/broker_down.sh

done
