#!/bin/bash

for f in "./"*/;
do
	if [[ -d "$f" ]] && [[ $f == *"case_"* ]];
	then
		for CASE in "$f"*/;
		do
			if [[ -d "$CASE" ]] && [[ $CASE == *"Res"* ]];
			then
				for ((i=0;i<4;i++ ))
				do
					FILEPATH="$CASE""subResult""$i"
					DATA=$(cat $FILEPATH | tail -1)
					echo "$FILEPATH"",$DATA"
				done
			fi
		done
	fi
done
