#!/bin/bash
CURRENT_FOLDER=""
NO_RUN=0
WAIT_TIME=""
SUB_ETIME=""
PUB_ETIME=""
PUB_NUM=""
PUB_BROKER_NUM=""
PUB_FILE_PATH=""
SUB_BROKER_NUM=""
SUB_FILE_PATH=""

while [ $# -gt 0 ]; do
	case "$1" in
		( -num_run )
			NO_RUN="$2"
			shift 2;;
		( -w )
			WAIT_TIME="$2"
			shift 2;;
		( -sub_e )
			SUB_ETIME="$2"
			shift 2;;
		( -pub_e )
			PUB_ETIME="$2"
			shift 2;;
		( -publisher_num )
			PUB_NUM="$2"
			shift 2;;
		( -pub_broker )
			PUB_BROKER_NUM="$2"
			shift 2;;
		( -sub_broker )
			SUB_BROKER_NUM="$2"
			shift 2;;
		( -pub_filepath )
			PUB_FILE_PATH="$2"
			shift 2;;
		( -sub_filepath )
			SUB_FILE_PATH="$2"
			shift 2;;
		( * )
			echo "Wrong parameter"
			exit 50
	esac
done

echo "Start No Policy"
echo "pub_broker: $PUB_BROKER_NUM"
echo "sub_broker: $SUB_BROKER_NUM"
for (( i=0;i<$NO_RUN;i++ ))
do
	$EXP_HOME/script/broker_down.sh
	$EXP_HOME/script/broker_up.sh \
		-pub_broker $PUB_BROKER_NUM \
		-sub_broker $SUB_BROKER_NUM

	/bin/bash $EXP_HOME/script/testcase_lb.sh \
		-o $i \
		-publisher_num $PUB_NUM \
		-pub_broker $PUB_BROKER_NUM \
		-sub_broker $SUB_BROKER_NUM \
		-w $WAIT_TIME \
		-sub_e $SUB_ETIME \
		-pub_e $PUB_ETIME \
		-sub_filepath $SUB_FILE_PATH \
		-pub_filepath $PUB_FILE_PATH 
done
