#!/bin/bash

function brokerTimeSet(){
	local BROKER_LIST=("${!1}")
	
	for BROKER in ${BROKER_LIST[@]}
	do
		local SERVER=$(extractIP $BROKER)
		echo $SERVER" time setting"
		ssh $SERVER "sudo rdate -s time.bora.net"
		ssh $SERVER "date"
	done
}
function extractIP(){
	local BROKER=$1
	
	DOT_POS=$(expr index "$BROKER" @)
	IP=${BROKER:$DOT_POS}	
	
	echo $IP
}

function brokerUp(){

	local BROKER_LIST=("${!1}")
	local BROKER_PORT=("${!2}")
	local BROKER_NAME=("${!3}")
	local BROKER_USE_SIZE="$4"
	local MEMORY_OPTION="$5"
	local OPTION="$6"

	for ((i=0;i<$BROKER_USE_SIZE;i++ ))
	do
		local BROKER=${BROKER_LIST[$i]}
		local BPORT=${BROKER_PORT[$i]}
		local BNAME=${BROKER_NAME[$i]}
		local BROKER_IP=$(extractIP $BROKER)
		
		echo "ssh $BROKER_IP $PADRES_HOME/bin/startbroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION"
		echo ""
		ssh $BROKER_IP "$PADRES_HOME/bin/startbroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION" &

		sleep 2
		echo "ps -aux | grep broker"
		ssh $BROKER_IP "ps -aux | grep broker"
	done
}

PUB_BROKER_SIZE=0
SUB_BROKER_SIZE=0
NEIGHBOR_OPTION=""
CURRENT_DIR="$(dirname "$0")"
. $CURRENT_DIR/broker_pubEdge.sh
. $CURRENT_DIR/broker_subEdge.sh

while [ $# -gt 0 ]; do
	case "$1" in
		( -pub_broker )
			PUB_BROKER_SIZE=`expr $2`
			shift 2;;
		( -sub_broker )
			SUB_BROKER_SIZE=`expr $2`
			shift 2;;
		( * )
			echo "Invalid Parameter"
			exit 1
	esac
done

if [ $PUB_BROKER_SIZE -eq 0 ] || [ $SUB_BROKER_SIZE -eq 0 ]
then
	echo "Unspecify BROKER_SIZE"
	exit 1
fi

if [ $SUB_BROKER_SIZE -gt $BROKERS_SIZE ] || [ $PUB_BROKER_SIZE -gt $BROKERP_SIZE ]
then
	echo "Exceed Broker Size"
	exit 1
fi


#brokerTimeSet BROKERP_LIST[@]
#brokerTimeSet BROKERS_LIST[@]
brokerUp BROKERS_LIST[@] BROKERS_PORT[@] BROKERS_NAME[@] "$SUB_BROKER_SIZE" "3072" "$NEIGHBOR_OPTION"

NEIGHBOR_OPTION=" -n "
for (( i=0;i<$SUB_BROKER_SIZE;i++ ))
do
	BROKER=${BROKERS_LIST[$i]}
	NEIGHBOR_IP=$(extractIP $BROKER)
	PORT=${BROKERS_PORT[$i]}
	NAME=${BROKERS_NAME[$i]}

	if [ "$i" -ne 0 ]; then
		NEIGHBOR_OPTION=$NEIGHBOR_OPTION","
	fi
	NEIGHBOR_OPTION=$NEIGHBOR_OPTION"socket://$NEIGHBOR_IP:$PORT/$NAME"
done

brokerUp BROKERP_LIST[@] BROKERP_PORT[@] BROKERP_NAME[@] "$PUB_BROKER_SIZE" "3072" "$NEIGHBOR_OPTION"

/bin/bash $EXP_HOME/script/monitoringMemory.sh -pub_broker $PUB_BROKER_SIZE -sub_broker $SUB_BROKER_SIZE &

: 'exit

ssh 192.168.0.35 "~/padres/bin/startbroker.sh -uri socket://192.168.0.35:30000/BrokerE -n socket://192.168.0.31:30000/BrokerA" &
sleep 3
echo "ps -aux | grep broker"
ssh 192.168.0.35 "ps -aux | grep broker"
echo -e "\n"
'
