#!/bin/bash

ARRAY=()

function extractIP(){
	local BROKER=$1
	
	DOT_POS=$(expr index "$BROKER" @)
	IP=${BROKER:$DOT_POS}	
	
	echo $IP
}

function brokerSubUp() 
{
	local BROKER_LIST=("${!1}")
	local BROKER_PORT=("${!2}")
	local BROKER_NAME=("${!3}")
	local BROKER_USE_SIZE="$4"
	local MEMORY_OPTION="$5"
	local OPTION="$6" local BROKER_NUM="$7"
	local RESULT_FOLDER="$8"

	for (( i=0;i<$BROKER_USE_SIZE;i++ ))
	do local BROKER=${BROKER_LIST[$i]}
		local BPORT=${BROKER_PORT[$i]}
		local BNAME=${BROKER_NAME[$i]}
		local BROKER_IP=$(extractIP $BROKER)
	
		echo "ssh $BROKER_IP $PADRES_HOME/bin/startSubBroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION -broker_number $BROKER_NUM"

		ssh -t $BROKER_IP "$PADRES_HOME/bin/startSubBroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION -broker_number $BROKER_NUM" 1>>"./$RESULT_FOLDER/sub$BROKER_NUM" 2>>"./$RESULT_FOLDER/sub$BROKER_NUM" &
		MYPID=$!
		ARRAY+=($MYPID)

		sleep 1
		echo "ps -aux | grep broker"
		ssh $BROKER_IP "ps -aux | grep broker"
		echo ""
		BROKER_NUM=`expr $BROKER_NUM + 1`
	done
}


function brokerPubUp() 
{
	local BROKER_LIST=("${!1}")
	local BROKER_PORT=("${!2}")
	local BROKER_NAME=("${!3}")
	local BROKER_USE_SIZE="$4"
	local MEMORY_OPTION="$5"
	local OPTION="$6"
	local BROKER_NUM="$7"
	local RESULT_FOLDER="$8"

	for (( i=0;i<$BROKER_USE_SIZE;i++ ))
	do
		local BROKER=${BROKER_LIST[$i]}
		local BPORT=${BROKER_PORT[$i]}
		local BNAME=${BROKER_NAME[$i]}
		local BROKER_IP=$(extractIP $BROKER)
	
		echo "ssh $BROKER_IP $PADRES_HOME/bin/startPubBroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION -broker_number $BROKER_NUM"

		ssh -t $BROKER_IP "$PADRES_HOME/bin/startPubBroker.sh -uri socket://$BROKER_IP:$BPORT/$BNAME -Xmx $MEMORY_OPTION $OPTION -broker_number $BROKER_NUM" 1>>"./$RESULT_FOLDER/pub$BROKER_NUM" 2>>"./$RESULT_FOLDER/pub$BROKER_NUM" &
		MYPID=$!
		ARRAY+=($MYPID)

		sleep 1
		echo "ps -aux | grep broker"
		ssh $BROKER_IP "ps -aux | grep broker"
		echo ""
		BROKER_NUM=`expr $BROKER_NUM + 1`
	done
}

NUMBER=0 #default
CURRENT_FOLDER=""
WAIT_TIME=""
SUB_ETIME=""
PUB_ETIME=""
PUB_NUM=""
ADV_TIME=""
SUB_TIME=""
PUB_BROKER_NUM=""
SUB_BROKER_NUM=""
PUB_FILEPATH=""
SUB_FILEPATH=""
SLEEP_TIME=""
BROKER_NUMBER=1

echo "---testcase.sh---"
. $EXP_HOME/script/broker_pubEdge.sh
. $EXP_HOME/script/broker_subEdge.sh
. $EXP_HOME/script/client_name.sh

while [ $# -gt 0 ]; do
	case "$1" in
	( -o )
		NUMBER=$2
		shift 2 ;;
	( -w )
		WAIT_TIME="$2"
		shift 2;;
#	( -sub_e ) deprecated
#		SUB_ETIME="$2"
#		shift 2;;
	( -pub_e )
		PUB_ETIME="$2"
		shift 2;;
	( -pub_broker )
		PUB_BROKER_NUM="$2"
		shift 2;;
	( -sub_broker )
		SUB_BROKER_NUM="$2"
		shift 2;;
	( -pub_filepath )
		PUB_FILE_PATH="$2"
		shift 2;;
	( -sub_filepath )
		SUB_FILE_PATH="$2"
		shift 2;;
	( -adv_time )
		ADV_TIME="$2"
		shift 2;;
	( -sub_time )
		SUB_TIME="$2"
		shift 2;;
	( * )
		echo "$1 $2"
		exit
	esac
done

RESULT_FOLDER=Result
RESULT_FOLDER="$RESULT_FOLDER$NUMBER"
#SLEEP_TIME=`expr $SUB_ETIME + 50`
rm -rf ./$RESULT_FOLDER
mkdir ./$RESULT_FOLDER

pwd
echo "$RESULT_FOLDER"

OPTION="-sync_time $WAIT_TIME -adv_wait_time $ADV_TIME -info_filepath $SUB_FILE_PATH"
#-exec_time $SUB_ETIME deprecated (20170116)
brokerSubUp BROKERS_LIST[@] BROKERS_PORT[@] BROKERS_NAME[@] "$SUB_BROKER_NUM" "2048" "$OPTION" "1" "$RESULT_FOLDER"
#Subscription Broker On

#PUB_ADV_TIME=`expr $ADV_TIME`
#PUB_WAIT_TIME=`expr $WAIT_TIME+50`
OPTION="-sync_time $WAIT_TIME -sub_wait_time $SUB_TIME -exec_time $PUB_ETIME -info_filepath $PUB_FILE_PATH"
#Publication Broker On

echo "$OPTION"
NEIGHBOR_OPTION=" -n "
for (( i=0;i<$SUB_BROKER_NUM;i++ ))
do
	BROKER=${BROKERS_LIST[$i]}
	NEIGHBOR_IP=$(extractIP $BROKER)
	PORT=${BROKERS_PORT[$i]}
	NAME=${BROKERS_NAME[$i]}

	if [ "$i" -ne 0 ]; then
		NEIGHBOR_OPTION=$NEIGHBOR_OPTION","
	fi
	NEIGHBOR_OPTION=$NEIGHBOR_OPTION"socket://$NEIGHBOR_IP:$PORT/$NAME"
done

OPTION=$OPTION$NEIGHBOR_OPTION
BROKER_NUMBER=`expr $SUB_BROKER_NUM + 1`
brokerPubUp BROKERP_LIST[@] BROKERP_PORT[@] BROKERP_NAME[@] "$PUB_BROKER_NUM" "2048" "$OPTION" "$BROKER_NUMBER" "$RESULT_FOLDER"

/bin/bash $EXP_HOME/script/monitoringMemory.sh -pub_broker $PUB_BROKER_NUM -sub_broker $SUB_BROKER_NUM 2> /dev/null &

for pid in ${ARRAY[@]}
do
	echo "PID:$pid"
done

for pid in ${ARRAY[@]}
do
	wait $pid
	echo "PID: $pid is done"
done
echo "All processes are done"
sleep 10
#echo "$SLEEP_TIME"
#sleep $SLEEP_TIME
