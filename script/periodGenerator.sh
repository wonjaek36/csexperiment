PUBLISHER_FLAG="-publisher"
PERIOD_LOW_FLAG="-periodLow"
PERIOD_HIGH_FLAG="-periodHigh"
FILE_PATH_FLAG="-filepath"
SHELL_FILE_PATH_FLAG="-shell_filepath"
DEGREE_CO_FLAG="-degree_co"

PUBLISHER_SIZE=0
PERIOD_LOW=0
PERIOD_HIGH=0
FILEPATH=""
SHELL_FILEPATH=""
DEGREE_CO=0
while [ $# -gt 0 ]; do
	case "$1" in
		( $PUBLISHER_FLAG )
			PUBLISHER_SIZE="$2"
			shift 2;;
		( $PERIOD_LOW_FLAG ) 
			PERIOD_LOW="$2"
			shift 2;;
		( $PERIOD_HIGH_FLAG )
			PERIOD_HIGH="$2"
			shift 2;;
		( $FILE_PATH_FLAG )
			FILEPATH="$2"
			shift 2;;
		( $DEGREE_CO_FLAG )
			DEGREE_CO="$2"
			shift 2;;
		( $SHELL_FILE_PATH_FLAG )
			SHELL_FILEPATH="$2"	
			shift 2;;
		( * )
			echo "$1 $2"
			exit 1 
	esac
done

java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	ca.utoronto.msrg.padres.demo.eventgenerator.PeriodGenerator\
	$PUBLISHER_FLAG $PUBLISHER_SIZE \
	$PERIOD_LOW_FLAG $PERIOD_LOW \
	$PERIOD_HIGH_FLAG $PERIOD_HIGH \
	$FILE_PATH_FLAG $FILEPATH \
	$SHELL_FILE_PATH_FLAG $SHELL_FILEPATH \
	$DEGREE_CO_FLAG $DEGREE_CO 

