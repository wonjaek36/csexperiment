PUBLISHER_FLAG="-publisher"
VALUE_FLAG="-value"
FILE_PATH_FLAG="-filepath"
SHELL_FILE_PATH_FLAG="-shell_filepath"

PUBLISHER_SIZE=0
VALUE=0
FILEPATH=""
SHELL_FILEPATH=""

while [ $# -gt 0 ]; do
	case "$1" in
		( $PUBLISHER_FLAG )
			PUBLISHER_SIZE="$2"
			shift 2;;
		( $VALUE_FLAG )
			VALUE="$2"
			shift 2;;
		( $FILE_PATH_FLAG )
			FILEPATH="$2"
			shift 2;;
		( $SHELL_FILE_PATH_FLAG )
			SHELL_FILEPATH="$2"
			shift 2;;
		( * )
			echo "$1 $2"
			exit 1
	esac
done

java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar\
	-Djava.security.policy=$PADRES_HOME/etc/java.policy\
	ca.utoronto.msrg.padres.demo.eventgenerator.ValueGenerator\
	$PUBLISHER_FLAG $PUBLISHER_SIZE \
	$FILE_PATH_FLAG $FILEPATH \
	$SHELL_FILE_PATH_FLAG $SHELL_FILEPATH \
	$VALUE_FLAG $VALUE

