
#!/bin/bash
function extractIP(){
	local BROKER=$1
	
	DOT_POS=$(expr index "$BROKER" @)
	IP=${BROKER:$DOT_POS}	
	
	echo $IP
}

NUMBER=0 #default
CURRENT_FOLDER=""
WAIT_TIME=""
SUB_ETIME=""
PUB_ETIME=""
PUB_NUM=""
PUB_BROKER=""
SUB_BROKER=""
PUB_FILEPATH=""
SUB_FILEPATH=""
SLEEP_TIME=""
CASE="lb"

. $EXP_HOME/script/broker_pubEdge.sh
. $EXP_HOME/script/broker_subEdge.sh
. $EXP_HOME/script/client_name.sh

while [ $# -gt 0 ]; do
	case "$1" in
	( -o )
		NUMBER=$2
		shift 2 ;;
	( -w )
		WAIT_TIME="$2"
		shift 2;;
	( -sub_e )
		SUB_ETIME="$2"
		shift 2;;
	( -pub_e )
		PUB_ETIME="$2"
		shift 2;;
	( -publisher_num )
		PUB_NUM="$2"
		shift 2;;
	( -pub_broker )
		PUB_BROKER="$2"
		shift 2;;
	( -sub_broker )
		SUB_BROKER="$2"
		shift 2;;
	( -pub_filepath )
		PUB_FILE_PATH="$2"
		shift 2;;
	( -sub_filepath )
		SUB_FILE_PATH="$2"
		shift 2;;
	( * )
		echo "$1 $2"
	esac
done

RESULT_FOLDER=Res"$CASE"
RESULT_FOLDER="$RESULT_FOLDER$NUMBER"
SLEEP_TIME=`expr $SUB_ETIME + 120`
rm -rf ./$RESULT_FOLDER
mkdir ./$RESULT_FOLDER

pwd
OPTIONS=()

echo "$RESULT_FOLDER"
for (( i=0;i<$PUB_BROKER;i++ )) 
do
	for (( j=0;j<$PUB_NUM;j++ )) 
	do
		let k="$i * $PUB_NUM + $j"
		source "./""$CASE""Publisher$k"
		echo "Reading ""$CASE""Publisher$k"
		SIZE=${#NUMBER[@]}
		for (( l=0;l<$SIZE;l++ ))
		do
			OPTIONS[$k]=${OPTIONS[$k]}" -n ${NUMBER[$l]} ${PERIOD[$l]}"
		done
	done
done

for (( i=0;i<$PUB_BROKER;i++ ))
do
	BROKER=${BROKERP_LIST[$i]}
	BROKER_IP=$(extractIP $BROKER)
	PORT=${BROKERP_PORT[$i]}
	NAME=${BROKERP_NAME[$i]}

	CLIENT_NAME=${PUB_CLIENT_NAME[$i]}

	for (( j=0;j<$PUB_NUM;j++ ))
	do
		let k="$i * $PUB_NUM + $j"
		echo "$PADRES_HOME/bin/startpublisher.sh -b socket://$BROKER_IP:$PORT/$NAME -i $CLIENT_NAME -w $WAIT_TIME -e $PUB_ETIME -a -ifp $PUB_FILE_PATH/pubTest -ofp ./$RESULT_FOLDER/pubResult ${OPTIONS[$k]} &"
		$PADRES_HOME/bin/startpublisher.sh -b socket://$BROKER_IP:$PORT/$NAME \
			-i $CLIENT_NAME \
			-w $WAIT_TIME \
		       	-e $PUB_ETIME \
		       	-a \
		       	-ifp $PUB_FILE_PATH/pubTest \
		       	-ofp ./$RESULT_FOLDER/pubResult \
		       	${OPTIONS[$k]} &
	done
done

for (( i=0;i<$SUB_BROKER;i++ ))
do
	BROKER=${BROKERS_LIST[$i]}
	BROKER_IP=$(extractIP $BROKER)
	PORT=${BROKERS_PORT[$i]}
	NAME=${BROKERS_NAME[$i]}
	FILENAME=$SUB_FILE_PATH"/subTest""$i"
	RESULT_FILE="./""$RESULT_FOLDER""/subResult""$i"
	echo "!!: $RESULT_FILE"

	CLIENT_NAME=${SUB_CLIENT_NAME[$i]}

	echo "$PADRES_HOME/bin/startsubscriber.sh -b socket://$BROKER_IP:$PORT/$NAME -i $CLIENT_NAME -e $SUB_ETIME -f $SUB_FILE_PATH/subTest &"
	$PADRES_HOME/bin/startsubscriber.sh \
		-b socket://$BROKER_IP:$PORT/$NAME \
		-i $CLIENT_NAME \
	       	-e $SUB_ETIME \
		-f $FILENAME > $RESULT_FILE &
done

echo "Waiting"" $SLEEP_TIME"
echo "$SLEEP_TIME"
sleep $SLEEP_TIME
