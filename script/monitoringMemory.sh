#!/bin/bash
CURRENT_DIR="$(dirname "$0")"
. $CURRENT_DIR/broker_pubEdge.sh
. $CURRENT_DIR/broker_subEdge.sh
PUB_BROKER_SIZE=0
SUB_BROKER_SIZE=0

while [ $# -gt 0 ]; do
	case "$1" in
		( -pub_broker )
			PUB_BROKER_SIZE=`expr $2`
			shift 2;;
		( -sub_broker )
			SUB_BROKER_SIZE=`expr $2`
			shift 2;;
		( * )
			echo "Invalid Parameter"
			exit 1
	esac
done 

if [ $PUB_BROKER_SIZE -eq 0 ] || [ $SUB_BROKER_SIZE -eq 0 ]
then
	echo "Unspecify BROKER_SIZE"
	exit 1
fi

if [ $SUB_BROKER_SIZE -gt $BROKERS_SIZE ] || [ $PUB_BROKER_SIZE -gt $BROKERP_SIZE ]
then
	echo "Exceed Broker Size"
	exit 1
fi

#ssh apl@192.168.0.102 "jstat -gccapacity 1245"
while true
do
	BROKER_PID=""
	for address in "${BROKERS_LIST[@]}"
	do
		BROKER_PID="$(ssh $address "jps | grep BrokerCore | sed 's/[^0-9]*//g'")"
		echo "-----------------------------------------------------------" >> "broker_$address"
		(ssh $address "jstat -gccapacity $BROKER_PID") >> "broker_$address" 2> /dev/null
		(ssh $address "jstat -gcutil $BROKER_PID")  >> "broker_$address" 2> /dev/null
		echo "" >> "broker_$address"
		echo "pid, pcpu, pmem, rss, size, vsize" >> "broker_$address"
		(ssh $address "ps -eo pid,pcpu,pmem,rss,size,vsize | grep $BROKER_PID") >> "broker_$address"
		echo "-----------------------------------------------------------" >> "broker_$address"
		echo "" >> "broker_$address"
	done
	for address in "${BROKERP_LIST[@]}"
	do
		BROKER_PID="$(ssh $address "jps | grep BrokerCore | sed 's/[^0-9]*//g'")"
		echo "-----------------------------------------------------------" >> "broker_$address"
		(ssh $address "jstat -gccapacity $BROKER_PID") >> "broker_$address" 2> /dev/null
		(ssh $address "jstat -gcutil $BROKER_PID")  >> "broker_$address" 2> /dev/null
		echo "" >> "broker_$address"
		echo "pid, pcpu, pmem, rss, size, vsize" >> "broker_$address"
		(ssh $address "ps -eo pid,pcpu,pmem,rss,size,vsize | grep $BROKER_PID") >> "broker_$address"
		echo "-----------------------------------------------------------" >> "broker_$address"
		echo "" >> "broker_$address"
	done
	sleep 20
done
