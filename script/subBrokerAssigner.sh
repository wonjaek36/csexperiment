BROKER_SIZE_FLAG="-broker_size"
PERIOD_FILE_PATH_FLAG="-period_filepath"
VALUE_FILE_PATH_FLAG="-value_filepath"
PRINT_PATH_PREFIX_FLAG="-printpath_prefix"
POLICY_FLAG="-policy"

BROKER_SIZE=""
PERIOD_FILEPATH=""
VALUE_FILEPATH=""
PRINT_PATH_PREFIX=""
POLICY=""

while [ $# -gt 0 ]; do
	case "$1" in
		( $BROKER_SIZE_FLAG )
			BROKER_SIZE=$2
			shift 2;;
		( $PERIOD_FILE_PATH_FLAG )
			PERIOD_FILEPATH=$2
			shift 2;;
		( $VALUE_FILE_PATH_FLAG )
			VALUE_FILEPATH=$2
			shift 2;;
		( $PRINT_PATH_PREFIX_FLAG )
			PRINT_PATH_PREFIX=$2
			shift 2;;
		( $POLICY_FLAG )
			POLICY=$2
			shift 2;;
		( * )
			echo "Wrong parameter"
			exit 1
	esac
done

OPTION=$BROKER_SIZE_FLAG" "$BROKER_SIZE
OPTION=$OPTION" "$FILE_PATH_FLAG" "$FILE_PATH
OPTION=$OPTION" "$PRINT_PATH_PREFIX_FLAG" "$PRINT_PATH_PREFIX

java -cp $CLASSPATH:$PADRES_HOME/lib/*:$PADRES_HOME/padres-2.0.jar \
	-Djava.security.policy=$PADRES_HOME/etc/java.policy \
	ca.utoronto.msrg.padres.demo.eventgenerator.SubscriptionAssigner \
	$BROKER_SIZE_FLAG $BROKER_SIZE \
	$PERIOD_FILE_PATH_FLAG $PERIOD_FILEPATH \
	$VALUE_FILE_PATH_FLAG $VALUE_FILEPATH \
	$PRINT_PATH_PREFIX_FLAG $PRINT_PATH_PREFIX \
	$POLICY_FLAG $POLICY 

