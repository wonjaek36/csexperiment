#!/bin/bash

CASE_NUM=7
#NUM_VALUE=0
NO_CLASS=200
NO_EVENT=140

BROKER_SIZE=2
RANK=10
SKEW="2.2"

NO_RUN=3
PUB_SIZE=1
WAIT_TIME=80
SUB_ETIME=300
PUB_ETIME=40
NUM_VALUE=50
FOLDER_PREFIX="case_"

#./scrum3/padres
/bin/bash ./../periodGenerator.sh \
	-num_class $NO_CLASS \
	-period_skew $SKEW \
	-period_low 1\
	-period_high $RANK\
	-value $NUM_VALUE\
	-value_skew 0 > classStatus
echo "-----periodGenerator-----"
echo ""

source classStatus_shell

PUB_FOLDER=pub
if [ -d "$PUB_FOLDER" ];
then
	rm -rf $PUB_FOLDER
fi
mkdir $PUB_FOLDER
for (( i=0;i<$NO_CLASS;i++ ))
do
	/bin/bash $EXP_HOME/pubGenerator.sh -number $i -num_event $NO_EVENT -num_value ${VALUES[$i]} -cur_folder $PUB_FOLDER
done
echo "-----pubGenerator-----"
echo ""

SUB_FOLDER=sub
if [ -d "$SUB_FOLDER" ];
then
	rm -rf $SUB_FOLDER
fi
mkdir $SUB_FOLDER
for (( i=0;i<$NO_CLASS;i+=2 ))
do
	let j="$i + 1"
	/bin/bash $EXP_HOME/subGenerator.sh -num_value ${VALUES[$i]} -first $i -second $j >> ./$SUB_FOLDER/subTest
done
echo "-----subGenerator-----"
echo ""

for (( i=1;i<=$CASE_NUM;i++ ))
do
	let BROKER_SIZE=$i+1
	CURRENT_FOLDER=$FOLDER_PREFIX$i
	if [ -d $CURRENT_FOLDER ];
	then
		rm -rf $CURRENT_FOLDER
	fi
	mkdir $CURRENT_FOLDER

	cd $CURRENT_FOLDER
	/bin/bash $EXP_HOME/assignClassToBroker.sh -broker_size $BROKER_SIZE -publisher_size 1 -periodFilePath ./../classStatus > brokerStatus
	cd ..

	let SUB_ETIME=300
	echo "Subscriber Running Time $SUB_ETIME"

	/bin/bash $EXP_HOME/experiments.sh -num_run $NO_RUN -cur_folder $CURRENT_FOLDER\
		-broker_num $BROKER_SIZE -publisher_num $PUB_SIZE \
	       	-w $WAIT_TIME -sub_e $SUB_ETIME -pub_e $PUB_ETIME
	echo "-----experiment-----"
	echo "SUB_ETIME : $SUB_ETIME"
	echo ""
done
