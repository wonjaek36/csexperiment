#!/bin/bash

function brokerDown(){
	local BROKER_LIST=("${!1}")

	for BROKER in ${BROKER_LIST[@]}
	do
		echo $BROKER
		for i in $(ssh $BROKER "jps | grep Broker | grep -o '[0-9]*'")
		do
			echo "ssh $BROKER kill $i"
			ssh $BROKER kill $i
		done
	done
}

CURRENT_DIR="$(dirname "$0")"
. $CURRENT_DIR/broker_pubEdge.sh
. $CURRENT_DIR/broker_subEdge.sh

monitoringPID=$(ps -ax | grep monitoringMemory[.sh] | awk '{ print $1 }')
echo "kill -9 $monitoringPID"
kill $monitoringPID

brokerDown BROKERP_LIST[@]
brokerDown BROKERS_LIST[@]

